<?php
/*
Plugin Name: Bloomkit Wordpress Plugin
Description:
Author: Bloomkit
Author URI:
Plugin URI:
*/

use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpKernel\Exception\HttpException;

use App\Classes\Wordpress;

define('BK_PLUGIN_DIR', __DIR__);

call_user_func(function() {
    $namespace = 'bloomkit';

    $version = 'v1';
    /**
     * Create and/or get access to the Lumen application container.
     * @param $make Can be null, a string, or a function.
     * @return If `$make` is a string, returns Application::make($make); if `$make`
     *   is a function, that function is invoked with the Application instance as the
     *   first and only argument, returning the result of that function call; otherwise,
     *   the Application instance is returned.
     */
    $app = function($make) {
        static $app;
        if (empty($app)) {
            $app = require __DIR__.'/bootstrap/app.php';
        }
        if (is_callable($make)) {
            return $make($app);
        }
        return $make ? $app->make($make) : $app;
    };


    /**
     * Register an activation hook that executions any database migrations.
     */
//    register_activation_hook(__FILE__, function() use ($app) {
//        $app(function($app) {
//            if (!Schema::hasTable('fpc_migrations')) {
//                Artisan::call('migrate:install');
//            }
//            Artisan::call('migrate', ['--force' => '1']);
//        });
//    });


    /**
     * Run Laravel artisan from within this plugin
     */
    if (class_exists('WP_CLI')) {
        WP_CLI::add_command($namespace, function($args) use ($app) {
            $app(function($app) use ($args) {
                if (empty($args))	{
                    WP_CLI::error("Unknown artisan command");
                    exit;
                }
                Artisan::call(array_shift($args), array_reduce($args, function($result, $arg) {
                    @list($name, $value) = explode('=', $arg);
                    $result[$name] = $value ? $value : 1;
                    return $result;
                }, []));
                WP_CLI::log(Artisan::output());
            });
        });
    }

    // Register Widgets
    // todo: this isn't ideal, would be nice to move to AppServiceProvider
    add_action('widgets_init', function() use ($app, $namespace, $version) {
        return $app(function() use ($app, $namespace, $version) {
            Wordpress::register_widgets();
        });
    });


    // WP Init with $app
    add_action('init', function() use ($app, $namespace, $version) {
        return $app(function() use ($app, $namespace, $version) {
            return $app;
        });
    }, 1);
});

