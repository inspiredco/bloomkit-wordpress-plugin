angular.module(Config.app_name)

    .factory('APIService', function($q, $http, $httpParamSerializerJQLike) {
        var Control = {
            post : function(type, post) {
                post.action = type;

                return $http.post(APP_CONFIG.api_url, $httpParamSerializerJQLike(post), {
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded'
                    }
                });
            }
        };

        return Control;
    })

    .factory('RequestService', function($q, $http) {


        var Control = {
            post : function(type, post) {
                var canceller = $q.defer();

                var cancel = function(reason){
                    canceller.resolve(reason);
                };

                var promise = $http.post(APP_CONFIG.api_url+'/'+type, post, { timeout: canceller.promise });

                return {
                    promise : promise,
                    cancel : cancel
                };
            },

            get : function(type) {
                return $http.get(APP_CONFIG.api_url+'/'+type);
            }
        };

        return Control;
    })

    .factory('AppService', function(NotifyService, LoadingService) {
        var Control = {
            Notify : NotifyService,
            Loading : LoadingService
        };

        return Control;
    })

    .factory('NotifyService', function() {
        var _items = [];
        var _container = false;

        var Control = {
            create_container : function() {
                if(_container === false) {
                    _container = $('<div class="user_notify_bar"></div>').appendTo('body');
                }
            },

            show : function(type, text) {
                var note = $('<div class="notify_item"></div>').addClass(type).html('<p>'+text+'</p>');
                var timer = setTimeout(function() {
                    note.fadeOut(500, function() {
                        note.remove();
                    });
                }, 3000);

                Control.create_container();

                _container.append(note);
            }
        };

        return Control;
    })

    .factory('LoadingService', function($timeout) {
        var elem = $('#site_cover');
        var splash = $('#site_cover');

        var Control = {
            show : function(state) {
                if(state) {
                    elem.fadeIn(400);
                } else {
                    elem.fadeOut(400);
                }
            },

            splash : function(state) {
                if(state) {
                    splash.fadeIn(400).addClass('loading');
                } else {
                    splash.fadeOut(400).removeClass('loading');
                }
            },

            splash_to_success : function(config) {
                var text_box = $('.cover_text .text_title', splash);

                splash.removeClass('loading').addClass('success');

                text_box.html(config.text);

                if(config.timeout) {
                    $timeout(function() {
                        splash.fadeOut(400).removeClass('success');
                    }, config.timeout);
                }
            }
        };

        return Control;
    })

    .factory('StoreService', function($rootScope, APIService, RequestService) {
        var _data = {
            store_token : 11
        };

        var Control = {
            load : function(item) {
                _data = item;

                return Control;
            },

            set_token : function(token) {
                _data.store_token = token;

                return Control;
            },

            fetch_products : function(post) {
                //post.store_token = _data.store_token;

                post.method = 'products_search';


                return APIService.post('bloom_shop', post).then(function(res) {
                    return res;
                });
            },

            request_products : function(post) {
                post.store_token = _data.store_token;

                return RequestService.post('products/search', post).then(function(res) {
                    if(res.data.success === true) {
                        //$scope.products = res.data.products;
                    }

                    return res;
                });
            }
        };

        return Control;
    })

    .factory('ProductsService', function() {
        var _items = [];
        var _id_map = {};

        var Control = {
            load : function(items) {
                items.forEach(function(item) {
                    _items.push(item);
                    _id_map[item.product_id] = (_items.length - 1);
                });

                return Control;
            },

            get : function(index) {
                return _items[index];
            },

            get_by_id : function(id) {
                return _items[_id_map[id]];
            },

            get_count : function() {
                return _items.length;
            },

            clear : function() {
                _items = [];
            },

            list_all : function() {
                return _items;
            }
        };

        return Control;
    })


    .factory('ProductService', function($rootScope, APIService) {
        var _data = {};

        var Control = {
            load : function(item) {
                _data = item;

                return Control;
            },

            get : function() {
                return _data;
            },

            get_id : function() {
                return _data.product_id;
            },

            price_get : function(index) {
                return _data.prices[index];
            },

            price_list_all : function() {
                return _data.prices;
            },

            price_unit : function(unit) {
                if(unit === undefined) {

                } else {

                }
            },

            review_list : function(data) {
                console.log('REVIEW', data);

                var post = {
                    'method' : 'product_reviews',
                    'product_id' : _data.product_id
                };

                return APIService.post('bloom_shop', post).then(function(res) {
                    if(res.data.success) {
                        console.log(res.data);
                        // if(Array.isArray(_data.reviews)) {
                        //     _data.reviews.unshift(res.data.review);
                        // }
                    }

                    return res;
                });
            },

            review_save : function(data) {
                console.log('REVIEW', data);

                var post = {
                    'method' : 'review_submit',
                    'product_id' : Control.get_id(),
                    'review' : data
                };

                return APIService.post('bloom_shop', post).then(function(res) {
                    if(res.data.success) {
                        if(Array.isArray(_data.reviews)) {
                            _data.reviews.unshift(res.data.review);
                        }
                    }

                    return res;
                });
            }
        };

        return Control;
    })

    .factory('CheckoutService', function($timeout, $rootScope) {
        // let checkout = Bloomkit.Checkout();
        //
        //checkout.load();

        return Bloomkit.Checkout();
    })

    /* DEPRECATE
    .factory('CheckoutService', function(APIService, CustomerService, OrderService, $timeout, $rootScope) {
        var _data = {
            shipping_methods : [],
            billing_methods : []
        };

        var Control = {
            Customer : CustomerService,
            Order : OrderService,

            load : function(item) {
                console.log('CO LOAD', JSON.parse(JSON.stringify(item)));

                if(item.order !== undefined) {
                    Control.set_order(item.order);
                }

                if(item.customer !== undefined) {
                    Control.set_customer(item.customer);
                }
            },

            set_order : function(data) {
                if(data) {
                    // if(_data.shipping_methods.length === 1) {
                    //     data.order_shipping_method_id = _data.shipping_methods[0].method_id;
                    // }

                    Control.Order.load(data);
                }

                return Control;
            },

            set_customer : function(data) {
                Control.Customer.load(data);

                return Control;
            },

            set_shipping_methods : function(methods) {
                _data.shipping_methods = methods
            },

            list_shipping_methods : function() {
               return _data.shipping_methods;
            },

            set_billing_methods : function(methods) {
                _data.billing_methods = methods
            },

            list_billing_methods : function() {
                return _data.billing_methods;
            },

            get : function() {
                return _data;
            },

            get_order_id : function() {
                return Control.Order.get_id() || false;
            },

            get_order_token : function() {
                return Control.Order.get_token() || false;
            },

            has_customer : function() {

            },

            login : function(post) {
                post.method = 'login';

                if(Control.get_order_token()) {
                    post.token = Control.get_order_token();
                }

                return APIService.post('bloom_checkout', post).then(function(res) {
                    if(res.data.success === true) {
                        if(res.data.customer) {
                            Control.set_customer(res.data.customer);
                        }

                        if(res.data.order) {
                            Control.set_order(res.data.order);

                            //Control.Customer.Cart.load_items(res.data.order.items);

                            $rootScope.$emit('order-updated');
                        }
                    }

                    return res;
                });
            },

            save : function(post) {
                post.method = 'save';

                if(Control.get_order_token()) {
                    post.token = Control.get_order_token();
                }

                return APIService.post('bloom_checkout', post).then(function(res) {
                    if(res.data.success === true) {
                        if(res.data.customer) {
                            Control.set_customer(res.data.customer);
                        }

                        if(res.data.order) {
                            Control.set_order(res.data.order);

                            Control.Customer.Cart.load(res.data.order.cart)
                            $rootScope.$emit('order-updated');
                        }
                    }

                    return res;
                });
            },

            complete : function(post) {
                post.method = 'complete';

                if(Control.get_order_token()) {
                    post.token = Control.get_order_token();
                }

                return APIService.post('bloom_checkout', post).then(function(res) {
                    $timeout(function() {
                        if(res.data.success === true) {
                            if(res.data.customer) {
                                Control.Customer.load(res.data.customer);
                            }

                            if(res.data.order) {
                                Control.Order.load(res.data.order);
                            }
                        }
                    });

                    return res;
                });
            },

            get_totals : function() {
                return Control.get().totals;
            }
        };

        return Control;
    })
    */

    .factory('OrderService', function(APIService, $rootScope) {
        var _data = {
            shipping_address : {},
            billing_address : {},
            order_billing_method_value : {},
            order_shipping_method_value : {}
        };

        var Control = {
            load : function(item) {

                _data = item;

                if(typeof _data.order_shipping_method_value !== 'object') {
                    _data.order_shipping_method_value = {
                        address: {}
                    };
                }

                if(typeof _data.order_billing_method_value !== 'object') {
                    _data.order_billing_method_value = {};
                }

                return Control;
            },

            get : function() {
                return _data;
            },

            get_id : function() {
                return _data.order_id || false;
            },

            get_shipping_value : function() {
                return _data.order_shipping_method_value;
            },

            get_billing_value : function() {
                return _data.order_billing_method_value;
            },

            get_token : function() {
                return _data.token || false;
            },

            update : function(data) {
                if(data) {
                    _data = $.extend({}, _data, data);
                }

                return Control;
            },

            save : function(post) {
                return APIService.post('bloom_checkout', post).then(function(res) {
                    if(res.data.success === true) {
                        if(res.data.order) {
                            Control.load(res.data.order);
                        }

                    }

                    return res;
                });
            }
        };

        $rootScope.$on('order-updated', function(event, data) {
            console.log(event, data);
        });

        return Control;
    })

    .factory('CustomerService', function(APIService) {
        return Bloomkit.Customer;
    })

    /* DEPRECATED
    .factory('CustomerService', function(APIService, CustomerCartService) {
        var _data = {};

        var _d_data = {
            'customer_pass_confirm' : ''
        };

        var Control = {
            Cart : CustomerCartService,

            load : function(item) {
                if(item) {
                    if(item.customer_id !== undefined) {
                        Control.Cart.clear_cookie();
                    }

                    if(item.customer_id !== undefined) {
                        //Control.Cart.set_customer(item.customer_id);
                    }

                    _data = item;
                }

                console.log(item);

                return Control;
            },

            get : function() {
                if(Control.get_id() === false) {
                    return Object.assign(_d_data, _data);
                }

                return _data;
            },

            get_id : function() {
                return _data.customer_id || false;
            },

            is_auth : function() {
                return (Control.get_id() !== false);
            },

            update : function(data) {
                if(data) {
                    _data = $.extend({}, _data, data);
                }

                return Control;
            },

            reset_pass : function() {
                return APIService.post('bloom_customer_save', post).then(function(res) {
                    if(res.data.success === true) {
                        if(res.data.customer) {
                            Control.update(res.data.customer);
                        }
                    }

                    return res;
                });
            },

            change_pass : function(post) {
                post.method = 'save';

                return APIService.post('bloom_account', post).then(function(res) {
                    if(res.data.success === true) {
                        if(res.data.customer) {
                            Control.update(res.data.customer);
                        }
                    }

                    return res;
                });
            },

            invite : function(post) {
                post.method = 'invite';

                return APIService.post('bloom_account', post).then(function(res) {
                    return res;
                });
            },

            follow : function(type, id, state) {
                if(state !== false) {
                    state = true;
                }

                var post = {
                    'method' : 'follow',
                    'type' : type,
                    'id' : id,
                    'follow' : state
                };

                return APIService.post('bloom_account', post).then(function(res) {
                    return res;
                });
            },

            set_primary_address : function(address) {
                var post = {
                    method : 'save',
                    customer: {
                        'customer_default_address_shipping_id' : address
                    }
                };

                return APIService.post('bloom_account', post).then(function(res) {
                    if(res.data.success === true) {
                        if(res.data.customer) {
                            Control.update(res.data.customer);
                        }
                    }

                    return res;
                });
            },

            register : function(data) {
                var post = {
                    method : 'save',
                    customer: data
                };

                return APIService.post('bloom_register', post).then(function(res) {
                    if(res.data.success === true) {
                        if(res.data.customer) {
                            Control.update(res.data.customer);
                        }
                    }

                    return res;
                });
            },
            save : function(data) {
                var post = {
                    method : 'save',
                    customer: data
                };

                return APIService.post('bloom_account', post).then(function(res) {
                    if(res.data.success === true) {
                        if(res.data.customer) {
                            Control.update(res.data.customer);
                        }
                    }

                    return res;
                });
            }
        };

        return Control;
    })
    */

    .factory('CustomerCartService', function($rootScope, APIService, $cookies) {
        var Control = {
            load : function(cart) {
                console.log('BK CART UPDATE RAW', JSON.parse(JSON.stringify(cart)));

                Bloomkit.Customer.Cart.load(cart);

                console.log('BK CART UPDATE', cart);

                //$rootScope.$emit('customer-cart-updated');

                return Control;
            },

            load_items : function(items) {
                console.log('CUSTOMER CART LI', items);

                return Bloomkit.Customer.Cart.load_items(items);
            },

            get : function() {
                return Bloomkit.Customer.Cart.get();
            },

            update : function(cart) {
                return Bloomkit.Customer.Cart.update(cart);
            },

            get_id : function() {
                return Bloomkit.Customer.Cart.get_id();
            },

            get_item : function(idx) {
                return _cart.items[idx];
            },

            item_add : function(item) {
                return Bloomkit.Customer.Cart.item_add(item).then(function(res) {
                    $rootScope.$emit('customer-cart-updated');

                    return res;
                });
            },

            item_update : function(item) {
                return Bloomkit.Customer.Cart.item_update(item).then(function(res) {
                    $rootScope.$emit('customer-cart-updated');

                    return res;
                });
            },

            item_remove : function(index) {
                return Bloomkit.Customer.Cart.item_remove(index).then(function(res) {
                    $rootScope.$emit('customer-cart-updated');

                    return res;
                });
            },

            get_count : function() {
                return Bloomkit.Customer.Cart.get_count();
            },

            clear_items : function() {
                return Bloomkit.Customer.Cart.clear_items();
            },

            clear_cookie : function() {
                $cookies.remove('customer_cart');
            },

            list_all : function() {
                return Bloomkit.Customer.Cart.list_all();
            },

            push_item : function(item) {
                return Bloomkit.Customer.Cart.push_item(item);
            },

            merge_item : function(item) {
                return Bloomkit.Customer.Cart.merge_item(item);
            }
        };

        return Control;
    });
