(function($) {

    $.fn.allchange = function (callback) {
        var me = this;
        var last = "";
        var infunc = function () {
            var text = $(me).val();
            if (text != last) {
                last = text;
                callback();
            }
            setTimeout(infunc, 100);
        }
        setTimeout(infunc, 100);
    };

})($);



function bk_is_empty(obj) {
    let bar;

    if(obj === false || ($.isArray(obj) && obj.length == 0)) {
        return true;
    }

    for (bar in obj) {
        if (obj.hasOwnProperty(bar)) {
            return false;
        }
    }

    return true;
}