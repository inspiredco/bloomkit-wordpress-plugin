
let WPAPI = (function() {
    let API = axios.create({
        baseURL: APP_CONFIG.api_url,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    });

    return {
        post : function(action, data, config) {
            if(typeof data !== 'object') {
                data = {};
            }

            data.action = action;

            return API.post('', Qs.stringify(data), config);
        }
    }
})();


let Customer_Cart = function() {
    let _cart = {};

    let Control = {
        load : function(cart) {
            console.log('CUSTOMER CART', JSON.parse(JSON.stringify(cart)));

            if(cart) {
                _cart = JSON.parse(JSON.stringify(cart));

                // if(!_.isEmpty(cart.items)) {
                //     Control.load_items(cart.items);
                // }
            } else {
                // No cart is available.
                _cart = false;
                Control.clear_items();
            }

            return Control;
        },

        load_items : function(items) {

            console.log('CUSTOMER CART ITEMS', JSON.parse(JSON.stringify(items)));

            Control.clear_items();

            if(items !== undefined) {
                _cart.items = items;
            }

            return Control;
        },


        get : function() {
            return _cart;
        },

        update : function(cart) {
            _cart = $.extend(_cart, cart);
        },

        get_id : function() {
            return _cart.cart_id;
        },

        get_item : function(idx) {
            return _cart.items[idx];
        },

        item_add : function(item) {
            let post = {
                'method' : 'item_add',
                'item' : item
            };

            return WPAPI.post('bloom_customer_cart', post).then(function(res) {
                if(res.data.success === true) {
                    Control.load(res.data.cart);
                }

                return res;
            });
        },

        item_update : function(item) {
            let post = {
                'method' : 'item_update',
                'item': {
                    'item_product_id' : item.item_product_id,
                    'item_product_variant_id' : item.item_product_variant_id,
                    'item_product_price_id' : item.item_product_price_id,
                    'item_quantity' : item.item_quantity,
                }
            };

            if(item.item_id) {
                post.item.item_id = item.item_id;
            }


            return WPAPI.post('bloom_customer_cart', post).then(function(res) {
                if(res.data.success === true) {
                    Control.load(res.data.cart);
                }

                return res;
            });
        },

        item_remove : function(index) {
            let post = {
                'method' : 'item_remove',
                'item' : Control.get_item(index)
            };

            return WPAPI.post('bloom_customer_cart', post).then(function(res) {
                if(res.data.success === true) {
                    Control.load(res.data.cart);
                }

                return res;
            });
        },

        get_count : function() {
            let total = 0;

            console.log(_cart);

            if(_cart.totals && _cart.totals.items) {
                total = _cart.totals.items;
            }

            if(!_.isEmpty(_cart.cart_total_items)) {
                total = _cart.cart_total_items;
            }

            return total;
        },

        clear_items : function() {
            _cart.items = [];
        },

        clear_cookie : function() {
            //$cookies.remove('customer_cart');
        },

        list_all : function() {
            return _cart.items;
        },

        push_item : function(item) {
            _cart.items.push(item);
            //_item_id_map[item.item_id] = (_cart.items.length - 1);
        },

        merge_item : function(item) {
            _cart.items[item.item_id] = $.extend(_cart.items[item.item_id], item);
        }
    };

    return Control;
};

let Customer = function() {
    let _cart = Customer_Cart();
    let _data = {};

    let _d_data = {
        'customer_pass_confirm' : ''
    };

    let Control = {
        Cart : _cart,

        load : function(data) {

            console.log('LOAD CUST', data);

            // if(_.isObject(data) && !_.isEmpty(data)) {
                _data = data;
            // }
        },

        get : function() {
            if(Control.get_id() === false) {
                return Object.assign(_d_data, _data);
            }

            return _data;
        },

        get_id : function() {
            return (_data && !_.isEmpty(_data.customer_id)) ? _data.customer_id : false;
        },

        is_auth : function() {
            return (Control.get_id() !== false);
        },

        update : function(data) {
            if(data) {
                _data = $.extend({}, _data, data);
            }

            return Control;
        },

        reset_pass : function() {
            let post = {};

            post.method = 'save';

            return WPAPI.post('bloom_customer_save', post).then(function(res) {
                if(res.data.success === true) {
                    if(res.data.customer) {
                        Control.update(res.data.customer);
                    }
                }

                return res;
            });
        },

        change_pass : function(post) {
            post.method = 'save';

            return WPAPI.post('bloom_customer', post).then(function(res) {
                if(res.data.success === true) {
                    if(res.data.customer) {
                        Control.update(res.data.customer);
                    }
                }

                return res;
            });
        },

        invite : function(post) {
            post.method = 'invite';

            return WPAPI.post('bloom_customer', post).then(function(res) {
                return res;
            });
        },

        follow : function(type, id, state) {
            if(state !== false) {
                state = true;
            }

            let post = {
                'method' : 'follow',
                'type' : type,
                'id' : id,
                'follow' : state
            };

            return WPAPI.post('bloom_customer', post).then(function(res) {
                return res;
            });
        },

        set_primary_address : function(address) {
            let post = {
                method : 'save',
                customer: {
                    'customer_default_address_shipping_id' : address
                }
            };

            return WPAPI.post('bloom_customer', post).then(function(res) {
                if(res.data.success === true) {
                    if(res.data.customer) {
                        Control.update(res.data.customer);
                    }
                }

                return res;
            });
        },

        register : function(data) {
            let post = {
                method : 'register',
                customer: data
            };

            return WPAPI.post('bloom_customer', post).then(function(res) {
                if(res.data.success === true) {
                    if(res.data.customer) {
                        Control.load(res.data.customer);
                    }
                }

                return res;
            });
        },

        save : function(data) {
            var post = {
                method : 'save',
                customer: data
            };

            return WPAPI.post('bloom_customer', post).then(function(res) {
                if(res.data.success === true) {
                    if(res.data.customer) {
                        Control.load(res.data.customer);
                    }
                }

                return res;
            });
        }
    };

    return Control;
};

let Products = function() {
    let Control = {
        search : function(post) {
            post.method = 'products_search';

            return WPAPI.post('bloom_shop', post).then(function(res) {
                return res;
            });
        }
    };

    return Control;
};

let Order = function() {
    let _data = {};

    let _d_data = {
        order_shipping_method_value : {
            address : {}
        },

        order_billing_method_value : {
            card : {}
        }
    };

    let Control = {
        load : function(data) {
            if(_.isObject(data) && _.isEmpty(data) === false) {
                _data = Object.assign(_d_data, data);
            }
        },

        get : function() {
            return _data;
        },

        get_id : function() {
            return _data.order_id || false;
        },

        is_complete : function() {
            return (_data.order_received === true || _data.order_received === 'Y');
        },

        has_shipping : function() {
            return (!_.isEmpty(_data.shipping) && !_.isEmpty(_data.shipping.method_id) && !_.isEmpty(_data.shipping.value));
        },

        has_billing : function() {
            return (!_.isEmpty(_data.billing) && !_.isEmpty(_data.billing.method_id) && !_.isEmpty(_data.billing.value));
        },

        get_shipping_value : function() {
            return _data.order_shipping_method_value;
        },

        get_billing_value : function() {
            return _data.order_billing_method_value;
        },

        get_token : function() {
            return _data.token || false;
        },

        update : function(data) {
            if(data) {
                _data = $.extend({}, _data, data);
            }

            return Control;
        },

        save : function(post) {
            return WPAPI.post('bloom_checkout', post).then(function(res) {
                if(res.data.success === true) {
                    if(res.data.order) {
                        Control.load(res.data.order);
                    }

                }

                return res;
            });
        }
    };

    return Control;
};

let Bloomkit = (function() {
    let _customer = Customer();

    console.log('INIT BK');

    let Control = {
        Customer : _customer,
    };

    return Control;
})();

Bloomkit.Shop = function(config) {
    let _products = Products();
    let _data = {};

    let Control = {
        Products: _products,

        load: function (data) {

        }
    };

    return Control;
};

Bloomkit.Checkout = function(config) {
    let _order = Order();
    let _data = {};

    let _config = {
        use_points : false,
        use_coupon : false
    };

    console.log('LOADING CO');


    let Control = {
        Order : _order,

        load : function(data) {

            console.log('LOADING ORDER', data);

            if(data.customer) {
                Bloomkit.Customer.load(data.customer);
            }

            if(data.order) {
                Control.Order.load(data.order);

                _config.use_points = data.order.order_config.use_loyalty_points;

                Bloomkit.Customer.Cart.load(data.order.cart)
            }


        },

        use_points : function(state) {
            _config.use_points = state;
        },

        using_points : function() {
            return _config.use_points;
        },

        apply_coupon : function(code) {
            _config.use_coupon = code;
        },

        save : function(post) {
            post.method = 'save';

            if(Control.get_order_token()) {
                post.token = Control.get_order_token();
            }

            //console.log('CO SUB CONFIG', _config);

            if(!post.order) {
                post.order = {};
            }

            if(post.order.use_points !== undefined) {
                Control.use_points(post.order.use_points);
            }

            //if(_config.use_points) {
                //post.order.use_points = _config.use_points;
            //}


            if(_config.use_coupon !== false) {
                post.order.coupon_code = _config.use_coupon;
            }

            return WPAPI.post('bloom_checkout', post).then(function(res) {
                if(res.data.success === true) {
                    Control.load(res.data);
                }

                return res;
            });
        },

        complete : function(post) {
            post.method = 'complete';

            if(Control.get_order_token()) {
                post.token = Control.get_order_token();
            }

            return WPAPI.post('bloom_checkout', post).then(function(res) {
                if(res.data.success === true) {
                    if(res.data.order) {
                        Control.Order.load(res.data.order);
                    }
                }

                return res;
            });
        },

        set_order : function(data) {
            Control.Order.load(data);

            return Control;
        },

        has_customer : function() {
            return Bloomkit.Customer.is_auth();
        },

        set_customer : function(data) {
            Bloomkit.Customer.load(data);

            return Control;
        },

        set_shipping_methods : function(methods) {
            _data.shipping_methods = methods
        },

        list_shipping_methods : function() {
            return _data.shipping_methods;
        },

        set_billing_methods : function(methods) {
            _data.billing_methods = methods
        },

        list_billing_methods : function() {
            return _data.billing_methods;
        },

        get : function() {
            return _data;
        },

        get_order_id : function() {
            return Control.Order.get_id() || false;
        },

        get_order_token : function() {
            return Control.Order.get_token() || false;
        },

        get_totals : function() {
            return Control.get().totals;
        },


        login : function(post) {
            return Control.save(post);
        }
    };

    return Control;
};

//Bloomkit.Checkout();
// let Shop = Bloomkit.Shop();
//
// Shop.Products.search({}).then(function(res) {
//     // res.data.products = <html from api>
//     console.log(res);
// });

// $(function() {
//     let Cart = Customer_Cart();
//
//     Customer.Cart.item_add({test: 'hello'}).then(function(res) {
//         console.log('CART ADD ITEM', res);
//     });
// });

