angular.module(Config.app_name)
    .controller('StoreCheckoutCtrl', function($rootScope, $scope, $timeout, $window, $location, CheckoutService, AppService) {

        let Step_State = function(config) {
            let _global = {};
            let _steps = [];

            let _d_global = {
                step : '',
                step_max : false,
                steps_complete : [],
            };

            let _d_step = {
                key: '',
                complete: false,
                loading: false,
                errors: [],
                hooks: [],
            };

            let Control = {
                init : function(run_config) {
                    let config = {};

                    if(_.isObject(run_config) && !_.isEmpty(run_config)) {
                        config = run_config;
                    }

                    _global = Object.assign(_d_global, config);
                },

                get_active_step : function() {
                    return _global.step;
                },

                step_load : function(step) {

                },


                step_is_completed : function(key) {
                    return (_global.steps_complete.indexOf(key) !== -1);
                },

                step_is_active : function(key) {
                    return (_global.step === key);
                },

                step_next : function(step_cur) {
                    let next = false;
                    let step_next = false;

                    _steps.forEach(function(step, key) {
                        if(step_next === false) {
                            if(next === true) {
                                step_next = key;
                            } else {
                                if(step_cur === key) {
                                    next = true;
                                }
                            }
                        }
                    });

                    return step_next;
                },

                step_max : function() {
                    let max = _global.step;

                    if(_global.steps_complete.length > 0) {
                        max = _global.steps_complete.slice(-1)[0];
                    }

                    return max;
                },

                step_submit : function() {
                    let key = _global.step;
                    let step = _steps[key];

                    if(_.isFunction(step.hooks.submit)) {
                        AppService.Loading.splash(true);

                        return step.hooks.submit().then(function(res) {

                            console.log(res);

                            AppService.Loading.splash(false);
                        });
                    }

                    return false;
                },

                step_complete : function(step) {
                    if(Control.step_is_completed(step) === false) {
                        _d_global.steps_complete.push(step);
                    }
                },

                step_add: function (step, config) {
                    let state = Object.assign({}, _d_step);

                    state.key = step;

                    if (!_.isEmpty(config.hooks)) {
                        state.hooks = config.hooks;
                    }

                    _steps[step] = state;
                },

                step_hook: function (step, on, fn) {

                },

                progress_percent : function() {
                    let count = (Object.keys(_steps).length) - 1;
                    let pos = _d_global.steps_complete.length;

                    return Math.ceil((pos/count) * 100);
                }
            };


            Control.init(config);


            return Control;
        };

        let step_config = {
            step : 'account',
            step_max : false,
            steps_complete : []
        };


        let co_load = {};

        if(!_.isEmpty(APP_LOAD.order)) {
            co_load.order = APP_LOAD.order;
        }

        CheckoutService.load(co_load);

        $scope.checkout_complete = false;


        let Steps = Step_State(step_config);

        if(CheckoutService.has_customer()) {
            Steps.step_complete('account');
        }

        if(CheckoutService.Order.has_shipping()) {
            Steps.step_complete('shipping');
        }

        if(CheckoutService.Order.has_billing()) {
            Steps.step_complete('billing');
        }

        if(CheckoutService.Order.is_complete()) {
            Steps.step_complete('review');
            Steps.step_complete('complete');

            $scope.checkout_complete = true;
        }

        if($scope.config.steps_complete.length > 0) {
            console.log('STEP', $scope.config.steps_complete.slice(-1)[0]);

            $scope.config.step_max = $scope.step_next($scope.config.steps_complete.slice(-1)[0]);
        }

        if($scope.order.order_received && $scope.order.order_received === 'Y') {
            $scope.config.steps_complete.push('review');
            $scope.config.steps_complete.push('complete');

            $scope.checkout_complete = true;
        }

        $scope.customer = Bloomkit.Customer.get();

        $scope.config = {
            step : 'account',
            step_max : false,
            steps : APP_LOAD.steps,
            steps_complete : [],
            step_errors: {},
            customer_registering : false,
            is_customer : ($scope.customer.customer_id !== undefined)
        };



        //


        console.log('Init Checkout', $scope.customer);

        $scope.order = CheckoutService.Order.get();

        //Bloomkit.Customer.Cart.load(APP_CONFIG.cart);

        //$scope.order = {};
        $scope.checkout_complete = false;
        $scope.totals = [];

        if(APP_LOAD.totals) {
            $scope.totals = APP_LOAD.totals;
        }

        // Load the available methods into the checkout
        if(APP_LOAD.shipping_methods) {
            CheckoutService.set_shipping_methods(APP_LOAD.shipping_methods);
        }

        if(APP_LOAD.billing_methods) {
            CheckoutService.set_billing_methods(APP_LOAD.billing_methods);
        }




        //
         console.log('ORDER', $scope.order);
        // console.log('CUSTOMER', $scope.customer);
        // console.log('CONFIG', $scope.config);
        // console.log('CART', $scope.cart);

        if(!$scope.customer.card) {
            $scope.customer.card = {
                card_holder_name : $scope.customer.customer_name_full,
                card_type : 'unknown'
            };
        }

        $scope.change_card_type = function(type) {
            $scope.customer.card.card_type = type;
        };

        $scope.field_options = {
            card: {
                creditCard: true,
                onCreditCardTypeChanged: $scope.change_card_type
            },

            expiry : {
                date: true,
                datePattern: ['m', 'y']
            }
        };

        let step_progress = {
            'account' : {
                submit : function() {
                    var post = {
                        customer: {
                            customer_name_first : $scope.customer.customer_name_first,
                            customer_name_last : $scope.customer.customer_name_last,
                            customer_email : $scope.customer.customer_email
                        }
                    };

                    if($scope.customer.customer_id === undefined) {
                        post.customer.customer_pass = $scope.customer.customer_pass;
                        post.customer.customer_pass_confirm = $scope.customer.customer_pass_confirm;
                    }

                    //console.log(post);
                    return CheckoutService.save(post).then(function(res) {
                        if(res.data.success === true) {
                            $scope.progress();
                        }

                        return res;
                    });
                }
            },

            'shipping' : {
                load : function() {
                    if(CheckoutService.list_shipping_methods().length === 1) {
                        $scope.order.order_shipping_method_id = CheckoutService.list_shipping_methods()[0].method_id;
                    }

                    console.log($scope.order.order_shipping_method_id);
                },

                submit : function() {
                    let value = $scope.order.order_shipping_method_value;
                    let post = {
                        order: {
                            order_shipping_method_id : $scope.order.order_shipping_method_id,
                            order_shipping_method_value: $scope.order.order_shipping_method_value
                        }
                    };

                    // Confirm shipping rate is selected before continue.
                    // todo: this is a quick hack, need to account for different shipping methods in the future
                    if(!bk_is_empty(value) && !bk_is_empty(value.rate_token)) {
                        return CheckoutService.save(post).then(function(res) {

                            if(res.data.success === true) {
                                $scope.progress();
                            }

                            return res;
                        });
                    } else {
                         $scope.config.steps['shipping'].errors = ['Shipping Rate must be selected!'];
                         $scope.config.step_errors = $scope.step_get_errors('shipping');
                    }

                    return false;
                }
            },

            'billing' : {
                load : function() {
                    if(CheckoutService.list_billing_methods().length === 1) {
                        $scope.order.order_billing_method_id = CheckoutService.list_billing_methods()[0].method_id;
                    }
                },

                submit : function() {
                    var post = {
                        order: {
                            order_billing_method_id : $scope.order.order_billing_method_id,
                            order_billing_method_value: $scope.order.order_billing_method_value
                        }
                    };

                    console.log('CO BILL', post, $scope.order);

                    return CheckoutService.save(post).then(function(res) {
                        if(res.data.success === true) {
                            $scope.progress();
                        }

                        return res;
                    });
                }
            },

            'review' : {
                submit : function() {
                    var post = {};

                    return CheckoutService.complete(post).then(function(res) {
                        if(res.data.success === true) {

                            $scope.checkout_complete = true;
                            $scope.config.steps_complete.push('complete');

                            $scope.progress();
                        }

                        return res;
                    });
                }
            },

            'complete' : {

            }
        };

        Steps.step_add('account', {
            hooks : step_progress.account
        });

        Steps.step_add('shipping', {
            hooks : step_progress.shipping
        });

        Steps.step_add('billing', {
            hooks : step_progress.billing
        });

        Steps.step_add('review', {
            hooks : step_progress.review
        });

        Steps.step_add('complete', {
            hooks : step_progress.complete
        });

        $scope.checkout_submit = function() {
            let submit = Steps.step_submit();

            if(submit !== false) {
                submit.then(function(res) {

                });
            }
        };

        $scope.step_get_errors = function(key) {
            if(key === undefined) {
                key = $scope.config.step;
            }

            var step = $scope.config.steps[key];

            console.log('ERROR DIS', key, step);

            if(step.errors !== false) {
                console.log('ERRORS DIS', step.errors);

                return step.errors;
            }

            return false;
        };

        $scope.step_has_errors = function(key) {
            if(key === undefined) {
                key = $scope.config.step;
            }

            return ($scope.config.steps[key].errors && $scope.config.steps[key].errors.length > 0);
        };


        $scope.registering_customer = function() {
            return (CheckoutService.has_customer() !== true);
        };

        $scope.checkout_login = function() {
            let post = {
                customer: {
                    'customer_login_email' : $scope.customer.customer_login_email,
                    'customer_login_pass' : $scope.customer.customer_login_pass
                }
            };

            return CheckoutService.login(post).then(function(res) {
                $scope.checkout_submit();
                return res;
            });
        };

        $scope.checkout_register = function() {
            $timeout(function() {
                $scope.config.customer_registering = true;
            });

        };

        $scope.change_shipping_method = function() {
            console.log('SHIP MEHTOD', $scope.order.order_shipping_method_value);

            $scope.order.order_shipping_method_value = {
                address : {},
                store: 0
            };

        };

        $scope.change_billing_method = function() {
            $scope.order.order_billing_method_value = {
                card : {}
            };
        };

        $scope.checkout_update = function() {
            $timeout(function() {
                $scope.customer = Bloomkit.Customer.get();
                $scope.order = CheckoutService.Order.get();
                $scope.totals = CheckoutService.Order.get_totals();
            });
        };

        $scope.checkout_submit = function() {
            //console.log('STEP', $scope.config.step);
            let step = $scope.config.step;


            if(step_progress[step].submit !== undefined) {
                let step_submit = step_progress[step].submit();

                if(step_submit !== false) {
                    AppService.Loading.splash(true);

                    step_submit.then(function(res) {
                        if(res.data.success === true) {

                            $timeout(function() {
                                $scope.totals = res.data.totals;
                                $scope.customer = Bloomkit.Customer.get();
                                $scope.order = CheckoutService.Order.get();
                            });


                            //$scope.checkout_update();

                            $scope.config.steps[step].errors = undefined;
                            $scope.config.step_errors = $scope.step_get_errors(step);

                            console.log(res);

                            if (!$scope.step_completed(step)) {
                                $scope.config.steps_complete.push(step);
                            }
                        } else {
                            $scope.config.steps[step].errors = res.data.errors;
                            $scope.config.step_errors = $scope.step_get_errors(step);

                            console.log('ERRORS', res.data.errors);
                        }

                        AppService.Loading.splash(false);
                    });
                }
            } else {
                $window.location.href = APP_CONFIG.base_url+'/account';
            }
        };

        $scope.step_next = function(step_cur) {
            var next = false;
            var step_next = false;

            $.each($scope.config.steps, function(key, step) {
                if(step_next === false) {
                    if(next === true) {
                        step_next = key;
                    } else {
                        if(step_cur === key) {
                            next = true;
                        }
                    }
                }
            });

            console.log('next', step_next);

            return step_next;
        };

        $scope.step_activate = function(key) {

        };

        $scope.progress = function() {
            var step_next = $scope.step_next($scope.config.step);

            if(step_next !== false) {
                if(!$scope.step_completed(step_next)) {
                    $scope.config.step_max = step_next;
                }

                $scope.step_change(step_next);
            }
        };

        $scope.step_complete = function(key) {
            $scope.config.steps_complete.push('shipping');
            $scope.config.step_max = 'billing';
        };

        $scope.step_change = function(key) {
            if($scope.step_completed(key) === true || key === $scope.config.step_max) {
                $location.hash(key);
            }
        };

        $scope.step_switch = function(key) {
            console.log('SWITCHING', key, 'completed:', $scope.step_completed(key));
            console.log($scope.config);

            if($scope.checkout_complete !== true) {
                $scope.step_change(key);
            }

        };

        $scope.step_load = function(key) {
            if(key !== undefined && key !== '') {
                $timeout(function () {
                    $scope.config.step = key;

                    var complete = false;

                    if(step_progress[key].load !== undefined) {
                        step_progress[key].load();
                    }

                    //$scope.config.step_errors = $scope.step_get_errors(key);

                    $.each($scope.config.steps, function(key, step) {
                        if(complete === true) {
                            if(!$scope.step_completed(key)) {
                                $scope.config.steps_complete.push(key);
                            }
                        }

                        if(key === $scope.config.step) {
                            complete = false;
                        }

                        $scope.config.steps[key].complete = complete;
                    });
                });
            }
        };

        $scope.step_active = function(key) {
            return Steps.step_is_active(key)
        };

        $scope.step_completed = function(key) {
            return Steps.step_is_completed(key)
        };

        $scope.step_classes = function(key) {
            let classes = {
                active : false,
                complete : false
            };

            if(Steps.step_is_active(key) === true) {
                classes.active = true;
            }

            if(Steps.step_is_completed(key) === true) {
                classes.complete = true;
            } else {
                if(Steps.step_max() === key) {
                    classes.active = true;
                }
            }

            return classes;
        };

        $scope.progress_bar_width = function() {
            let width = Steps.progress_percent();

            if(width > 100) {
                width = 100;
            }

            return {
                width : width+'%'
            };
        };


        if(CheckoutService.has_customer()) {
            $scope.config.steps_complete.push('account');
        }

        if($scope.order.shipping && $scope.order.shipping.method_id !== undefined && $scope.order.shipping.value !== undefined) {
            $scope.config.steps_complete.push('shipping');
        }

        if($scope.order.billing && $scope.order.billing.method_id !== undefined && $scope.order.billing.value !== undefined) {
            $scope.config.steps_complete.push('billing');
        }

        if($scope.config.steps_complete.length > 0) {
            console.log('STEP', $scope.config.steps_complete.slice(-1)[0]);

            $scope.config.step_max = $scope.step_next($scope.config.steps_complete.slice(-1)[0]);
        }

        if($scope.order.order_received && $scope.order.order_received === 'Y') {
            $scope.config.steps_complete.push('review');
            $scope.config.steps_complete.push('complete');

            $scope.checkout_complete = true;
        }

        console.log($scope.config);

        $window.addEventListener('load', function() {
            var key = $scope.config.step_max;

            if($location.hash() !== undefined) {
                key = $location.hash();
            }

            $scope.step_load(key);
        });

        $window.addEventListener('hashchange', function() {
            $scope.step_load($location.hash());
        });
    })

    .directive('bloomCheckoutPaypalExpress', function() {
        return {
            restrict: 'A',
            templateUrl: Config.app_path+'views/checkout/paypal_express.html',
            scope : {
            },
            link : function(scope, element, attrs) {

                angular.element(document).ready(function () {
                    paypal.Button.render({
                        env: 'sandbox', // Or 'sandbox',

                        commit: true, // Show a 'Pay Now' button

                        style: {
                            color: 'gold',
                            size: 'small'
                        },

                        payment: function(data, actions) {
                            /*
                             * Set up the payment here
                             */
                        },

                        onAuthorize: function(data, actions) {
                            /*
                             * Execute the payment here
                             */
                        },

                        onCancel: function(data, actions) {
                            /*
                             * Buyer cancelled the payment
                             */
                        },

                        onError: function(err) {
                            /*
                             * An error occurred during the transaction
                             */
                        }
                    }, '#paypal-button');
                });
            }
        }
    })


    .directive('checkoutShippingRateSelector', function($rootScope, CheckoutService, $timeout) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/checkout/address_rate_selector.html',
            //transclude: true,
            scope : {
                value : '='
            },
            link : function(scope) {
                scope.order = CheckoutService.Order.get();

                console.log(scope.value);

                scope.state = {
                    loading: false,
                    has_rates : false,
                    has_error : false,
                    error: ''
                };

                if(!scope.value){
                    scope.value = {};
                }

                if(!scope.value.address){
                    scope.value.address = {};
                }

                if(scope.value.rates){
                    scope.state.has_rates = true;
                }

                angular.element(document).ready(function () {
                    //scope.order = CheckoutService.Order.get();
                    scope.order = CheckoutService.Order.get();

                    console.log('RATES ORDER', scope.order);
                });

                scope.address_config = {
                    save : function(address) {
                        scope.value.address = address;
                        console.log('Saving address');
                        return scope.get_rates();
                    },

                    select : function(id) {
                        if(id === 'new') {
                            scope.state.has_rates = false;
                            scope.value.rates = [];
                        }
                    }
                };

                scope.get_rates = function() {
                    scope.state.loading = true;
                    scope.state.has_error = false;
                    scope.state.has_rates = false;
                    scope.state.error = '';

                    scope.value.rates = [];
                    scope.value.rate_token = false;

                    //console.log('RATES GET', scope.order, Bloomkit.Checkout);

                    let post = {
                        get_rates : true,
                        order: {
                            order_shipping_method_id : scope.order.order_shipping_method_id,
                            order_shipping_method_value: scope.value
                        }
                    };

                    return CheckoutService.save(post).then(function(res) {
                        $timeout(function() {
                            if(res.data.success === true) {
                                scope.order = CheckoutService.Order.get();

                                scope.value.rates = res.data.order.shipping.value.rates;
                                scope.state.has_rates = true;

                                res.data.address = res.data.order.shipping.value.address;
                            } else {
                                scope.state.has_rates = true;
                                scope.state.has_error = true;
                                scope.state.error = res.data.errors[0];
                            }

                            scope.state.loading = false;
                        });


                        return res;
                    });
                };

                // crtl.value = {
                //     address : {}
                // };



                $rootScope.$on('checkout-address-updated', function(event, data) {
                    console.log(event, data);
                });

                //console.log(scope.address_config());

                // $scope.$watch('address', function (newValue, oldValue) {
                //     console.log('ADDR CHANGE');
                //     console.log(newValue, oldValue);
                // }, true);

                // scope.address_save = function() {
                //     console.log($scope.value);
                //
                //     var post = {
                //         get_rates : true,
                //         order: {
                //             order_shipping_method_id : scope.order.order_shipping_method_id,
                //             order_shipping_method_value: scope.value
                //         }
                //     };
                //
                //     console.log(post);
                //
                //     CheckoutService.save(post).then(function(res) {
                //         $timeout(function() {
                //             CheckoutService.Order.load(res.data.order);
                //
                //             console.log('ORDER SAVE', scope.order, scope.rates);
                //         });
                //
                //     });
                // };
            }
        }
    })

    .directive('checkoutCustomerAddressSelector', function(CheckoutService, $timeout) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/checkout/address_selector.html',
            scope : {
                address : '=',
                config : '=?'
            },
            link : function(scope) {
                console.log(scope);
                console.log(scope.config);

                scope.order = CheckoutService.Order.get();

                if(scope.config === undefined) {
                    scope.config = {};
                }

                scope.config.new_address = false;
                scope.config.loading = false;

                scope.selected = {
                    id : '0',
                    address : {}
                };


                scope.address_new = function() {
                    $timeout(function() {
                        scope.config.new_address = true;

                        scope.selected.id = 'new';

                        scope.address = {
                            address_street_1: '',
                            address_street_2: '',
                            address_city: '',
                            address_region: '',
                            address_country: '',
                            address_post_code: ''
                        };
                    });

                };

                if(scope.address !== undefined) {
                    scope.selected.id = ''+scope.address.address_id;
                    scope.selected.address = scope.address;

                    console.log(scope.selected);

                } else {
                    scope.address_new();
                }


                scope.addresses = Bloomkit.Customer.get().addresses;

                angular.element(document).ready(function () {
                   //$timeout(function() {
                        scope.addresses = Bloomkit.Customer.get().addresses;
                    //});

                    if(!scope.addresses || scope.addresses.length === 0) {
                        scope.address_new();
                    }
                });


                scope.has_save_fn = function() {
                    return (scope.config.save !== undefined);
                };

                scope.is_selected = function(id) {
                    return ((parseInt(id) == scope.address.address_id));
                };


                scope.address_save = function() {

                    if(scope.config.save !== undefined) {
                        if(scope.selected.id === 'new') {
                            scope.config.loading = true;
                        }

                        scope.config.save(scope.address).then(function(res) {

                            $timeout(function() {
                                scope.addresses = Bloomkit.Customer.get().addresses;

                                if(res.data.success === true) {
                                    if(res.data.address !== undefined) {
                                        scope.selected.id = ''+res.data.address.address_id;
                                        scope.address = res.data.address;

                                        scope.config.new_address = false;
                                    }
                                } else {
                                    $.each(res.data.errors, function(field, error) {
                                        $('input[name="'+field+'"]').css({
                                            'border' : '1px solid red'
                                        });

                                        console.log(field, error);
                                    });
                                }

                                scope.config.loading = false;
                            });


                        });
                    }

                };

                scope.address_select = function() {
                    if(scope.config.select !== undefined) {
                        scope.config.select(scope.selected.id);
                    }

                    if(scope.selected.id === 'new') {
                        scope.address_new();
                    } else {
                        scope.addresses.forEach(function(a, i) {
                            if(a.address_id == scope.selected.id) {
                                scope.address = a;
                            }
                        });

                        if(scope.config.save !== undefined) {
                            scope.address_save();
                        }

                        scope.config.new_address = false;
                    }
                };
            }
        }
    })

    .directive('checkoutCustomerCardSelector', function(CheckoutService, $timeout) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/checkout/card_selector.html',
            scope : {
                card : '='
            },
            link : function(scope, element, attrs) {
                scope.config = {
                    new_card : false
                };

                scope.selected = {
                    id : '0',
                    card : {}
                };

                console.log(scope.card);

                // scope.address_config = {
                //     save : function(address) {
                //         scope.card.address = address;
                //         console.log('Saving address');
                //         return scope.get_rates();
                //     },
                //
                //     select : function(id) {
                //         if(id === 'new') {
                //             scope.state.has_rates = false;
                //             scope.value.rates = [];
                //         }
                //     }
                // };

                scope.cards = Bloomkit.Customer.get().cards;

                if(scope.card !== undefined) {
                    if(scope.card.card_id !== undefined) {
                        scope.selected.id = scope.card.card_id;
                        scope.selected.card = scope.card;
                    } else {
                        scope.config.new_card = true;
                    }
                } else {
                    scope.card = {
                        card_number : '',
                        card_expiry : '',
                        card_holder_name : '',
                        card_ccv : ''
                    };
                }

                if(scope.card.address === undefined) {
                    scope.card.address = {};
                }

                console.log('CARD ADDR', scope.card);



                angular.element(document).ready(function () {
                    scope.cards = Bloomkit.Customer.get().cards;
                });



                if(!scope.cards || scope.cards.length === 0) {
                    scope.config.new_card = true;
                }


                scope.is_selected = function(id) {
                    if(scope.card) {
                        return (parseInt(id) == scope.card.card_id);
                    }

                    return false;
                };

                scope.select = function() {
                    if(scope.selected.id === 'new') {
                        scope.config.new_card = true;

                        scope.card = {
                            card_save : 1
                        };

                        if(scope.card.address === undefined) {
                            scope.card.address = {};
                        }
                    } else {
                        scope.config.new_card = false;

                        scope.card.card_id = scope.selected.id;
                        // scope.cards.forEach(function(a, i) {
                        //     if(a.card_id == scope.selected.id) {
                        //         scope.card = a;
                        //     }
                        // });
                    }
                };
            }
        }
    })

    .controller('CheckoutCartCtrl', function($rootScope, $scope, $window, CustomerService) {
        if(APP_LOAD.cart) {
            CustomerService.Cart.load(APP_LOAD.cart);
        }


        $scope.cart = CustomerService.Cart.get();

        $scope.step_submit = function() {
            $window.location.href = '/checkout';
        };

        $rootScope.$on('customer-cart-updated', function() {
            //$scope.cart = CustomerService.Cart.get();

            console.log("CART22", $scope.cart);
        });
    });
