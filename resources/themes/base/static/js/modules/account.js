angular.module(Config.app_name)

    .controller('CustomerRegisterContestCtrl', function($window, $location, $timeout, $scope, $mdDialog, AppService, CustomerService) {
        $scope.fields = CustomerService.get();
        $scope.errors = [];

        $scope.config = {
            complete : false,
            header : {
                title : 'Claim Your Prize!',
                text : 'Thank you for inviting your friends'
            }
        };

        $scope.fields.newsletter_subscribe = true;

        $scope.display_errors = function(errors) {
            console.log(errors);

            $scope.errors = errors;
        };

        $scope.stage_success = function() {
            return ($scope.config.success);
        };

        $scope.submit = function() {
            let post = {
                step : 'account'
            };

            if(APP_LOAD.winner_token) {
                post.winner_token = APP_LOAD.winner_token;
            }

            if($scope.fields.newsletter_subscribe === true) {
                post.newsletter_subscribe = true;
            }

            var fields = ['customer_name_first', 'customer_name_last', 'customer_email', 'customer_pass', 'customer_pass_confirm'];

            fields.forEach(function(key) {
                post[key] = $scope.fields[key];
            });

            AppService.Loading.splash(true);

            return CustomerService.save(post).then(function (res) {

                $timeout(function() {
                    if (res.data.success === true) {
                        $scope.config.success = true;
                    } else {
                        $scope.display_errors(res.data.errors);
                    }
                });



                AppService.Loading.splash(false);

                return res;
            });

            //console.log($scope.customer);
        };
    })

    .controller('CustomerRegisterCtrl', function($window, $location, $timeout, $scope, $mdDialog, AppService, CustomerService) {
        $scope.fields = CustomerService.get();
        $scope.errors = [];

        $scope.config = {
            complete : false,
            header : {
                title : 'Become A Member Today!',
                text : ''
            },
            actions : [
                {

                }
            ],
            step : 'account',
            steps_complete : [],
            steps : {
                'account' : {
                    title : 'Account'
                },

                'experience' : {
                    title : 'Experience'
                },

                'success' : {
                    title : 'Get Started'
                }
            }
        };

        $scope.fields.newsletter_subscribe = true;

        console.log('INVITE', APP_LOAD.invite_token);

        $scope.display_errors = function(errors) {
            console.log(errors);

            $scope.errors = errors;
        };

        $scope.stage_success = function() {
            return ($scope.config.success);
        };

        $scope.submit = function() {
            var post = {
                step : 'account'
            };

            if(APP_LOAD.invite_token) {
                post.invite_token = APP_LOAD.invite_token;
            }

            if($scope.fields.newsletter_subscribe === true) {
                post.newsletter_subscribe = true;
            }

            var fields = ['customer_name_first', 'customer_name_last', 'customer_email', 'customer_pass', 'customer_pass_confirm'];

            fields.forEach(function(key) {
                post[key] = $scope.fields[key];
            });

            AppService.Loading.splash(true);

            return CustomerService.register(post).then(function (res) {

                console.log('CUSTOMER REGISTERED', res);

                $timeout(function() {
                    if (res.data.success === true) {
                        $scope.config.success = true;
                    } else {
                        $scope.display_errors(res.data.errors);
                    }
                });



                AppService.Loading.splash(false);

                return res;
            });

            //console.log($scope.customer);
        };
    })

    .controller('LoginCtrl', function($scope, $mdDialog, CustomerService, AppService) {
        $scope.customer = CustomerService.get();

        $scope.forgot_pass = function() {
            $mdDialog.show({
                controller: 'LoginForgotPassDialog',
                templateUrl: Config.app_path+'views/account/dialog_forgot_pass.html',
                clickOutsideToClose:true,
                locals : {}
            }).then(function(folder) {
                console.log(folder);
            }, function() {
                $scope.status = 'You cancelled the dialog.';
            });
        };
    })

    .controller('LoginForgotPassDialog', function($scope, $timeout, $mdDialog, APIService) {
        $scope.dialog_config = {
            title : 'Forgot Your Password',
            save_btn_text : 'Reset Password'
        };

        $scope.submit = function() {
            var post = {
                'method' : 'reset_pass',
            };

            APIService.post('bloom_account', post).then(function(res) {
                console.log(res);
                //scope.items = res.data.orders;
            });

            //$timeout(function() {
            //console.log($scope.image_data);

            //$mdDialog.hide($scope.image_data);
            //});

        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };
    })

    .controller('AccountDashCtrl', function($scope, $mdDialog, CustomerService, AppService) {
        $scope.customer = CustomerService.get();

        $scope.invite_friends = function() {
            $mdDialog.show({
                controller: 'AccountInviteDialog',
                templateUrl: Config.app_path+'views/account/dialog_invite.html',
                clickOutsideToClose:true,
                locals : {}
            }).then(function(folder) {
                console.log(folder);
            }, function() {
                $scope.status = 'You cancelled the dialog.';
            });
        };

        $scope.change_email = function() {
            $mdDialog.show({
                controller: 'AccountEditEmailDialog',
                templateUrl: Config.app_path+'views/account/dialog_edit_email.html',
                clickOutsideToClose:true,
                locals : {}
            }).then(function(folder) {
                console.log(folder);
            }, function() {
                $scope.status = 'You cancelled the dialog.';
            });
        };
    })

    .controller('AccountEditProfileCtrl', function($scope, $mdDialog, AppService, CustomerService) {
        $scope.fields = CustomerService.get();

        console.log($scope.customer);

        $scope.change_pass = function() {
            $mdDialog.show({
                controller: 'AccountEditPasswordDialog',
                templateUrl: Config.app_path+'views/account/password_dialog.html',
                clickOutsideToClose:true,
                locals : {}
            }).then(function(folder) {
                console.log(folder);
            }, function() {
                $scope.status = 'You cancelled the dialog.';
            });
        };

        $scope.submit = function() {
            var post = {
                customer_name_first : $scope.fields.customer_name_first,
                customer_name_last : $scope.fields.customer_name_last,
                customer_gender : $scope.fields.customer_gender,
                customer_date_birth : $scope.fields.customer_date_birth
            };

            AppService.Loading.splash(true);

            CustomerService.save(post).then(function(res) {
                AppService.Loading.splash(false);

                if(res.data.success) {
                    AppService.Notify.show('success', 'Your account has been updated successfully!');
                }

                console.log(res);
            });
        }

    })

    .controller('AccountEditEmailDialog', function($scope, $timeout, $mdDialog) {
        $scope.dialog_config = {
            title : 'Update Email Address',
            save_btn_text : 'Update Email'
        };

        $scope.submit = function() {
            //$timeout(function() {
            console.log($scope.image_data);

            $mdDialog.hide($scope.image_data);
            //});

        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };
    })

    .controller('AccountEditPasswordDialog', function($scope, $timeout, $mdDialog, CustomerService) {
        $scope.fields = {};

        $scope.dialog_config = {
            title : 'Update Password',
            save_btn_text : 'Update Password'
        };


        $scope.submit = function() {
            var post = $scope.fields;

            $scope.dialog_config.loading = true;

            return CustomerService.change_pass(post).then(function(res) {
                if(res.data.success === true) {
                    $timeout(function () {
                        $scope.dialog_config.success = true;

                        $timeout(function () {
                            $mdDialog.hide(res);
                        }, 1000);
                    }, 1000);
                } else {
                    $scope.dialog_config.loading = false;
                }

                return res;
            });
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };
    })

    .controller('AccountOrdersCtrl', function($scope, CustomerService) {

    })

    .directive('bloomCustomerOrdersTable', function(APIService) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/account/customer_orders_table.html',
            scope : {

            },
            link : function(scope, element, attrs) {
                scope.config = {
                    base_url : APP_CONFIG.base_url+'/account/orders/view'
                };

                scope.items = [];

                scope.fetch_orders = function() {
                    var post = {
                        'method' : 'orders_search',
                        'status' : 1,
                        'order_by' : 'date',
                        'order' : 'DESC'
                    };

                    APIService.post('bloom_account', post).then(function(res) {
                        scope.items = res.data.orders;
                    });
                };

                scope.fetch_orders();
            }
        }
    });
