angular.module(Config.app_name)
    .controller('BrandProfileCtrl', function($scope, $timeout, $window, $location, $mdDialog) {
        $scope.write_review = function() {
            $mdDialog.show({
                controller: 'AddReviewDialog',
                templateUrl: Config.app_path+'views/review/add_review_dialog.html',
                clickOutsideToClose:true,
                locals : {

                }
            }).then(function(folder) {
                console.log(folder);
            }, function() {
                $scope.status = 'You cancelled the dialog.';
            });
        };
    });
