

// $(function() {
//     $(document).foundation();
// });


var Bloom = angular.module(Config.app_name, [
        'ngMaterial',
        'ngSanitize',
        'ngCookies',
        'in-viewport',
        //'ngMap',
        'cleave.js'
    ], function($interpolateProvider) {
        $interpolateProvider.startSymbol('%%');
        $interpolateProvider.endSymbol('%%');
    })

    .config(function($mdThemingProvider) {
        $mdThemingProvider.theme('altTheme').primaryPalette('green');
    })

    .run(function($rootScope, $window, $mdDialog, $mdSidenav, AppService, CustomerService, CustomerCartService, PrimoCoverService) {
        $rootScope.show_mobile_menu = false;
        $rootScope.show_cart_menu = false;

        CustomerService.load(APP_CONFIG.customer);

        //console.log('APP_CONFIG', APP_CONFIG);
       console.log('APP_CONFIG RAW', JSON.parse(JSON.stringify(APP_CONFIG)));
       console.log('APP_LOAD RAW', JSON.parse(JSON.stringify(APP_LOAD)));

        if(!_.isEmpty(APP_CONFIG.cart)) {
            //CustomerCartService.load(APP_CONFIG.cart);
            Bloomkit.Customer.Cart.load(APP_CONFIG.cart);
        }


        angular.element(document).ready(function () {
            AppService.Loading.splash(false);
        });

        $rootScope.customer_is_auth = function() {
            return CustomerService.is_auth();
        };

        $rootScope.toggle_cart_menu = function() {
            $rootScope.show_cart_menu = (!$rootScope.show_cart_menu);
        };

        $rootScope.toggle_mobile_menu = function() {
            $rootScope.show_mobile_menu = (!$rootScope.show_mobile_menu);
        };

        $rootScope.open_signup = function() {
            PrimoCoverService.open();
        };



    })


    .controller('BloomProductIndexCtrl', function($scope, $window, $location, $timeout, $mdDialog, AppService, CustomerService, StoreService) {
        //StoreService.set_token(STORE_TOKEN);

        let Shop = Bloomkit.Shop();

        $scope.viewer_config = {
            page: 1,
            per_page : 8,
            filters : {
                //brand: 14
            }
        };


        if(APP_LOAD.category) {
            $scope.viewer_config.filters.category = APP_LOAD.category.cat_id;
        }



        $scope.config = {
            pages : [],
            page : 1,
            loaded : false,
            cat_rules : {
                '0' : { show_strains: true, show_effects: true },
                '2' : { show_strains: true, show_effects: true },
                '3' : { show_strains: false, show_effects: false }
            },
            filters : {
                category : 0,
                sub_cats : [],
                strains : [],
                effects : [],
                search : ''
            }
        };

        if(APP_LOAD.search_text !== undefined) {
            $scope.viewer_config.filters.search = APP_LOAD.search_text;
        }

        $scope.filters_config = {
            
            filters : {
                'price' : {

                }    
            }
        };

        if(APP_LOAD.categories) {
            $scope.categories = APP_LOAD.categories;
        }


        if($scope.categories && $scope.categories.length > 0) {
            _.each($scope.categories, function(cat, i) {
                if(cat.category.cat_title === 'Strains') {
                    $scope.strains = cat.children;
                }

                if(cat.category.cat_title === 'Effects') {
                    $scope.effects = cat.children;
                }
            });
        }


    })

    .controller('BloomProductSingleCtrl', function($scope, AppService, CustomerService, ProductService, $mdDialog) {
        var view_state = {
            review : false
        };

        var _write_review = false;

        $scope.product = ProductService.load(APP_CONFIG.product).get();


        console.log($scope.product);

        $scope.detail_tab = 'overview';

        $scope.switch_detail_tab = function(tab) {
            $scope.detail_tab = tab;
        };

        $scope.is_detail_tab_active = function(tab) {
            return (tab === $scope.detail_tab);
        };

        $scope.cart_attr_fields = {};

        $scope.cart_fields = {
            item_product_id : ProductService.get_id(),
            item_quantity: 1,
            item_product_price_id : '0',
            item_product_variant_id : '0',
            item_product_variant_index : '0',
            item_product_price_index : '0'
        };

        $scope.review_fields = {
            display_name : false,
            rating: 5,
            comment: ''
        };

        if($scope.product.product_is_variation) {
            $scope.cart_fields.item_product_variant_id = $scope.product.variations[0].variant_id;
        } else {
            if($scope.product.prices && $scope.product.prices[0]) {
                $scope.cart_fields.item_product_price_id = $scope.product.prices[0].price_id;
            }
        }

        $scope.get_cart_price = function() {
            var price;

            if($scope.product.product_is_variation) {
                price = $scope.product.variations[$scope.cart_fields.item_product_variant_index].variant_cost;
            } else {
                if($scope.product.prices && $scope.product.prices[$scope.cart_fields.item_product_price_index]) {
                    price = $scope.product.prices[$scope.cart_fields.item_product_price_index].price_cost;
                }
            }

            return accounting.formatMoney((price * $scope.cart_fields.item_quantity), '');
        };

        //$scope.get_cart_price();

        $scope.product_change_variant = function() {
            $scope.cart_fields.item_product_variant_id = $scope.product.variations[$scope.cart_fields.item_product_variant_index].variant_id;
        };

        $scope.product_change_price = function() {
            if($scope.product.prices && $scope.product.prices[$scope.cart_fields.item_product_price_index]) {
                $scope.cart_fields.item_product_price_id = $scope.product.prices[$scope.cart_fields.item_product_price_index].price_id;
            }
        };

        $scope.cart_item_add = function() {
            console.log($scope.cart_fields);

            CustomerService.Cart.item_add($scope.cart_fields).then(function(res) {
                $scope.cart_fields.item_quantity = 1;

                $mdDialog.show({
                    templateUrl: Config.app_path+'views/common/success_dialog.html',
                    parent: angular.element(document.getElementsByClassName('site_wrap')),
                    clickOutsideToClose:true,
                    locals : {},
                    controller : function($scope, $window, $mdDialog) {
                        $scope.continue = function() {
                            $mdDialog.hide();
                        };

                        $scope.goto_checkout = function() {
                            $window.location.href = APP_CONFIG.base_url+'/checkout';
                        };
                    }
                });
            });
        };

        $scope.cart_item_selected = function(index) {
            return ($scope.cart_fields.item_product_price_index == index);
        };

        $scope.filter_options = function(options) {
            return options;
            // return _.map(options, function(option, i) {
            //     return _.each($scope.product.variations, function(variant, i) {
            //         return _.each(variant.attributes, function(attr, i) {
            //
            //         });
            //     });
            // });
        };

        $scope.write_review = function() {
            $mdDialog.show({
                controller: 'AddReviewDialog',
                templateUrl: Config.app_path+'views/review/add_review_dialog.html',
                clickOutsideToClose:true,
                locals : {
                    config : {}
                }
            }).then(function(res) {
                console.log('HIDE', res);

                if(res.data.success === true) {
                    $('#product_rating_label').fadeOut(200, function() {
                        $(this).html(res.data.rating.average).fadeIn(200);
                        $('#product_rating_count').html(res.data.rating.count)
                    });
                }
            });
        };

    })

    .controller('AddReviewDialog', function($scope, $timeout, $mdDialog, config) {
        $scope.dialog_config = {
            title : 'Update Email Address',
            save_btn_text : 'Update Email',
            success_msg : 'Thanks!',
            loading: false,
            complete : false
        };

        $scope.fields = {
            comment: '',
            ratings : {},
            images : []
        };

        $scope.config = {
            include_rating_categories : true,
            on_submit : function(submit_fn) {
                $scope.dialog_config.loading = true;

                //$timeout(function() {

                return submit_fn().then(function(res) {
                    if(res.data.success === true) {
                        $timeout(function() {
                            $scope.dialog_config.success = true;
                            $scope.dialog_config.loading = false;

                            $timeout(function() {
                                $mdDialog.hide(res);
                            }, 1000);
                        }, 1000);
                    } else {
                        $scope.dialog_config.loading = false;
                    }

                    return res;
                });

                //$mdDialog.hide();
                //});

            }
        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };
    })

    .filter('isEmpty', function () {
        var bar;
        return function (obj) {
            if(obj === false || ($.isArray(obj) && obj.length == 0)) {
                return true;
            }

            for (bar in obj) {
                if (obj.hasOwnProperty(bar)) {
                    return false;
                }
            }

            return true;
        };
    })

    .filter('formatPriceDisplay', function(){
        return function(amt){
            if(amt === '') {

            }
        };
    })

    .filter('padLength', function(){
        return function(len, pad){
            if(len == 0) return '';
            else{
                pad = (pad || 0).toString();
                return new Array(1 + len).join(pad);
            }
        };
    });;
