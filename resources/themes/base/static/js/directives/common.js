function gcd (a, b) {
    return (b == 0) ? a : gcd (b, a%b);
}

function calculateAspectRatioFit(srcWidth, srcHeight, maxWidth, maxHeight) {

    var ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);

    return { width: srcWidth*ratio, height: srcHeight*ratio };
}

angular.module(Config.app_name)

    .directive('bloomCreditCardForm', function($timeout) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/common/credit_card_form.html',
            scope : {
                card : '='
            },
            link : function(scope, element, attrs) {
                if(scope.card === undefined) {
                    scope.card = {
                        card_save : ''
                    };
                }

                console.log('CARD FORM', scope.card);

                // if(scope.card.address === undefined) {
                //     scope.card.address = {};
                // }

                scope.config = {
                    new_card : false,
                    show_form: false
                };

                scope.card_number = {
                    rawValue : '',
                    creditCardType: ''
                };

                if(scope.card.card_number !== '') {
                    scope.card_number.rawValue = scope.card.card_number;
                }

                scope.card_type_changed = function(type) {
                    scope.card_number.creditCardType = type;
                };

                scope.onCreditCardValueChanged = function(e) {
                    scope.card.card_number = e.target.rawValue;
                };

                scope.field_configs = {
                    cc : {
                        creditCard: true,
                        onValueChanged : scope.onCreditCardValueChanged,
                        onCreditCardTypeChanged: scope.card_type_changed
                    },
                    expire : {
                        date: true,
                        delimiter: ' / ',
                        datePattern: ['m', 'y']
                    }
                };



                angular.element(document).ready(function () {
                    $timeout(function() {
                        scope.config.show_form = true;
                    });
                });
            }
        }
    })

    .directive('bloomAddressDisplay', function() {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/common/address_display.html',
            scope : {
                address : '='
            },
            link : function(scope, element, attrs) {

            }
        }
    })

    .directive('productImageGallery', function() {
        return {
            restrict: 'E',
            scope : {
                images : '=',
                cover : '='
            },
            templateUrl: Config.app_path+'views/products/product_image_gallery.html',
            link : function(scope, element, attrs) {

                scope.config = {
                    stage : {
                        width: 0,
                        height: 0
                    }
                };

                var Control = {
                    _elems : {},
                    _config : {},

                    init : function() {
                        var self = this;

                        this._config.active_item = false;

                        this._config.nav = {
                            cur_step : 0,
                            max_step : 0,
                            image_count : 0
                        };

                        this._elems.stage = $(element).eq(0);
                        this._elems.nav = $('[role="navigation"]', this._elems.stage).eq(0);
                        this._elems.nav_view = $('[role="viewport"]', this._elems.nav).eq(0);
                        this._elems.nav_view_track = $('[role="viewport-track"]', this._elems.stage).eq(0);

                        this._elems.nav_btn_prev = $('.prev', this._elems.nav).eq(0);
                        this._elems.nav_btn_next = $('.next', this._elems.nav).eq(0);


                        if(scope.images && scope.images.length < 1) {
                            this._elems.nav.hide();
                        }

                        this.set_sizes();

                        $(window).on('resize', function() {
                            Control.set_sizes();
                        });

                        $(this._elems.nav).on('click', 'a', function(evt) {
                            var btn = $(this);
                            var direction = (btn.attr('role') === 'scroll-next') ? 'next' : 'prev';

                            console.log(btn.attr('role'));

                            self.scroll(direction);
                        });

                        $(this._elems.nav_view_track).on('click', '.thumb_item', function(evt) {
                            if(self._config.active_item !== false) {
                                self._config.active_item.removeClass('active');
                            }

                            self._config.active_item = $(this);
                            self._config.active_item.addClass('active');
                        });

                        $('a[data-rel^=lightcase]', this._elems.stage).lightcase();

                        console.log(this._elems);
                    },

                    set_sizes : function() {
                        var sizes = {
                            stage : {
                                width: this._elems.stage.width(),
                                height: this._elems.stage.height()
                            },

                            viewport : {
                                width: this._elems.nav_view.width(),
                                height: this._elems.nav_view.height()
                            },

                            track : {
                                width: this._elems.nav_view_track.width(),
                                height: this._elems.nav_view_track.height()
                            },

                            image : {
                                width: 100,
                                height: 100
                            }
                        };

                        console.log($(this._elems.stage));

                        var axis = ($('.product_image_gallery', this._elems.stage).css('flex-direction') === 'column') ? 'width' : 'height';

                        console.log(axis, sizes.track[axis], sizes.viewport[axis]);

                        if(sizes.track[axis] > sizes.viewport[axis]) {
                            this._elems.nav_btn_next.show();
                        } else {
                            this._elems.nav_btn_next.hide();
                        }



                        this._config.nav.image_count = $('img', this._elems.nav_view_track).length;
                        this._config.nav.per_view = Math.floor(sizes.viewport.width / sizes.image.width);

                        this._config.axis = axis;
                        this._config.sizes = sizes;
                        //this._config.nav.max_step =
                    },

                    scroll : function(direction) {
                        console.log(this._config.nav);

                        var new_step = (direction === 'next') ? (this._config.nav.cur_step + 1) : (this._config.nav.cur_step - 1);

                        if(new_step < 0) {
                            new_step = 0;
                        }

                        var axis = this._config.axis;
                        var axis_prop = (axis === 'width') ? 'left' : 'top';
                        var vp_axis_dim = this._config.sizes.viewport[axis];

                        var gutter_elem = $('.thumb_item', this._elems.nav_view_track).eq(0);
                        var gutter = (axis === 'width') ? gutter_elem.width() : gutter_elem.height();

                        var new_offset = Math.floor(gutter * new_step);
                        var css = {};

                        css[axis_prop] = -new_offset+'px';

                        if(new_offset > gutter) {
                            this._elems.nav_btn_prev.show();
                        } else {
                            this._elems.nav_btn_prev.hide();
                        }

                        if((new_offset + vp_axis_dim) < $(this._elems.nav_view_track).width()) {
                            this._elems.nav_btn_next.show();
                        } else {
                            this._elems.nav_btn_next.hide();
                        }

                        console.log(gutter, new_offset);

                        $(this._elems.nav_view_track).animate(css, 500);

                        this._config.nav.cur_step = new_step;
                    }
                };

                angular.element(document).ready(function () {
                    Control.init();
                });


                console.log(scope.images);

                scope.selected_image = scope.cover;

                scope.show_gallery_nav = function() {
                    return (scope.images !== undefined && scope.images.length > 1);
                };

                scope.select_image = function(index) {
                    scope.selected_image = scope.images[index].image;

                    console.log(scope.selected_image);
                };
            }
        }
    })



    .directive('productVariantSelector', function(ProductService, APIService) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/products/variant_selector.html',
            scope : {
                product : '='
            },
            link : function(scope, element, attrs) {
                //scope.product = ProductService.get();

                scope.selected = {
                    variant : false,
                    attrs : {}
                };

                scope.filter_options = function(options) {
                    return options;
                    // return _.map(options, function(option, i) {
                    //     return _.each($scope.product.variations, function(variant, i) {
                    //         return _.each(variant.attributes, function(attr, i) {
                    //
                    //         });
                    //     });
                    // });
                };

                console.log(scope.product);
            }
        }
    })

    .directive('productEmbeddedViewer', function(StoreService, APIService, $timeout) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/products/embedded_viewer.html',
            scope : {
                filters : '@'
            },
            link : function(scope, element, attrs) {
                let Shop = Bloomkit.Shop();

                var search_config = {};

                if(scope.filters) {
                    search_config = eval('(' + scope.filters+ ')');
                }

                scope.products = [];
                scope.products_html = '';

                scope.config = {
                    loading: true
                };

                scope.result_html = function() {
                    return scope.products_html;
                };

                return Shop.Products.search(search_config).then(function(res) {
                    console.log('Viewer Search', res);

                    if(res.data.success === true) {
                        scope.products_html += res.data.products;
                    }

                    scope.config.loading = false;
                });
            }
        }
    })

    .directive('productBrowseViewer', function(StoreService, $timeout, APIService) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/products/browse_viewer.html',
            scope : {
                search : '='
            },
            link : function(scope, element, attrs) {
                let Shop = Bloomkit.Shop();

                let stage_elem, stage_btn;

                let search_config = {
                    filters : {
                        per_page : 6
                    }
                };

                let stage_config = {
                    max_y : 0,
                    buffer : 100
                };


                scope.config = {
                    page : 0,
                    page_loaded : 0,
                    page_count: 2,
                    loading: false,
                    loading_more: false,
                    has_products : true
                };

                if(scope.search) {
                    search_config = scope.search;
                }


                let calc_dim = function() {
                    let stage_y = $(stage_elem).height();
                    let stage_pos = $(stage_elem).offset();

                    stage_config.max_y = stage_y + stage_pos.top;
                };

                let scroll_update = function() {
                    let sy,my;

                    if(scope.config.loading === false) {
                        calc_dim();

                        sy = window.scrollY + window.innerHeight;
                        my = stage_config.max_y;

                        if(sy > (my-stage_config.buffer)) {

                            let load_more = scope.load_more();

                            if(load_more !== false) {
                                load_more.then(function(res) {

                                });
                            }
                        }
                    }
                };

                $(window).scroll(function() {
                    scroll_update();
                });

                $(window).on('resize', function() {
                    calc_dim();
                });

                angular.element(document).ready(function () {
                    stage_elem = $('.viewer_grid').eq(0);
                    stage_btn = $('.stage_load_more_btn').eq(0);

                    scroll_update();
                });

                scope.fetch_products = function() {
                    let post = search_config.filters;

                    //post.per_page = 6;
                    post.page = scope.config.page;

                    return Shop.Products.search(post).then(function(res) {

                        if(res.data.info) {
                            scope.config.page_count = res.data.info.page_count;
                        }

                        if(res.data.success === true) {
                            if(scope.config.has_products === false) {
                                scope.config.has_products = true;
                            }

                            $(stage_elem).append(res.data.products);
                        }

                        return res;
                    });
                };

                scope.load_more = function() {

                    if(scope.config.loading === false) {
                        let next_page = (scope.config.page + 1);

                        if(next_page <= scope.config.page_count) {

                            scope.config.page = next_page;

                            scope.loading_more_btn(true);

                            return scope.fetch_products().then(function(res) {

                                scope.loading_more_btn(false);

                                return res;
                            });
                        }
                    }

                    return false;
                };

                scope.loading_more_btn = function(state) {
                    if(state !== false) {
                        state = true;
                    }

                    scope.config.loading = state;

                    if(state === true) {
                        $(stage_btn).addClass('loading');
                    } else {
                        $(stage_btn).removeClass('loading')
                    }
                };

                scope.load_page = function(page) {
                    if(page > scope.config.page_loaded) {
                        scope.config.page_loaded = page;
                    }

                    if(page > scope.config.page_count) {
                        page = scope.config.page_count;
                    }

                    scope.config.page = page;
                };

                scope.result_has_products = function() {
                    //return true;
                    return (scope.config.has_products);
                };


                scope.show_load_more_btn = function() {
                    return (scope.config.page <= scope.config.page_count);
                };


            }
        }
    })

    .factory('PrimoCoverService', function(){
        var loader = false;
        var cover = $('#site_signup');

        var config = {
            open : false
        };



        var Control = {
            open : function() {
                console.log('opening...');

                if(config.open === false) {
                    console.log('opening true..');
                    config.open = true;

                    cover.show(function() {
                        var time_init = anime.timeline();

                        time_init
                            .add({
                                targets: '#site_signup',
                                width: '20%',
                                height: '101%',
                                easing: 'easeOutExpo',
                                duration: 400
                            })
                            .add({
                                targets: '#site_signup',
                                width: '101%',
                                easing: 'easeOutExpo',
                                duration: 400
                            })
                            .add({
                                targets: '#site_signup .signup_block',
                                top: '50%',
                                offset: '-=300',
                                easing: 'easeOutExpo',
                                duration: 500
                            });
                    });

                }
            },

            close : function() {
                console.log('closing');

                Control.loading(false);

                cover.hide().css({
                    width: 0,
                    height: 0
                });

                $('signup_block', cover).css({
                    top: '150%'
                });

                config.open = false;

                console.log('closed');
            },

            loading : function(state) {
                if(loader === false) {
                    loader = $('<div class="signup_loading"><div class="loading_icon"><div class="aicon_circle_loader"></div></div></div>').hide().appendTo(cover);
                }

                if(state) {
                    loader.show();
                } else {
                    loader.hide();
                }
            }
        };

        return Control;
    })

    .directive('primoFollowButton', function(CustomerService) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/common/follow_button.html',
            scope : {
                type : '@',
                id : '@'
            },
            link : function(scope, element, attrs) {

                scope.config = {
                    loading : false
                };


                var follows = {
                    brands : []
                };

                if(CustomerService.get().follows) {
                    follows.brands = CustomerService.get().follows.brands;
                }

                var followed = false;

                console.log(scope.type);
                console.log(follows);

                $(follows.brands).each(function(i, item) {
                    if(item.follow_assoc_id == scope.id && item.follow_assoc_type == scope.type) {
                        followed = true;

                        console.log('Found!');

                        return;
                    }
                });

                console.log(followed);

                scope.is_followed = function() {
                    return followed ;
                };

                scope.follow = function() {
                    var state = (!scope.is_followed());

                    scope.config.loading = true;

                    CustomerService.follow(scope.type, scope.id, state).then(function(res) {
                        if(res.data.success) {
                            followed = (!scope.is_followed());
                        }

                        scope.config.loading = false;
                    });

                };
            }
        }
    })

    .directive('customerAddressManager', function(CustomerService, $mdDialog) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/customer/address_manager/address_grid.html',
            scope : {},
            link : function(scope, element, attrs) {

                $(function() {
                    $('.btn_loader').click(function(evt) {
                        if($(this).hasClass('loading')) {
                            $(this).removeClass('loading');
                        } else {
                            $(this).addClass('loading');
                        }

                    });
                });

                scope.addresses = CustomerService.get().addresses;

                console.log(scope.addresses);

                scope.edit = function(idx) {
                    if(scope.addresses[idx]) {
                        var post = {
                            address : scope.addresses[idx]
                        };

                        scope.open_dialog(post).then(function(res) {
                            console.log(res);
                        });
                    }
                };

                scope.create = function() {
                    scope.open_dialog().then(function(res) {
                        scope.addresses = CustomerService.get().addresses;
                    });
                };

                scope.delete = function(idx) {
                    scope.addresses.splice(idx, 1);
                };


                scope.make_primary = function(idx) {
                    var addresses = CustomerService.get().addresses;

                    if(addresses[idx]) {
                        var address = addresses[idx];

                        return CustomerService.set_primary_address(address.address_id).then(function(res) {
                            console.log(res);

                            return res;
                        });
                    }
                };

                scope.is_primary_address = function(id) {
                    return (CustomerService.get().customer_default_address_shipping_id === id);
                };

                scope.open_dialog = function(config) {
                    if(config === undefined) {
                        config = {};
                    }

                    return $mdDialog.show({
                        controller: 'CustomerAddressManagerDialog',
                        templateUrl: Config.app_path+'views/customer/address_manager/address_dialog.html',
                        clickOutsideToClose:true,
                        locals : {
                            config : config
                        }
                    }).then(function(res) {
                        return res;
                    });
                };
            }
        }
    })

    .controller('CustomerAddressManagerDialog', function($scope, $timeout, $mdDialog, CustomerService, config) {
        $scope.address = {};

        $scope.dialog_config = {
            title : 'Create New Address',
            save_btn_text : 'Create Address',
            success_msg : 'Thanks!',
            loading: false,
            complete : false
        };

        if(config && config.address) {
            $scope.address = config.address;

            $scope.dialog_config.title = 'Update Address';
            $scope.dialog_config.title = 'Save Changes';
        }

        $scope.address.address_type = 'shipping';

        console.log($scope.address);

        $scope.submit = function() {
            var post = {
                address : $scope.address
            };

            $scope.dialog_config.loading = true;

            console.log(post);

            return CustomerService.save(post).then(function(res) {
                if(res.data.success === true) {
                    $timeout(function () {
                        $scope.dialog_config.success = true;

                        $timeout(function () {
                            $mdDialog.hide(res);
                        }, 1000);
                    }, 1000);
                } else {
                    $scope.dialog_config.loading = false;
                }

                return res;
            });

        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };
    })

    .directive('primoInviteButton', function(PrimoCoverService, $mdDialog) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/common/invite_button.html',
            scope : {},
            link : function(scope, element, attrs) {
                scope.open_dialog = function() {
                    $mdDialog.show({
                        controller: 'PrimoInviteDialog',
                        templateUrl: Config.app_path+'views/common/invite_dialog.html',
                        clickOutsideToClose:true,
                        locals : {}
                    }).then(function(folder) {
                        console.log(folder);
                    }, function() {

                    });
                };
            }
        }
    })

    .controller('PrimoInviteDialog', function($scope, $timeout, $mdDialog, CustomerService) {
        $scope.dialog_config = {
            title : 'Invite Your Friends',
            save_btn_text : 'Update Email',
            success_msg : 'Invites have been sent!',
            loading: false,
            complete : false
        };

        $scope.fields = {
            comment: '',
            invites : []
        };

        $scope.add_row = function() {
            $scope.fields.invites.push({ email : ''});
        };

        $scope.remove_row = function(idx) {
            $scope.fields.invites.splice(idx, 1);
        };

        $scope.add_row();

        $scope.submit = function() {
            var post = $scope.fields;

            $scope.dialog_config.loading = true;

            return CustomerService.invite(post).then(function(res) {
                if(res.data.success === true) {
                    $timeout(function () {
                        $scope.dialog_config.success = true;

                        $timeout(function () {
                            //$mdDialog.hide(res);
                        }, 1000);
                    }, 1000);
                } else {
                    $scope.dialog_config.loading = false;
                }

                return res;
            });

        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };
    })

    .directive('primoSignupButton', function(PrimoCoverService) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/common/signup_button.html',
            scope : {},
            link : function(scope, element, attrs) {
                scope.open_signup = function() {
                    PrimoCoverService.open();
                };
            }
        }
    })

    .directive('primoSignupForm', function(APIService, PrimoCoverService) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/common/signup_form.html',
            scope : {},
            link : function(scope, element, attrs) {
                scope.config = {
                    error: false
                };

                scope.fields = {
                    'email' : ''
                };

                scope.close = function() {
                    PrimoCoverService.close();
                };

                scope.submit = function() {
                    scope.config.error = false;

                    var post = scope.fields;

                    post.method = 'submit';

                    PrimoCoverService.loading(true);

                    return APIService.post('bloom_newsletter', post).then(function(res) {

                        if(res.data.success === true) {
                            PrimoCoverService.close();
                        } else {
                            if(res.data.error) {
                                scope.config.error = res.data.error;
                            }

                            PrimoCoverService.loading(false);
                        }

                        console.log(res);

                        return res;
                    });
                }
            }
        }
    })

    .directive('newsletterSignupForm', function(APIService, AppService) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/common/newsletter_signup.html',
            scope : {},
            link : function(scope, element, attrs) {
                scope.fields = {
                    'email' : ''
                };

                scope.submit = function() {
                    var post = scope.fields;

                    post.method = 'submit';

                    AppService.Loading.splash(true);

                    return APIService.post('bloom_newsletter', post).then(function(res) {
                        if(res.data.success === true) {
                            AppService.Loading.splash_to_success({
                                'text' : 'Awesome!',
                                'timeout' : 2000
                            });

                            scope.fields.email = '';
                        } else {
                            AppService.Loading.splash(false);
                        }

                        return res;
                    });
                }
            }
        }
    })

    .directive('orderTotalsSummary', function() {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/common/order_totals_summary.html',
            scope : {
                totals : '='
            },
            link : function(scope, element, attrs) {

                console.log('TOTALS', scope.totals);
            }
        }
    })

    .directive('reviewsViewer', function($rootScope, ProductService) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/review/viewer.html',
            scope : {
                config : '='
            },
            link : function(scope, element, attrs) {
                scope.reviews_html = '';

                var rating_type_values = {
                    'weed_highness' : ['buzzed', 'middle', 'flying'],
                    'weed_flavour' : ['sweet', 'middle', 'sour'],
                    'weed_effect' : ['relaxed', 'middle', 'hyper']
                };

                scope.review_html = function() {
                    return scope.reviews_html;
                };

                scope.fetch_reviews = function() {
                    return ProductService.review_list().then(function(res) {
                        console.log(res);
                        if(res.data.success === true) {
                            scope.reviews_html = res.data.reviews;
                        }

                        return res;
                    }).then(function() {
                        angular.element(document).ready(function () {
                            $('.review_image').lightcase({
                                swipe: true,
                                maxHeight: 1000,
                                maxWidth: 900
                            });
                        });
                    });
                };

                scope.review_ratings = function(index) {
                    console.log(index, scope.reviews);


                    var out_ratings = [];



                    console.log(scope.reviews[index].review_data, scope.reviews[index].review_data.length > 0);

                    if (scope.reviews[index].review_data) {
                        var ratings = scope.reviews[index].review_data.ratings;

                        $.each(ratings, function (key, rating) {
                            console.log('RK', key, rating, rating_type_values[key]);

                            if (rating_type_values[key] !== undefined) {
                                var values = rating_type_values[key];
                                var values_count = values.length;

                                var value_max = values[(values.length - 1)];
                                var value_min = values[0];

                                var value_pos = values.indexOf(rating);
                                var value_percent = 0;

                                if (value_pos !== -1) {
                                    value_percent = (value_pos / values_count) * 100;
                                }
                                // //
                                // // console.log('RATING', {
                                // //     value_min: value_min,
                                // //     value_max: value_max,
                                // //     percent: value_percent+'%'
                                // // });
                                // //
                                // out_ratings.push({
                                //     value_min: value_min,
                                //     value_max: value_max,
                                //     percent: value_percent+'%'
                                // });
                            }

                        });
                    }

                    return out_ratings;
                };

                angular.element(document).ready(function () {
                    scope.fetch_reviews();
                });

                $rootScope.$on('product-review-added', function(event, data) {
                    scope.fetch_reviews();
                });
            }
        }
    })

    .directive('productReviewForm', function($rootScope, ProductService) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/products/review_form.html',
            scope : {
                fields : '=',
                config : '='
            },
            link : function(scope, element, attrs) {
                scope.show_review_form = true;

                scope.config.include_rating_categories = false;

                scope.submit_review = function() {

                    scope.config.on_submit(function() {
                        return ProductService.review_save(scope.fields).then(function(res) {
                            if(res.data.success === true) {
                                //scope.show_review_form = false;

                                $rootScope.$emit('product-review-added');
                            }

                            return res;
                        });
                    })

                };
            }
        }
    })

    .directive('productReviewStars', function() {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/products/review_stars.html',
            scope : {
                rating : '='
            },
            link : function(scope, element, attrs) {
                var t_rating = 0;

                if(!scope.rating || scope.rating == '') {
                    scope.rating = 0;
                }

                scope.is_active = function(pos) {
                    return (parseInt(scope.rating) >= parseInt(pos) || parseInt(t_rating) >= parseInt(pos));
                };

                scope.hover = function(pos) {
                    t_rating = parseInt(pos);
                };

                scope.select = function(pos) {
                    scope.rating = parseInt(pos);
                };

                angular.element(element).on('mouseover', function() {
                    t_rating = 0;
                });
            }
        }
    })

    .directive('productSearcher', function($mdDialog, $location) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/common/product_searcher.html',
            scope : {

            },
            link : function(scope, element, attrs) {
                scope.open_search_dialog = function() {
                    console.log('Here22');

                    $mdDialog.show({
                        templateUrl: Config.app_path+'views/common/product_searcher_dialog.html',
                        clickOutsideToClose:true,
                        locals : {},
                        controller : function($scope, StoreService) {
                            $scope.search_field = '';
                            $scope.products = [];

                            $scope.search_submit = function() {
                                $location.href = 'http://localhost/primo/shop/';
                            };

                            $scope.search_update = function() {
                                $scope.fetch_products();
                            };

                            $scope.fetch_products = function() {
                                var post = {};

                                if($scope.search_field !== '') {
                                    post.search = $scope.search_field;

                                    StoreService.fetch_products(post).then(function(res) {
                                        if(res.data.success === true) {
                                            $scope.products = res.data.result.products;
                                        }
                                    });
                                }
                            };
                        }
                    }).then(function(folder) {
                        console.log(folder);
                    });
                };
            }
        }
    })


    .directive('mobileMenuSidebar', function($rootScope, $timeout) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path + 'views/common/mobile_menu_sidebar.html',
            transclude: true,
            scope: {},
            link: function (scope, element, attrs) {

            }
        }
    })



    .directive('customerAddressFields', function($http, $httpParamSerializerJQLike) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/common/address_fields.html',
            scope : {
                address : '='
            },
            link : function(scope, element, attrs) {
                console.log('ADDRESS FIELDS');
                console.log(scope.address);

                var post = {
                    name : 'North'
                };

                $http({
                    url: 'https://atlas.api.bloomkit.co/api/search',
                    method: "POST",
                    data: JSON.stringify(post),
                    //withCredentials: true,
                    headers: {
                        'Content-Type': 'application/json; charset=utf-8'
                    }
                }).then(function(res) {
                    console.log(res);
                });

                // $http.post('https://atlas.api.bloomkit.co/api/search', $httpParamSerializerJQLike(post), {
                //     headers: {
                //         'Content-Type': 'application/json'
                //     }
                // });
                //
                // $http({
                //     method: 'POST',
                //     url: 'https://atlas.api.bloomkit.co/api/search',
                //     data: JSON.stringify(post)
                // }).then(function(res) {
                //     console.log(res);
                // });
            }
        }
    })

    .directive('checkoutTotals', function() {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/common/checkoutTotals.html',
            scope : {
                order : '='
            },
            controller : function($scope) {

            }
        }
    })

    .directive('addressCitySelect', function($http, $timeout) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/common/city_select.html',
            scope : {
                city : '='
            },
            link : function(scope, element, attrs) {
                scope.search_text = '';
                scope.search_text_region = '';

                scope.selected_city = '';
                scope.selected_region = '';

                scope.city_fields = {
                    name : '',
                    region : '',
                    country : '',
                    countries : [],
                    regions : []
                };

                scope.config = {
                    create_city : false,
                    create_region : false
                };


                scope.is_create = function(key) {
                    return scope.config.create;
                };


                angular.element(document).ready(function () {
                    $('.address_hidden_autofill').allchange(function () {
                        scope.city_fields.name = $('input[name="address_city"]').val();
                        scope.city_fields.region = $('input[name="address_prov"]').val();
                        scope.city_fields.country = $('input[name="address_country"]').val();

                        var autofill_str = scope.city_fields.name+', '+scope.city_fields.region+', '+scope.city_fields.country;

                        scope.search_city(autofill_str).then(function(res) {
                            console.log(res);

                            scope.select_city(res[0].data);
                        });
                    });
                });



                if(scope.city) {
                    scope.search_text = scope.city.display_name;
                } else {
                    scope.city = {};
                }

                scope.select_country = function() {
                    scope.get_regions(scope.city.city_country)
                };

                scope.select_city = function(city) {
                    if(city && city.city_id) {
                        scope.city = city;
                        scope.search_text = city.display_name;
                    }

                };

                scope.create_city = function(text) {

                    scope.city.city_title = text;

                    if(scope.config.create === false) {
                        scope.config.create = true;

                        scope.city = {
                            city_title : text,
                            city_region : scope.city_fields.region,
                            city_country : scope.city_fields.country
                        };

                        scope.get_countries();
                    }
                };

                scope.search_city = function(text) {

                    var post = {
                        'name' : text
                    };

                    return scope.search_fn(post).then(function(res) {
                        console.log(res.data);

                        if(res.data.length > 0) {
                            scope.config.create = false;

                            return res.data.map(function(city) {
                                return {
                                    value : city.city_id,
                                    display: city.display_name,
                                    data: city
                                }
                            });
                        } else {
                            scope.create_city(text);
                        }

                        return [];
                    });
                };

                scope.get_countries = function() {
                    $http({
                        url: 'https://atlas.api.bloomkit.co/api/countries/list',
                        method: "POST",
                        //data: JSON.stringify(post),
                        //withCredentials: true,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8'
                        }
                    }).then(function(res) {
                        scope.city_fields.countries = res.data;
                    });
                };

                scope.get_regions = function(country) {
                    let post = {
                        country : country
                    };

                    $http({
                        url: 'https://atlas.api.bloomkit.co/api/regions/list',
                        method: "POST",
                        data: JSON.stringify(post),
                        //withCredentials: true,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8'
                        }
                    }).then(function(res) {
                        scope.city_fields.regions = res.data;
                    });
                };

                scope.search_fn = function(post) {
                    return $http({
                        url: 'https://atlas.api.bloomkit.co/api/search',
                        method: "POST",
                        data: JSON.stringify(post),
                        //withCredentials: true,
                        headers: {
                            'Content-Type': 'application/json; charset=utf-8'
                        }
                    });
                };

                /*
                 $('#autocomplete').autocomplete({
                 serviceUrl: SITE_CONFIG.manager_api_url+'/regions/cities/search',
                 onSelect: function (suggestion) {
                 alert('You selected: ' + suggestion.value + ', ' + suggestion.data);
                 }
                 });
                 */
            }
        }
    })

    .directive('cartQuantityInput', function(APIService) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/common/quantity_input.html',
            scope : {
                index : '=',
                quantity : '=',
                update : '='
            },
            link : function(scope, element, attrs) {
                console.log(scope.quantity);

                scope.quantity = parseInt(scope.quantity);
                
                scope.updated = function() {
                    if(scope.update) {
                        console.log(scope.index, scope.quantity);

                        scope.update(scope.index, scope.quantity);
                    }
                };

                scope.increase = function() {
                    scope.quantity += 1;

                    scope.updated();
                };

                scope.decrease = function() {
                    scope.quantity -= 1;

                    if(scope.quantity < 1) {
                        scope.quantity = 1;
                    }

                    scope.updated();
                };
            }
        }
    })

    .directive('customerIdSelector', function($mdDialog) {
        return {
            restrict: 'A',
            templateUrl: Config.app_path+'views/customer/id_selector.html',
            scope : {
                image : '='
            },
            link : function(scope, element, attrs) {
                scope.config = {
                    has_image : false
                };

                scope.open_upload_dialog = function(method) {
                    $mdDialog.show({
                        templateUrl: Config.app_path+'views/customer/id_upload_dialog.html',
                        clickOutsideToClose: true,
                        locals : {
                            upload_method : method
                        },
                        controller: function($scope, $mdDialog, $timeout, upload_method) {
                            $scope.image_data = false;

                            $scope.dialog_config = {
                                title : 'Upload Photo Identification',
                                save_btn_text : 'Select Image',
                                method : upload_method
                            };

                            $scope.set_customer_image = function(image_data) {
                                console.log(image_data);

                                $scope.image_data = image_data;
                            };

                            $scope.submit = function() {
                                //$timeout(function() {
                                    console.log($scope.image_data);

                                    $mdDialog.hide($scope.image_data);
                                //});

                            };

                            $scope.cancel = function() {
                                $mdDialog.cancel();
                            };
                        }
                    }).then(function(image_data) {
                        scope.image = image_data;
                    });
                };
            }
        }
    })

    .directive('customerIdUploadWebcam', function() {
        return {
            restrict: 'A',
            templateUrl: Config.app_path+'views/customer/id_upload_webcam.html',
            scope : {
                image : '='
            },
            link : function(scope, element, attrs) {
                scope.upload_data = false;
                scope.selected_image = false;

                scope.config = {
                    webcam : {
                        exists : true,
                        live : false
                    }
                };

                angular.element(document).ready(function () {
                    Webcam.attach('#webcam_view');

                    Webcam.on('live', function () {
                        scope.config.webcam.live = true;
                    });

                    Webcam.on('error', function (err) {
                        scope.config.webcam.exists = false;
                    });
                });

                scope.webcam_snapshot = function() {
                    Webcam.snap(function(data_uri) {
                        $scope.upload_data = data_uri;
                        scope.config.webcam.live = false;

                        $('#webcam_view').hide();
                        $('#webcam_result').html('<img src="'+$scope.upload_data+'"/>');
                    });
                };
            }
        }
    })

    .directive('bloomImageUploader', function($timeout) {
        return {
            restrict: 'A',
            templateUrl: Config.app_path+'views/common/file_uploader.html',
            scope : {
                images : '=',
                onSelect : '&'
            },
            link : function(scope, element, attrs) {

                angular.element(document).ready(function () {
                    const inputElement = document.querySelector('#image_upload_input');

                    FilePond.registerPlugin(FilePondPluginFileEncode);
                    FilePond.registerPlugin(FilePondPluginImageTransform);
                    //FilePond.registerPlugin(FilePondPluginImagePreview);

                    const pond = FilePond.create( inputElement, {
                        imageResizeTargetWidth: 100,
                        allowMultiple: true,
                        dropOnPage: true,
                        dropOnElement: false
                    });

                    const pondroot = document.querySelector('.filepond--root');

                    pondroot.addEventListener('FilePond:addfile', function(e) {
                        console.log('File added', e.detail);
                        console.log('File added2', e);
                    });

                    pond.on('addfile', function(file) {
                        console.log('META', file);
                        var input = $('input[name="file"]');
                        console.log('VAL', input.val());

                        $(input).change(function() {

                        });
                        
                        console.log('IN', input);
                        //console.log('VAL', $(input).val());


                        var reader = new FileReader();

                        reader.onload = function (e) {
                            $timeout(function () {
                                scope.images.push({
                                    'id' : file.id,
                                    'data': e.target.result
                                });

                                console.log('FILE DATA', e.target.result);

                                //console.log(scope.selected);

                                /*
                                $("<img />", {
                                    "src": scope.image,
                                    "class": "thumb-image"
                                }).appendTo(image_holder);

                                if(scope.onSelect !== undefined) {
                                    scope.onSelect({ image_data: scope.image});
                                }
                                */

                            });
                        };

                        reader.readAsDataURL(file.file);
                    });

                    scope.image_remove = function(index) {
                        pond.removeFile(scope.images[index].id);

                        scope.images.splice(index, 1);
                    };

                    /*
                    $("#image_upload_input").on('change', function () {

                        if (typeof (FileReader) != "undefined") {
                            var reader = new FileReader();
                            var image_holder = $("#image_upload_preview");

                            reader.onload = function (e) {
                                $timeout(function () {
                                    scope.image = e.target.result;

                                    //console.log(scope.image);

                                    //console.log(scope.selected);

                                    $("<img />", {
                                        "src": scope.image,
                                        "class": "thumb-image"
                                    }).appendTo(image_holder);

                                    if(scope.onSelect !== undefined) {
                                        scope.onSelect({ image_data: scope.image});
                                    }
                                });
                            };

                            image_holder.empty();
                            image_holder.show();

                            reader.readAsDataURL($(this)[0].files[0]);
                        } else {
                            alert("This browser does not support FileReader.");
                        }
                    });
                    */

                });
            }
        }
    })

    .directive('bloomGridScroller', function($rootScope, $timeout, AppService) {
        return {
            restrict : 'A',
            transclude: true,
            replace: true,
            templateUrl: Config.app_path+'views/common/grid_scroller.html',
            scope : {
                // total_pages : '=',
                // onSelect : '&'
            },
            link : function(scope, element, attrs, ctrl, transclude) {
                console.log(APP_LOAD.grid_config);

                var stage_elem = $('.scroller_stage', element);

                scope.config = {
                    page_count : APP_LOAD.grid_config.page_count,
                    loaded_page: 1,
                    page : 1
                };

                scope.list_pages = function() {
                    var pages = [];

                    for(var i=0; i < scope.config.page_count; i+=1) {
                        pages.push((i + 1));
                    }

                    return pages;
                };

                scope.load_page = function(page) {
                    if(page > scope.config.loaded_page) {
                        scope.config.loaded_page = page;
                    }

                    if(page > scope.config.page_count) {
                        page = scope.config.page_count;
                    }

                    scope.config.page = page;
                };

                scope.goto_page = function(page) {

                };
                
                scope.next_load_page = function() {
                    var next = scope.config.loaded_page + 1;

                    if(next <= scope.config.page_count) {
                        scope.config.loaded_page = next;
                    }
                };

                scope.page_is_visible = function(page) {
                    //page += 1;

                    return (page <= scope.config.loaded_page);
                };

                
                scope.page_classes = function(page) {
                    console.log('P', page);

                    page += 1;

                    var classes = {
                        'scroller_page' : true
                    };

                    if(page <= scope.config.page) {
                        classes.visible = true;
                    }


                    return classes;
                };

                scope.nav_class = function(page) {
                    //console.log(page);

                    var classes = {
                        nav_item : true
                    };

                    if(page <= scope.config.loaded_page) {
                        classes.complete = true;
                    }

                    if(page == scope.config.page) {
                        classes.active = true;
                    }

                    return classes;
                };

                scope.page_visible = function(page) {
                    console.log('ENTER', page);

                    scope.config.page = (page + 1);
                };

                $(window).on('resize scroll', function() {

                });

                var Scroller = new ScrollMagic.Controller();

                var scene = new ScrollMagic.Scene({ triggerElement: '.browse_more_wrap' })
                    .addTo(Scroller)
                    .on("enter", function (e) {
                        console.log('LOADING:', scope.config.loaded_page);

                        $timeout(function() {
                            scope.next_load_page();

                            scene.update();
                        });


                        //console.log(scope.config.loaded_page);
                    });

                transclude(scope, function(clone, scope) {
                    stage_elem.html(clone);
                });
            }
        }
    })

    .directive('bloomProductBrowseFilters', function(APIService) {
        return {
            restrict: 'A',
            templateUrl: Config.app_path+'views/products/index_filters.html',
            scope : {
                filters : '=',
                config : '='
            },
            link : function(scope, element, attrs) {
                scope.success = true;

                scope.fields = {
                    name : 'test'
                };
            }
        }
    })
    
    .filter('isEmpty', function () {
        var bar;
        return function (obj) {
            if(obj === false || ($.isArray(obj) && obj.length == 0)) {
                return true;
            }

            for (bar in obj) {
                if (obj.hasOwnProperty(bar)) {
                    return false;
                }
            }

            return true;
        };
    })

    .filter('nl2p', function () {
        return function(text){
            text = String(text).trim();
            return (text.length > 0 ? '<p>' + text.replace(/[\r\n]+/g, '</p><p>') + '</p>' : null);
        }
    })

    .filter('padLength', function(){
        return function(len, pad){
            if(len == 0) return '';
            else{
                pad = (pad || 0).toString();
                return new Array(1 + len).join(pad);
            }
        };
    });
