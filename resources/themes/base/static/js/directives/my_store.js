angular.module(Config.app_name)

    .directive('customerMyStoreHud', function($rootScope, CustomerService, $timeout, AppService, $mdDialog) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/customer/my_store/hud.html',
            transclude: false,
            scope : {},
            link : function(scope, element, attrs) {
                scope.store = {};

                if(CustomerService.get().meta !== undefined && CustomerService.get().meta.store !== undefined) {
                    scope.store = CustomerService.get().meta.store;
                }


                scope.selected_display = function() {
                    if(scope.store.store_name !== undefined) {
                        return scope.store.store_name;
                    } else {
                        return 'My Store'
                    }
                };

                scope.open_dialog = function(config) {
                    if(config === undefined) {
                        config = {};
                    }

                    return $mdDialog.show({
                        controller: 'CustomerMyStoreDialog',
                        templateUrl: Config.app_path+'views/customer/my_store/dialog.html',
                        clickOutsideToClose:true,
                        locals : {
                            config : config
                        }
                    }).then(function(res) {
                        console.log('OD RET', res);

                        scope.store = CustomerService.get().meta.store;

                        return res;
                    });
                };
            }
        }
    })

    .directive('customerMyStoreSelector', function($rootScope, CustomerService, $timeout, AppService, $mdDialog) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/customer/my_store/selector.html',
            transclude: false,
            scope : {
                store : '='
            },
            link : function(scope, element, attrs) {
                scope.stores = [];

                scope.selected = {
                    id : '0'
                };

                if(scope.store !== undefined) {
                    scope.selected.id = ''+scope.store;
                }

                console.log(APP_CONFIG.stores);

                if(APP_CONFIG.stores !== undefined) {
                    scope.stores = APP_CONFIG.stores;
                }

                scope.select = function() {
                    scope.store = scope.selected.id;
                }
            }
        }
    })

    .controller('CustomerMyStoreDialog', function($scope, $timeout, $mdDialog, CustomerService, config) {
        $scope.dialog_config = {
            title : 'Select Your Store',
            save_btn_text : 'Set Store',
            success_msg : 'Thanks!',
            loading: false,
            complete : false
        };


        $scope.customer = {
            meta : {
                my_store_id : '0'
            }
        };

        $scope.submit = function() {
            var post = {
                meta : {
                    'my_store_id' : $scope.customer.meta.my_store_id
                }
            };

            $scope.dialog_config.loading = true;

            console.log(post);

            return CustomerService.save(post).then(function(res) {
                if(res.data.success === true) {
                    $timeout(function () {
                        $scope.dialog_config.success = true;

                        $timeout(function () {
                            $mdDialog.hide(res);
                        }, 1000);
                    }, 1000);
                } else {
                    $scope.dialog_config.loading = false;
                }

                return res;
            });

        };

        $scope.cancel = function() {
            $mdDialog.cancel();
        };
    });