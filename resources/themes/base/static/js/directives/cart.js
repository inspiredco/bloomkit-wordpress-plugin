angular.module(Config.app_name)

    .directive('productAddToCart', function($rootScope, CustomerService, ProductService, APIService, $timeout, $mdDialog, $window) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/products/cart_add_form.html',
            link : function(scope, element, attrs) {
                scope.product = ProductService.get();

                console.log(scope.product);

                scope.state = {
                    has_stock : true,
                    can_add : false
                };

                scope.cart_fields = {
                    item_product_id : ProductService.get_id(),
                    item_quantity: 1,
                    item_product_price_id : '0',
                    item_product_variant_id : '0',
                    item_product_variant_index : 'select',
                    item_product_price_index : '0'
                };

                if(scope.product.product_is_variation === true) {
                    scope.cart_fields.item_product_variant_id = 'select';
                    //scope.cart_fields.item_product_variant_id = scope.product.variations[0].variant_id;
                } else {
                    scope.state.can_add = true;

                    scope.cart_fields.item_product_price_id = scope.product.prices[0].price_id;
                }

                scope.get_cart_price = function() {
                    var price;

                    if(scope.product.product_is_variation === true) {
                        price = scope.product.variations[scope.cart_fields.item_product_variant_index].variant_price_value;
                    } else if(scope.product.prices[scope.cart_fields.item_product_price_index].price_cost) {
                        price = scope.product.prices[scope.cart_fields.item_product_price_index].price_cost;
                    }

                    console.log('PRICE', price, scope.cart_fields.item_product_variant_index);
                    console.log(scope.product.variations);

                    return accounting.formatMoney((price * scope.cart_fields.item_quantity), '');
                };

                //$scope.get_cart_price();

                scope.product_change_variant = function() {
                    if(scope.cart_fields.item_product_variant_index !== 'select') {
                        scope.state.can_add = true;

                        var variant = scope.product.variations[scope.cart_fields.item_product_variant_index];

                        console.log('VARIANT', variant);

                        $timeout(function() {
                            if(parseFloat(variant.variant_inventory_count) <= 0) {
                                scope.state.has_stock = false;
                            } else {
                                scope.state.has_stock = true;
                            }
                        });


                        scope.cart_fields.item_product_variant_id = variant.variant_id;
                    } else {
                        scope.state.can_add = false;
                    }

                };

                scope.product_change_price = function() {
                    scope.cart_fields.item_product_price_id = scope.product.prices[scope.cart_fields.item_product_price_index].price_id;
                };

                scope.cart_item_add = function() {
                    console.log(scope.cart_fields);
                    var can_add = true;

                    if(scope.product.product_is_variation === true) {
                        if(scope.cart_fields.item_product_variant_id === 'select') {
                            can_add = false;
                        }

                        if(scope.state.has_stock !== true) {
                            can_add = false;
                        }
                    }

                    if(can_add === true) {
                        $mdDialog.show({
                            templateUrl: Config.app_path + 'views/common/success_dialog.html',
                            parent: angular.element(document.getElementsByClassName('site_wrap')),
                            clickOutsideToClose: true,
                            locals: {},
                            controller: function ($scope, $window, $mdDialog) {
                                $scope.dialog_config = {
                                    loading: true,
                                    errors : false,
                                    success : false
                                };

                                CustomerService.Cart.item_add(scope.cart_fields).then(function(res) {
                                    $timeout(function() {
                                        if(res.data.success === true) {
                                            $scope.dialog_config.success = true;
                                        } else {
                                            $scope.dialog_config.errors = true;
                                        }


                                        $scope.dialog_config.loading = false;

                                        scope.cart_fields.item_quantity = 1;
                                    });

                                    $rootScope.$emit('customer-cart-updated');
                                });

                                $scope.continue = function () {
                                    $mdDialog.hide();
                                };

                                $scope.goto_checkout = function () {
                                    $window.location.href = APP_CONFIG.base_url + '/checkout';
                                };
                            }
                        });
                    }
                };

                scope.cart_item_selected = function(index) {
                    return (scope.cart_fields.item_product_price_index == index);
                };
            }
        }
    })

    // .directive('customerCartHudIcon', function($rootScope, CustomerService, $timeout, AppService) {
    //     return {
    //         restrict: 'E',
    //         templateUrl: Config.app_path+'views/cart/cart_hud_icon.html',
    //         transclude: true,
    //         scope : {},
    //         link : function(scope, element, attrs) {
    //             var timer = false;
    //
    //             scope.config = {
    //                 show_menu : false
    //             };
    //
    //             scope.item_count = CustomerService.Cart.get_count();
    //             scope.items = CustomerService.Cart.list_all();
    //
    //             console.log(scope.item_count, scope.items);
    //
    //             scope.toggle_menu = function() {
    //                 scope.config.show_menu = (!scope.config.show_menu);
    //
    //                 // $('html').click(function() {
    //                 //     console.log('hello');
    //                 //
    //                 //     $timeout(function() {
    //                 //         scope.config.show_menu = false;
    //                 //     });
    //                 // });
    //             };
    //
    //             $rootScope.$on('customer-cart-updated', function() {
    //                 scope.item_count = CustomerService.Cart.get_count();
    //                 scope.items = CustomerService.Cart.list_all();
    //             });
    //         }
    //     }
    // })

    .directive('customerCartHud', function($rootScope, $timeout, AppService,  CustomerCartService) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/cart/cart_hud.html',
            transclude: true,
            scope : {},
            link : function(scope, element, attrs) {
                var timer = false;

                scope.config = {
                    show_menu : false
                };

                scope.hud_cart = {
                    item_count : 0,
                    items : []
                };



                scope.reload = function() {
                    let items =  CustomerCartService.list_all();

                    //console.log('HUD CART ITEMS', JSON.parse(JSON.stringify(items)));

                    $timeout(function() {
                        scope.hud_cart.item_count =  CustomerCartService.get_count();
                        scope.hud_cart.items =  CustomerCartService.list_all();

                        console.log('HUD CART', JSON.parse(JSON.stringify(scope.hud_cart)));

                        console.log('HUD RELOAD', scope.hud_cart);
                    });
                };

                scope.toggle_menu = function() {
                    scope.config.show_menu = (!scope.config.show_menu);

                    if (scope.config.show_menu == true) {
                        $('html').click(function (event) {
                            if (! $(event.target).parents('.header_cart_hud').length > 0) {
                                $timeout(function() {
                                    scope.config.show_menu = false;
                                });
                            }
                        })
                    }
                };

                scope.update_quantity = function(idx) {
                    var item = scope.hud_cart.items[idx];

                    if(timer !== false) {
                        $timeout.cancel(timer);
                    }

                    timer = $timeout(function() {
                        if(item.item_quantity !== '') {
                            AppService.Loading.splash(true);

                            Bloomkit.Customer.Cart.item_update(item).then(function(res) {
                                AppService.Loading.splash(false);
                            });
                        }

                        timer = false;
                    }, 1000);
                };

                scope.remove = function(idx) {
                    AppService.Loading.splash(true);

                    Bloomkit.Customer.Cart.item_remove(idx).then(function(res) {
                        scope.reload();

                        AppService.Loading.splash(false);
                    });
                };

                angular.element(document).ready(function () {
                    scope.reload();
                });

                $rootScope.$on('customer-cart-updated', function() {
                    scope.reload();
                });
            }
        }
    })

    .directive('customerCartItemsTable', function($rootScope, $timeout, AppService) {
        return {
            restrict: 'E',
            templateUrl: Config.app_path+'views/cart/items_table.html',
            scope : {
                config : '=',
                edit : '@'
            },
            link : function(scope, element, attrs) {
                var timer = false;

                scope.items = Bloomkit.Customer.Cart.list_all();

                scope.update_quantity = function(idx, qty) {
                    let item = scope.items[idx];

                    console.log(item, idx, qty);

                    if(timer !== false) {
                        $timeout.cancel(timer);
                    }

                    timer = $timeout(function() {
                        if(item.item_quantity !== '') {
                            AppService.Loading.splash(true);

                            Bloomkit.Customer.Cart.item_update(item).then(function(res) {
                                $timeout(function() {
                                    if(res.data.success === true) {
                                        scope.items = res.data.cart.items;
                                    }
                                });

                                $rootScope.$emit('customer-cart-updated');

                                AppService.Loading.splash(false);
                            });
                        }

                        timer = false;
                    }, 1000);
                };

                scope.remove = function(idx) {
                    var item = scope.items[idx];

                    AppService.Loading.splash(true);

                    Bloomkit.Customer.Cart.item_remove(idx).then(function(res) {
                        console.log('REMOVED', res);

                        $timeout(function() {
                            if(res.data.success === true) {
                                scope.items = res.data.cart.items;
                            }
                        });

                        $rootScope.$emit('customer-cart-updated');

                        AppService.Loading.splash(false);
                    });
                };


                // $rootScope.$on('customer-cart-updated', function() {
                //     $timeout(function() {
                //         //scope.item_count = Bloomkit.Customer.Cart.get_count();
                //         scope.items = Bloomkit.Customer.Cart.list_all();
                //     });
                // });
            }
        }
    }
);