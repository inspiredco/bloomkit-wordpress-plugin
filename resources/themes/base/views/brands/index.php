<div class="brands_home">
    <div class="body_section bg_alt">
        <h2 class="section_title">Our Brands</h2>

        <div class="section_body body_contain">

            <div class="brands_index_grid">
                <?php
                foreach($brands as $brand) {
                    ?>

                    <div class="grid_item brand_item">
                        <a href="<?=$brand->get_url(); ?>" style="display: block; background: url('<?=$brand->get_post_field_image_url('brand_media_cover'); ?>') 0 0 no-repeat #F59DA1; height: 400px; background-size: cover;">
                            <div class="brand_logo_wrap">
                                <img src="<?=$brand->get_post_field_image_url('brand_media_logo'); ?>" class="brand_logo"/>
                            </div>
                        </a>

                        <div class="brand_desc">
                            <?=$brand->get_post_desc_short(); ?>
                        </div>
                    </div>

                    <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>