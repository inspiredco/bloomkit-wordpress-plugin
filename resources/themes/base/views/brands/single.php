<div class="brands_single page_landing" ng-controller="BrandProfileCtrl">
    <header class="brand_header">
        <div class="brand_banner">
            <div class="banner_bg lazy" data-bg="url('<?=$brand->get_post_field_image_url('brand_media_banner'); ?>')">
                <!--<img src="<?=$brand->get_post_field_image_url('brand_media_banner'); ?>" />-->
            </div>

            <div class="banner_logo">
                <img src="<?=$brand->get_post_field_image_url('brand_media_logo'); ?>" />
            </div>
        </div>

        <div class="landing_navbar">
            <nav class="landing_nav brand_nav">
                <a href="">About</a>
                <a href="">Products</a>
                <a href="">Reviews</a>
                <a href="">News</a>
            </nav>
        </div>
    </header>

    <div class="brand_body">
        <div class="body_section bg_alt pad_bottom_none body_contain">
            <h2 class="section_left_title">About <?=$brand->get_post_title(); ?></h2>


            <div class="section_body">
                <div class="float_box box_white">

                    <div class="box_body box_brand_summary">
                        <div class="row">
                            <!--
                            <div class="col-md-5 col-lg-3">
                                <div class="brand_summary">
                                    <div class="summary_rating">
                                        <div class="label_rating">
                                            <span class="rating_value"><?=$brand->get_rating_average('?'); ?></span>
                                        </div>
                                    </div>

                                    <div class="summary_row">
                                        <span class="row_title">Rating Average</span>
                                        <span class="row_value"><?=$brand->get_rating_average('?'); ?>/10</span>
                                    </div>

                                    <div class="summary_row">
                                        <span class="row_title">Reviews Counted</span>
                                        <span class="row_value"><?=$brand->get_rating_count(); ?></span>
                                    </div>

                                    <div class="summary_row summary_price_row">
                                        <span class="row_title">Price</span>
                                        <div class="avg_price">
                                            <?php
                                            $price_rating = $brand->get_post_field('brand_price');

                                            if(empty($price_rating)) {
                                                $price_rating = 4;
                                            }

                                            for($i=0; $i < 4; $i+=1) {
                                                $classes = ['price_dollar'];

                                                if($i <= $price_rating) {
                                                    $classes[] = 'active';
                                                }

                                                echo '<span class="'.join(' ', $classes).'">$</span>';
                                            }
                                            ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            -->

                            <div class="col">
                                <div class="brand_info">
                                    <div class="info_overview">
                                        <!--<h3 class="info_title">Brand Overview</h3>-->
                                        <div class="overview_content">
                                            <p><?=$brand->get_post_field('brand_desc_overview'); ?></p>
                                        </div>

                                        <?php
                                        if(primo_allow_register()) {
                                            ?>

                                            <primo-follow-button type="brand" id="<?= $brand->get_id(); ?>"></primo-follow-button>

                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>




        <?php
        $gallery = $brand->get_post_field('brand_gallery');

        if(!empty($gallery)) {
            ?>

            <div class="body_section bg_alt">
                <div class="section_body body_contain">
                    <?php
                    $count = count($gallery);

                    ?>

                    <div class="brand_hero_gallery">
                        <?php
                        foreach($gallery as $image) {
                            ?>

                            <div class="gallery_item">
                                <img class="lazy" data-src="<?=$image['url']; ?>" />
                            </div>

                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>

           <?php
        }
        ?>


        <?php
        $product_sections = $brand->get_post_field('brand_landing_product_sections');

        if(!empty($product_sections)) {
            foreach($product_sections as $section) {
                ?>

                <div class="body_section">
                    <!--<h2 class="section_title"><?=$section['section_title']; ?></h2>-->

                    <div class="section_body body_contain">
                        <product-embedded-viewer filters="{limit:<?=$section['section_item_count']; ?>,brand:<?=$brand->get_id(); ?>,category:<?=$section['section_category_id']; ?>}"></product-embedded-viewer>
                    </div>

                    <div class="row_center_btn">
                        <?=primo_arrow_btn($section['section_button_text'], site_url($section['section_button_url'])); ?>
                    </div>
                </div>

                <?php
            }
        }
        ?>

        <?php
        if(false && $brand->get_rating_count() > 0) {
            ?>

            <div class="body_section bg_alt">
                <div class="review_rating">
                    <div class="label_rating">
                        <span class="rating_value"><?=$brand->get_rating_average('?'); ?></span>
                    </div>
                </div>

                <h2 class="section_title"><?=$brand->get_rating_count(); ?> Reviews</h2>

                <div class="section_body body_contain">
                    <div class="reviews_viewer">
                        <div class="viewer_filters">

                        </div>

                        <div class="viewer_body">

                            <div class="viewer_items review_items">
                                <?php
                                //look($product->get_reviews());

                                $rating_type_values = [
                                    'highness' => ['buzzed', 'middle', 'flying'],
                                    'flavour' => ['sweet', 'middle', 'sour'],
                                    'effect' => ['relaxed', 'middle', 'hyper'],
                                ];

                                foreach($brand->get_reviews() as $review) {
                                    //look($review);

                                    ?>

                                    <div class="viewer_item">
                                        <div class="item_review_meta">
                                            <div class="meta_header">
                                                <div class="meta_rating_overall">
                                                    <div class="label_rating rating_small">
                                                        <span class="rating_value"><?=$review->review_data->ratings->overall; ?></span>
                                                    </div>
                                                </div>

                                                <div class="meta_author">
                                                    <span class="author_name"><?=$review->customer->customer_name_abbrev; ?></span>
                                                    <span class="author_date"><?=$review->review_date_created; ?></span>
                                                </div>
                                            </div>

                                            <div class="meta_ratings">
                                                <?php
                                                if(false) { //!empty($review->review_data->ratings)) {
                                                    foreach($review->review_data->ratings as $key => $rating) {
                                                        if(!empty($rating_type_values[$key])) {
                                                            $label = $rating_type_values[$key]['title'];
                                                            $values = $rating_type_values[$key]['values'];
                                                            $values_count = count($values);

                                                            $value_max = $values[($values_count - 1)];
                                                            $value_min = $values[0];

                                                            $value_pos = array_search($rating, $values);
                                                            $value_percent = 0;

                                                            if(!empty($value_pos)) {
                                                                $value_percent = ($value_pos / ($values_count-1)) * 100;
                                                            }
                                                            ?>

                                                            <div class="rating_item">
                                                                <span class="rating_label"><?=$label; ?></span>

                                                                <div class="rating_scale">
                                                                    <span class="scale_label"><?=$value_min; ?></span>
                                                                    <div class="scale_bar">
                                                                        <div class="bar_point" style="left: <?=$value_percent; ?>%;"></div>
                                                                        <div class="bar_line"></div>
                                                                    </div>
                                                                    <span class="scale_label"><?=$value_max; ?></span>
                                                                </div>
                                                            </div>

                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                            </div>
                                        </div>

                                        <div class="item_review_main">
                                            <h4><?=$review->review_data->title; ?></h4>
                                            <?=wpautop(stripslashes($review->review_data->comment)); ?>

                                            <?php
                                            if(!empty($review->media)) {
                                                ?>

                                                <div class="review_images">

                                                    <?php
                                                    foreach($review->media as $media) {
                                                        ?>

                                                        <div class="review_image_item">
                                                            <a href="<?=$media->image->sizes->large->url; ?>" data-rel="lightcase:reviewImages<?=$review->review_id; ?>">
                                                                <img src="<?=$media->image->sizes->thumb->url; ?>" />
                                                            </a>
                                                        </div>
                                                        <?php
                                                    }
                                                    ?>

                                                </div>

                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>

                                    <?php
                                }
                                ?>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <?php
        }
        ?>


        <div class="body_section bg_alt">
            <h2 class="section_title">Hot off the press</h2>

            <div class="section_body body_contain">
                <div class="blog_post_list">
                    <?php
                    do_shortcode('[bloom_blog_posts]');
                    ?>
                </div>

                <div class="row_center_btn">
                    <?=primo_arrow_btn('View More', site_url('weed-knowledge')); ?>
                </div>
            </div>
        </div>
    </div>
</div>