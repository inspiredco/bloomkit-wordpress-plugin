
<div class="body_section">
    <?php
    if($success === true) {
        ?>

        <h2 class="section_title">You're good to go!</h2>

        <div class="section_body body_contain" style="text-align: center;">
            <h4>Your password has been successfully reset!</h4>

            <a href="<?=site_url('account'); ?>" class="text_btn">Go to your account</a>
        </div>

        <?php
    } else {
        ?>

        <h2 class="section_title">Reset Your Password</h2>

        <div class="section_body body_contain">
            <form class="form login_register_form" method="post" action="">
                <div class="form_row">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="password" class="field" name="reset_password" placeholder="Password..." />
                        </div>

                        <div class="col-md-6">
                            <input type="password" class="field" name="reset_password_confirm" placeholder="Confirm Password..." />
                        </div>
                    </div>

                </div>

                <button type="submit" class="btn btn_primary btn_block">Reset Password</button>
            </form>
        </div>

        <?php
    }
    ?>

</div>