<div class="body_section">
    <?php
    if($success === true) {
        ?>

        <h2 class="section_title">We've sent you an email</h2>

        <div class="section_body body_contain" style="text-align: center;">
            <h4>To complete your password reset, please follow the instructions in the password reset email.</h4>
        </div>

        <?php
    } else {
        ?>

        <h2 class="section_title">Reset Your Password</h2>

        <div class="section_body body_contain">
            <form class="form login_register_form" method="post" action="">
                <div class="form_row">
                    <input type="text" class="field" name="bk_forgot_pass_email" placeholder="Email Address" />
                </div>

                <button type="submit" class="btn btn_primary btn_block">Reset Password</button>
            </form>

        </div>

        <?php
    }
    ?>

</div>