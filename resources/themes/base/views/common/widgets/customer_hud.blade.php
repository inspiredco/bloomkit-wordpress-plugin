<nav class="customer_hud_nav" ng-if="!(customer_is_auth())" ng-cloak>
    <a href="<?=bk_get_route_url('login'); ?>">Login</a>
    <a href="<?=bk_get_route_url('register'); ?>">Register</a>
</nav>

<nav class="customer_hud_nav" ng-if="(customer_is_auth())" ng-cloak>
    <a href="<?=bk_get_route_url('account'); ?>">My Account</a>
    <a href="<?=bk_get_route_url('account', 'logout'); ?>">Logout</a>
</nav>

