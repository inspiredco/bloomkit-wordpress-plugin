<div class="body_section" ng-controller="LoginCtrl">
    <h2 class="section_title">Let's Smoke One Together</h2>

    <div class="section_body wrap">

        <div class="customer_login_register_box">
            <div class="box_back_layer">
                <?php
                if(bk_allow_register()) {
                    ?>

                    <div class="register_text">
                        <h3 class="text_title">Create An Account</h3>
                        <p class="text_tagline">Create an account and become a Primo member. Follow the link below and fill out the form on the next page to enjoy the benefits of your Primo membership.</p>
                    </div>

                    <a href="<?=bk_get_route_url('register'); ?>" class="btn btn_primary btn_block">Create an account</a>

                    <?php
                } else {
                    ?>

                    <div class="register_text">
                        <h3 class="text_title">Registration Closed</h3>
                        <p class="text_tagline">We are currently not accepting new members.</p>
                    </div>

                    <?php
                }
                ?>
            </div>

            <div class="box_front_layer">
                <h3 class="layer_title">Sign-In</h3>

                <?php
                if(!empty($error)) {
                    ?>

                    <div class="alert alert-danger">
                        <?=$error; ?>
                    </div>

                    <?php
                }
                ?>
                <form class="form login_register_form" method="post" action="">
                    <div class="col_content">

                        <div class="form_row">
                            <input type="text" class="field" name="bk_customer_login_user" placeholder="Email Address" />
                        </div>
                        <div class="form_row">
                            <input type="password" class="field" name="bk_customer_login_pass" placeholder="Password" />
                        </div>
                        <div class="form_row">
                            <span class="login_disclaimer">By logging in, you agree to Primo's Privacy Policy and Terms of Use</span>
                        </div>
                        <button type="submit" class="btn btn_primary btn_block">Sign In</button>
                    </div>

                    <!--<a ng-click="forgot_pass()">Forgot Your Password?</a>-->
                </form>
            </div>
        </div>

    </div>
</div>