<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>You've been invited to join Primo</title>
    <style type="text/css">
        @import url("https://use.typekit.net/fcz1toe.css");
        /* Client-specific Styles */
        #outlook a {padding:0;} /* Force Outlook to provide a "view in browser" menu link. */
        body{width:100% !important; height: 100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0; font-family: Trebuchet MS,sans-serif; }
        /* Prevent Webkit and Windows Mobile platforms from changing default font sizes, while not breaking desktop design. */
        .ExternalClass {width:100%;} /* Force Hotmail to display emails at full width */
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;} /* Force Hotmail to display normal line spacing.  More on that: http://www.emailonacid.com/forum/viewthread/43/ */
        #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
        img {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}
        a img {border:none;}
        .image_fix {display:block;}
        p {margin: 1em 0;}
        table td {border-collapse: collapse;}
        table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }
        a {text-decoration: none;}
        @media only screen and (max-device-width: 480px) {
            a[href^="tel"], a[href^="sms"] {
                text-decoration: none;
                color: black; /* or whatever your want */
                pointer-events: none;
                cursor: default;
            }
            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                text-decoration: default;
                color: #7ED321 !important; /* or whatever your want */
                pointer-events: auto;
                cursor: default;
            }
        }
        @media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {
            /* You guessed it, ipad (tablets, smaller screens, etc) */
            /* Step 1a: Repeating for the iPad */
            a[href^="tel"], a[href^="sms"] {
                text-decoration: none;
                color: #7ED321; /* or whatever your want */
                pointer-events: none;
                cursor: default;
            }
            .mobile_link a[href^="tel"], .mobile_link a[href^="sms"] {
                text-decoration: default;
                color: #7ED321 !important;
                pointer-events: auto;
                cursor: default;
            }
        }
        @media only screen and (-webkit-min-device-pixel-ratio: 2) {
            /* Put your iPhone 4g styles in here */
        }
        @media only screen and (-webkit-device-pixel-ratio:.75){
            /* Put CSS for low density (ldpi) Android layouts in here */
        }
        @media only screen and (-webkit-device-pixel-ratio:1){
            /* Put CSS for medium density (mdpi) Android layouts in here */
        }
        @media only screen and (-webkit-device-pixel-ratio:1.5){
            /* Put CSS for high density (hdpi) Android layouts in here */
        }
        /* end Android targeting */

        @media screen and (max-width: 480px) {
            #primoContainer {
                width:100% !important;
            }
        }
        .primoButton {
            background-color: #FFF200;
            color: black;
            padding: 12px 24px;
            font-size: 14px;
            line-height: 40px;
            text-decoration: none;
            font-weight: 600;
            text-transform: uppercase;
        }

    </style>

    <!-- Targeting Windows Mobile -->
    <!--[if IEMobile 7]>
    <style type="text/css">
    </style>
    <![endif]-->

    <!-- ***********************************************
    ****************************************************
    END MOBILE TARGETING
    ****************************************************
    ************************************************ -->

    <!--[if gte mso 9]>
    <style>
        /* Target Outlook 2007 and 2010 */
    </style>
    <![endif]-->
</head>
<body style="background-color: #fff;">

<table cellpadding="0" cellspacing="0" border="0" width="100%" id="backgroundTable">
    <!--<tr>
            <td style="padding: 25px 10%; background-color: #000; color: #ffffff; text-align: center;">
                <img src="http://localhost/bloomkit-primo/wp-content/themes/primo/static/img/primologo.svg" alt="Primo">
            </td>
        </tr>-->
    <tr>
        <td>
            <table align="center" border="1" cellpadding="0" cellspacing="0" width="600" style="margin-top:10px; border-bottom: 5px solid #FFED08;"
                   id="primoContainer">
                <tr>
                    <td>
                        <table align="center" style="text-align: center; width: 100%;">
                            <tr>
                                <td align="center" style="padding: 20px 40px 0px; background-color: #000; color: #ffffff;">
                                    <!--<h1 style="text-transform: uppercase; font-size:28px; line-height:1;margin:0;">New Customer</h1>-->
                                    <img src="<?=site_url('/wp-content/themes/primo/static/img/primologo-email-large.png'); ?>" style="width: 100%; max-width: 57.5px; height: 50px;" alt="Primo" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center" style="padding: 30px 40px 20px; background-color: #000; color: #ffffff;">
                                    <table align="center" style="text-align: center; font-family: Termina;">
                                        <tr style="font-family: Termina; font-weight: 500; font-size: 8px; letter-spacing: 1px;">
                                            <td style="padding: 0px 10px;">
                                                <a style="color: #FFFFFF; text-decoration: none;" href="https://itsprimo.com/about-primo/">ABOUT</a>
                                            </td>
                                            <td style="padding: 0px 10px;">
                                                <a style="color: #FFFFFF; text-decoration: none;" href="<?= bk_get_route_url('shop'); ?>">STORE</a>
                                            </td>
                                            <td style="padding: 0px 10px;">
                                                <a style="color: #FFFFFF; text-decoration: none;" href="https://itsprimo.com/weed-knowledge/">WEED KNOWLEDGE</a>
                                            </td>
                                            <td style="padding: 0px 10px;">
                                                <a style="color: #FFFFFF; text-decoration: none;" href="<?= bk_get_route_url('brands'); ?>">BRANDS</a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="padding: 30px 40px; font-family: proxima-nova, sans-serif;">
                        {!! $body_html !!}
                    </td>
                </tr>
                <tr>
                    <td align="center" style="padding: 20px 40px; background-color: #000; color: #ffffff;">
                        <table align="center" style="text-align: center;">
                            <tr>
                                <td align="center" style="padding: 40px 0; background-color: #000; color: #ffffff;">
                                    <img src="<?=site_url('/wp-content/themes/primo/static/img/primologo-email-large.png'); ?>" style="width: 100%; max-width: 57.5px; height: 50px;" alt="Primo" />
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: proxima-nova, sans-serif; max-width: 450px; font-weight: 300; line-height: 24px; font-size: 12px; padding: 0 20px 25px;">
                                    Primo is focused on providing the highest quality medical marijuana products, without the corporate baggage. We care about one thing and one thing only - providing you with the right high.
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table align="center" style="text-align: center; font-family: Termina;">
                                        <tr style="font-family: Termina; font-weight: 500; font-size: 8px; letter-spacing: 1px;">
                                            <td style="padding: 10px;">
                                                <a style="color: #FFFFFF; text-decoration: none;" href="https://itsprimo.com/about-primo/">ABOUT</a>
                                            </td>
                                            <td style="padding: 10px;">
                                                <a style="color: #FFFFFF; text-decoration: none;" href="<?= bk_get_route_url('shop'); ?>">STORE</a>
                                            </td>
                                            <td style="padding: 10px;">
                                                <a style="color: #FFFFFF; text-decoration: none;" href="https://itsprimo.com/weed-knowledge/">WEED KNOWLEDGE</a>
                                            </td>
                                            <td style="padding: 10px;">
                                                <a style="color: #FFFFFF; text-decoration: none;" href="<?= bk_get_route_url('brands'); ?>">BRANDS</a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="font-family: Termina; font-weight: 500; font-size: 8px; letter-spacing: 1px; padding: 25px 0; ">
                                    &copy;2019 PRIMO ALL RIGHTS RESERVED
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<!-- End of wrapper table -->
</body>

</html>