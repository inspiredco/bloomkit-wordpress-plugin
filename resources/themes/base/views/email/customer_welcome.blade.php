<p style="line-height: 20px;">Thank you for joining Primo. Please confirm your email address to confirm your account.</p>
<p style="line-height: 20px;">We'll be curating the best cannabis products and accessories in the world. Stay tuned for limited edition product drops, free shared knowledge, product battles &amp; reviews, and event updates.</p>

@if(!empty($verify_url))
    <p style="text-align: center; margin: 2em 0;"><a href="{{$verify_url}}" target="_blank" rel="nooopener noreferrer" style="background-color: #FFF200; font-family: termina, sans-serif; color: black; padding: 12px 24px; font-size: 14px; line-height: 40px; text-decoration: none; font-weight: 600; text-transform: uppercase;" class="primoButton">Complete Registration</a></p>
@else
    <p style="text-align: center; margin: 2em 0;"><a href="{!! bk_get_route_url('account') !!}" target="_blank" rel="nooopener noreferrer" style="background-color: #FFF200; font-family: termina, sans-serif; color: black; padding: 12px 24px; font-size: 14px; line-height: 40px; text-decoration: none; font-weight: 600; text-transform: uppercase;" class="primoButton">Your Account</a></p>
@endif

<p style="line-height: 20px;">Through the above link you'll be able to access your account, view orders, change your password, and invite your friends.</p>
<p style="line-height: 20px;">Welcome to the #EstablishedUnderground.</p>
<p style="line-height: 20px;">Cheers,</p>
<p style="line-height: 20px;">The Primo Team</p>
<img src="https://itsprimo.com/wp-content/uploads/2018/08/Primo_Logos-01.png" alt="Creating Email Magic" width="69" height="60" />