<table style="width: 100%;">
    <tr>
        <td border="0" style="border-bottom: 0;">
            <p>Hello {{ $customer->customer_name_full }},</p>
            <p>This is just a quick note to let you know your order has been received. We will be packing it up in the next couple hours and shipping it ASAP.</p>
            <p>A shipping tracking number will follow this email.</p>
        </td>
    </tr>
    <tr>
        <td style="padding: 20px 0 0; border-top: 0; border-bottom: 0;">
            <table border="1" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <th width="60%" valign="middle" style="padding: 12px;" align="left">
                        <b>Product</b>
                    </th>
                    <th width="20%" valign="middle" style="padding: 12px;" align="left">
                        <b>Quantity</b>
                    </th>
                    <th width="20%" valign="middle" style="padding: 12px;" align="left">
                        <b>Price</b>
                    </th>
                </tr>

                @foreach($order->get_items() as $item)
                    <tr>
                        <td valign="middle" style="padding: 12px; line-height: 20px">
                            @if($item->product->is_variation())
                                <br />{{ $item->variation->display_attributes_str }}
                            @endif
                        </td>
                        <td valign="middle" style="padding: 12px;">
                            <?= $item->item_quantity; ?>
                        </td>
                        <td valign="middle" style="padding: 12px;">
                            <?= $item->item_cost; ?>
                        </td>
                    </tr>
                @endforeach

                <tr>
                    <td colspan="2" valign="middle" style="padding: 12px;">
                        <b>Subtotal</b>
                    </td>
                    <td colspan="1" valign="middle" style="padding: 12px;">
                        $<?=$order->get_cost_subtotal(); ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" valign="middle" style="padding: 12px;">
                        <b>Shipping</b>
                    </td>
                    <td colspan="1" valign="middle" style="padding: 12px;">
                        $<?=$order->get_cost_shipping(); ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" valign="middle" style="padding: 12px;">
                        <b>Taxes</b>
                    </td>
                    <td colspan="1" valign="middle" style="padding: 12px;">
                        $<?=$order->get_cost_tax(); ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" valign="middle" style="padding: 12px;">
                        <b>Total</b>
                    </td>
                    <td colspan="1" valign="middle" style="padding: 12px;">
                        $<?=$order->get_cost_total(); ?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>

    <?php
        $billing = $order->get_billing_method();
        $shipping = $order->get_shipping_method();
    ?>


    <tr>
        <td style="border-top: 0; border-bottom: 0;">
            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td width="50%" valign="top" style="padding: 12px; line-height: 20px;" align="left" class="primoColumn">
                        <p><b>Billing Address</b></p>
                        <p>
                            <?= $billing->value->card->card_holder_name ?><br />

                            @if(!empty($billing->value->card->address))
                                <?= $billing->value->card->address->address_street_1 ?><br />
                                @if (!empty($billing->value->card->address->address_street_2))
                                    <?= $billing->value->card->address->address_street_2 ?><br />
                                @endif
                                <?= $billing->value->card->address->city->city_title ?>,
                                <?= $billing->value->card->address->city->region_abbrev ?><br />
                                <?= $billing->value->card->address->address_post_code ?><br />
                            @endif
                        </p>
                    </td>
                    <td width="50%" valign="top" style="padding: 12px; line-height: 20px;" align="left" class="primoColumn">
                        <p><b>Shipping Address</b></p>
                        <p>
                            @if(!empty($shipping->value->address))
                                <?= $shipping->value->address->address_street_1 ?><br />
                                @if (!empty($shipping->value->address->address_street_2))
                                    <?= $shipping->value->address->address_street_2 ?><br />
                                @endif
                                <?= $shipping->value->address->city->city_title ?>,
                                <?= $shipping->value->address->city->region_abbrev ?><br />
                                <?= $shipping->value->address->address_post_code ?><br />
                            @endif
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="border-top: 0;">
            <p>Thank you for supporting the industry.</p>
            <p>Cheers,</p>
            <p>The Primo Team</p>
            <img src="https://itsprimo.com/wp-content/uploads/2018/08/Primo_Logos-01.png" alt="Creating Email Magic"
                 width="69" height="60" />
        </td>
    </tr>
</table>