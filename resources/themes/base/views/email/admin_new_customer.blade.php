<p style="line-height: 20px;"></p>
<table style="width: 100%;">
    <tr>
        <td border="0" style="border-bottom: 0;">
            <p>A new customer has signed up for The BWell Market.</p>
        </td>
    </tr>
    <tr>
        <td style="padding: 20px 0 0; border-top: 0; border-bottom: 0;">
            <table border="1" cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <th width="50%" valign="middle" style="padding: 12px;" align="left">
                        <b>Customer Information</b>
                    </th>
                    <th width="50%" valign="middle" style="padding: 12px;" align="left"></th>
                </tr>

                <tr>
                    <td valign="middle" style="padding: 12px; line-height: 20px">
                        Name
                    </td>
                    <td valign="middle" style="padding: 12px;">
                        {{ $customer->customer_name_full }}
                    </td>
                </tr>
                <tr>
                    <td valign="middle" style="padding: 12px; line-height: 20px">
                        Email
                    </td>
                    <td valign="middle" style="padding: 12px;">
                        {{ $customer->customer_email }}
                    </td>
                </tr>
                <tr>
                    <td valign="middle" style="padding: 12px; line-height: 20px">
                        Customer ID
                    </td>
                    <td valign="middle" style="padding: 12px;">
                        {{ $customer->customer_id }}
                    </td>
                </tr>
                <tr>
                    <td valign="middle" style="padding: 12px; line-height: 20px">
                        Date Created
                    </td>
                    <td valign="middle" style="padding: 12px;">
                        {{ $customer->customer_date_created }}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>