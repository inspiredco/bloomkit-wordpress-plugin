<p style="line-height: 20px;">You've been invited to join Primo: an online review driven marketplace focused on curating the highest grade cannabis products, limited edition merch and accessories made in LA, and the best blog content meant to educate.</p>
<p style="line-height: 20px;">Sign-up is free and takes 2 minutes:</p>
<p style="text-align: center; margin: 2em 0;"><a style="background-color: #FFF200; font-family: termina, sans-serif; color: black; padding: 12px 24px; font-size: 14px; line-height: 40px; text-decoration: none; font-weight: 600; text-transform: uppercase;" class="primoButton" href="{{ $invite_url }}" target="_blank" rel="nooopener noreferrer">Sign Up Now</a></p>
<p style="line-height: 20px;">We look forward to curating the best products in the world for you. Don't get left behind.</p>
<p style="line-height: 20px;">Cheers,</p>
<p style="line-height: 20px;">The Primo Team</p>
<img src="https://itsprimo.com/wp-content/uploads/2018/08/Primo_Logos-01.png" alt="Creating Email Magic" width="69" height="60" />