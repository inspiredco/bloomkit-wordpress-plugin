<div style="text-align: center;">
    <h1 style="margin: 20px 0; font-size: 25px; color: #555;">Password Reset</h1>
    <p style="color: #ccc; font-size: 18px;">Your password has been reset. Click the link below to complete.</p>

    <a href="<?=$reset_url ?>" style="font-size: 14px; font-weight: bold; text-decoration: none; ">Reset Your Password</a>
</div>