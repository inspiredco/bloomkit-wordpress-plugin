<div class="dispensaries_map">
    <div class="map_controls">
        <div class="map_items">
            <?php
            for($i = 0; $i < 5; $i+=1) {
                ?>

                <div class="map_item">
                    <h3 class="item_title">WEEDS</h3>
                    <div class="item_address">
                        116 W Broadway, Vancouver, BC V7L2Y6
                    </div>
                </div>

                <?php
            }
            ?>
        </div>
    </div>


    <div class="map_stage" style="height: 100%;">
        <ng-map center="49.2433672, -123.0972795" zoom="12" pan-control="false" zoom-control="false" scale-control="true" default-style="false"></ng-map>
    </div>
</div>
