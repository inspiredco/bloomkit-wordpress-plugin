<script>

</script>

<div ng-controller="AccountEditProfileCtrl">
    <div class="body_section body_splash">
        <h2 class="section_title">Edit Your Profile</h2>

        <div class="section_body body_contain">
            <form class="form" method="POST" ng-submit="submit()">
                <section class="form_section">
                    <div class="form_row">
                        <div class="row">
                            <div class="col-12 col-md-6">
                                <div class="field_block">
                                    <label>First Name</label>
                                    <input type="text" class="field" ng-model="fields.customer_name_first" />
                                </div>
                            </div>

                            <div class="col-12 col-md-6">
                                <div class="field_block">
                                    <label>Last Name</label>
                                    <input type="text" class="field" ng-model="fields.customer_name_last" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form_row">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form_field">
                                    <label>Gender</label>
                                    <select class="field" ng-model="fields.customer_gender">
                                        <option>Not Applicable</option>
                                        <option>Male</option>
                                        <option>Female</option>
                                        <option>Other</option>
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form_field">
                                    <label>Birthdate</label>
                                    <input type="text" class="field" ng-model="fields.customer_date_birth" />
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <!--
                <section class="form_section">
                    <div class="form_row row_split">
                        <div class="row_col split_col_label">
                            <span class="label_title">Additional Information</span>
                        </div>

                        <div class="row_col split_col_value">
                            <div class="form_row">
                                <label>How often do you use cannabis?</label>
                                <select class="field" name="customer_name_first">
                                    <option>Daily</option>
                                    <option>Weekly</option>
                                    <option>Monthly</option>
                                    <option>Yearly</option>
                                </select>
                            </div>

                            <div class="form_row">
                                <label>Preferred method of use?</label>
                                <select class="field" name="customer_name_first">
                                    <option>Joint</option>
                                    <option>Pipe / Bong</option>
                                    <option>Vaporizer</option>
                                    <option>Edibles</option>
                                </select>
                            </div>

                            <div class="form_row">
                                <label>How long have you been using cannabis?</label>
                                <select class="field" name="customer_name_first">
                                    <option>Under a year</option>
                                    <option>2 Years</option>
                                    <option>3 Years</option>
                                    <option>4 Years</option>
                                    <option>5 Years and over</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </section>
                -->

                <div class="form_row_submit submit_split">
                    <a href="<?=bk_get_route_url('account'); ?>" class="btn btn_small action_btn_back"><i class="fa fa-chevron-left"></i> Cancel</a>
                    <button type="submit" class="btn btn_small btn_primary btn_loader action_btn_submit">Save Changes</button>
                </div>
            </form>

<!--            <section class="content_section section_border">-->
<!--                <div class="row">-->
<!--                    <div class="col-12 col-md-8">-->
<!--                        <h4>Change Your Password</h4>-->
<!--                    </div>-->
<!---->
<!--                    <div class="col-12 col-md-4">-->
<!--                        <a class="btn" ng-click="change_pass()">Change Password</a>-->
<!--                    </div>-->
<!--                </div>-->
<!--            </section>-->

        </div>
    </div>


</div>