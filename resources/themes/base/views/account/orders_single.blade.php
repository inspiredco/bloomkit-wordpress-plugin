<div class="body_section">
    <h3 class="section_title">View Order #<?=$order->get_id(); ?></h3>

    <div class="section_body body_contain">
        <section class="content_section">
            <div class="row">
                <div class="col-md-6">
                    <div class="float_panel account_customer">
                        <div class="panel_header">
                            <h3 class="panel_title">Order #<?=$order->get_id(); ?></h3>
                        </div>

                        <section class="panel_section">
                            <strong>Date:</strong> <?=$order->get_date('created'); ?>
                        </section>

                        <section class="panel_section">
                            <strong>Status: </strong> <?=$order->get_status_title(); ?>
                        </section>

                        <section class="panel_section">
                            <strong>Items: </strong> <?=$order->get_item_count(); ?> Items
                        </section>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="float_panel account_order_totals">
                        <div class="panel_header">
                            <h3 class="panel_title">Order Totals</h3>
                        </div>

                        <?php
                        $totals = $order->get_totals();
                        ?>
                        <section class="panel_section total_row">
                            <span class="total_label">Subtotal</span>
                            <span class="total_value">$<?=$order->get_cost_subtotal(); ?></span>
                        </section>

                        @if(!empty($totals->discount))
                            <section class="panel_section total_row">
                                <span class="total_label">Coupon</span>
                                <span class="total_value">-$<?=$totals->discount; ?></span>
                            </section>
                        @endif

                        <section class="panel_section total_row">
                            <span class="total_label">Shipping</span>
                            <span class="total_value">$<?=$order->get_cost_shipping(); ?></span>
                        </section>

                        <section class="panel_section total_row">
                            <span class="total_label">Taxes</span>
                            <span class="total_value">$<?=$order->get_cost_tax(); ?></span>
                        </section>

                        <section class="panel_section total_row">
                            <span class="total_label">Total</span>
                            <span class="total_value">$<?=$order->get_cost_total(); ?></span>
                        </section>
                    </div>
                </div>
            </div>
        </section>

        <!--
        <section class="content_section">
            <div class="row">
                <div class="col-lg-6">
                    <div class="float_panel checkout_review_panel">
                        <div class="panel_header">
                            <h3 class="panel_title">Payment Method</h3>
                        </div>

                        <section class="panel_section">
                            <h3 class="section_title">%% order.billing.method_title %%</h3>
                        </section>

                        <section ng-cloak class="panel_section" ng-if="(order.billing.config.template_key === 'credit_card')">
                            @include('shop.checkout.billing.credit_card.review')
                        </section>

                        <section ng-cloak class="panel_section" ng-if="(order.billing.config.template_key  === 'emt')">
                            @include('shop.checkout.billing.emt.review')
                        </section>

                        <section ng-cloak class="panel_section" ng-if="(order.billing.config.template_key  === 'cod')">
                            @include('shop.checkout.billing.cod.review')
                        </section>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="float_panel account_customer">
                        <div class="panel_header">
                            <h3 class="panel_title">Shipping</h3>
                        </div>

                        <section class="panel_section">
                            <h3 class="section_title">%% order.shipping.method_title %%</h3>
                        </section>

                        <section ng-cloak class="panel_section" ng-if="(order.shipping.config.template_key === 'delivery')">
                            @include('shop.checkout.shipping.delivery.review')
                        </section>

                        <section ng-cloak class="panel_section" ng-if="(order.shipping.config.template_key === 'pickup')">
                            @include('shop.checkout.shipping.pickup.review')
                        </section>
                    </div>
                </div>
            </div>
        </section>
         -->

        <section class="content_section">
            <div class="float_panel account_customer">
                <div class="panel_header">
                    <h3 class="panel_title">Order Items</h3>
                </div>

                <section class="panel_section no_pad">
                    <div class="cart_items_list">
                        @foreach($order->get_items() as $item)

                            <div class="list_item cart_item">
                                <div class="item_image">
                                    <img src="{!! $item->product->get_cover_image_url() !!}" />
                                </div>

                                <div class="item_info">
                                    <div class="item_title">{!! $item->product->get_title() !!}</div>

                                    <div class="item_price">
                                        ${{ $item->item_cost }}
                                    </div>

                                    <div class="item_summary">
                                        @if($item->product->is_variation())
                                            <span class="summary_item">{{ $item->variation->display_attributes_str }}</span>
                                        @else
                                            <span class="summary_item">{{ $item->price_option->price_title }}</span>
                                        @endif


                                    </div>

                                    <div class="item_actions">
                                        <div class="action_quantity">
                                            <span>QTY: {{ $item->item_quantity }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </section>
            </div>
        </section>

    </div>
</div>