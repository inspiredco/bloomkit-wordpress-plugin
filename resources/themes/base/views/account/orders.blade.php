<div class="body_section">
    <h3 class="section_title">Active Orders</h3>

    <div class="section_body body_contain">
        <bloom-customer-orders-table></bloom-customer-orders-table>
    </div>
</div>