<div ng-controller="AccountDashCtrl">
    <div class="body_section">
        <div class="section_body body_contain">
            <div class="row">
                <div class="col-md-5 col-lg-3">
                    <div class="box_panel panel_profile">
                        <header class="profile_header">
                            <div class="header_icon">
                                <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                            </div>

                            <div class="header_customer">
                                <span class="customer_name"><?=$customer->customer_name_full; ?></span>
                                <span class="customer_email"><?=$customer->customer_email; ?></span>
                                <span class="customer_date"><?=date('F j, Y', strtotime($customer->customer_date_created)); ?></span>
                            </div>
                        </header>

                        <!--
                        <div class="profile_row row no-gutters">
                            <div class="profile_data col-md-6">
                                <span class="data_label">Current Orders</span>
                                <span class="data_value">0</span>
                            </div>

                            <div class="profile_data col-md-6">
                                <span class="data_label">Total Orders</span>
                                <span class="data_value">0</span>
                            </div>
                        </div>
                        -->

                        <div class="profile_actions">
                            <a href="{!! bk_get_route_url('account', 'edit') !!}" class="btn btn_default action_block_btn">Edit Profile</a>

                            @if(bk_allow_buy())
                                <a href="{!! bk_get_route_url('account', 'orders') !!}" class="btn btn_default">My Orders</a>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="col-md-7 col-lg-9">

                    @if($customer->customer_verified_email !== 'Y')
                        <section class="content_section">
                            <h4>Account Notices</h4>

                            <div class="alert alert-danger">
                                Your email address is not verified!
                            </div>
                        </section>
                    @endif

                    <!--
                    <section class="content_section">
                        <h4>Your Orders</h4>

                        <bloom-customer-orders-table></bloom-customer-orders-table>
                    </section>
                    -->

                    <section class="content_section section_border">
                        <div class="row">
                            <div class="col-12 col-md-8">
                                <h4>Referral Program</h4>
                                <p>No one left behind! Invite your friends, get free stuff.</p>
                            </div>

                            <div class="col-12 col-md-4">
                                <div class="section_actions">
                                    <primo-invite-button></primo-invite-button>
                                </div>

                            </div>
                        </div>
                    </section>

                    <section class="content_section">
                        <h4>Following Brands</h4>

                        <div class="section_body">
                            @if(!empty($customer->follows) && !empty($customer->follows->brands))
                                <div class="account_followed_brands">
                                    <div class="customer_followed_list customer_followed_brands_list">
                                        @foreach($customer->follows->brands as $follow)
                                            @if(!empty($follow->brand))
                                                <a href="{!! $follow->brand->get_url() !!}" class="list_item list_item_brand">
                                                    <div class="brand_logo">
                                                        <img src="{!! $follow->brand->get_post_field_image_url('brand_media_logo') !!}" />
                                                    </div>
                                                </a>
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            @else
                                <div class="alert alert-info">
                                    You are not following any brands!
                                </div>
                            @endif

                        </div>
                    </section>

                    @if(bk_allow_buy())
                        <section class="content_section section_border">
                            <div class="section_header">
                                <h4>
                                    Active Orders
                                </h4>
                            </div>

                            <div class="section_body">
                                <bloom-customer-orders-table></bloom-customer-orders-table>
                            </div>
                        </section>
                    @endif

                    @if(bk_allow_buy() && false)
                        <section class="content_section section_border">
                            <div class="section_header">
                                <h4>
                                    Saved Addresses
                                    <small>Manage your saved shipping addresses. Save your addresses for a faster checkout. </small>
                                </h4>
                            </div>

                            <div class="section_body">
                                <customer-address-manager customer="customer"></customer-address-manager>
                            </div>
                        </section>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
