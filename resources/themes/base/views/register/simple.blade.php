<?php
if(bk_allow_register()) {
    ?>

    <?php
    bk_json_load_var([
        'invite_token' => $invite_token
    ]);
    ?>

    <div class="body_section pad_top_small">
        <div class="section_body body_splash body_contain">
            <div class="customer_register" ng-controller="CustomerRegisterCtrl">

                <div class="register_view" ng-if="stage_success()">
                    <header class="register_header splash_header">
                        <h1 class="header_title">
                            Welcome to Primo!
                        </h1>
                    </header>

                    <div class="splash_body">
                        <div class="alert alert-success">
                            <p>Your registration was successful!</p>
                        </div>


                        <div class="register_actions_row">
                            <a href="<?= bk_get_route_url('account'); ?>" class="btn btn_small action_btn_back">Your Account</a>
                            <a href="<?= home_url(); ?>" class="btn btn_small btn_primary action_btn_submit">Start Shopping</a>
                        </div>
                    </div>
                </div>

                <div class="register_view" ng-if="!(stage_success())">
                    <header class="register_header splash_header">
                        <h1 class="header_title">
                            %% config.header.title %%
                            <small>%% config.header.text %%</small>
                        </h1>
                    </header>

                    <div class="splash_body">
                        <div class="form_errors" ng-if="!(errors | isEmpty)">
                            <div class="alert alert-danger" role="alert" ng-repeat="error in errors track by $index">
                                %% error %%
                            </div>
                        </div>

                        <form class="form" ng-submit="submit()">
                            <div class="form_row">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="field_block">
                                            <label>First Name</label>
                                            <input type="text" class="field" name="customer_name_first" ng-model="fields.customer_name_first"/>
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6">
                                        <div class="field_block">
                                            <label>Last Name</label>
                                            <input type="text" class="field" name="customer_name_last" ng-model="fields.customer_name_last" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form_row">
                                <?php
                                if (empty($invite_token)) {
                                    ?>

                                    <div class="field_block">
                                        <label>Email</label>
                                        <input type="email" class="field" ng-model="fields.customer_email" name="customer_email" />
                                    </div>

                                    <?php
                                }
                                ?>


                                <div class="field_block">
                                    <div class="register_newsletter_field">
                                        <md-checkbox ng-model="fields.newsletter_subscribe" aria-label="I would like to receive the newsletter">
                                            I would like to receive the newsletter (you can unsubscribe from the newsletter at any time).
                                        </md-checkbox>
                                    </div>
                                </div>
                            </div>

                            <div class="form_row">
                                <div class="row">
                                    <div class="col-12 col-md-6">
                                        <div class="field_block">
                                            <label>Password</label>
                                            <input type="password" class="field" ng-model="fields.customer_pass" name="customer_pass" />
                                        </div>
                                    </div>

                                    <div class="col-12 col-md-6">
                                        <div class="field_block">
                                            <label>Confirm Password</label>
                                            <input type="password" class="field" ng-model="fields.customer_pass_confirm" name="customer_pass_confirm" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="register_actions_row">
                                <button type="submit" class="btn btn_small btn_primary action_btn_submit">
                                    Create Your Account
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
    
    <?php
}
?>