<script>
    var APP_LOAD = <?= json_encode([ 'steps' =>  $steps]); ?>;
</script>

<div class="body_section pad_top_small">
    <div class="section_body body_splash body_contain">
        <div class="customer_register" ng-controller="CustomerRegisterCtrl">
            <header class="register_header splash_header">
                <h1 class="header_title">
                    %% config.header.title %%
                    <small>%% config.header.text %%</small>
                </h1>

                <div class="header_progress">
                    <nav class="nav_dot_progress">
                        <div class="progress_bar">
                            <div class="bar_progress" ng-style="progress_bar_width()"></div>
                        </div>

                        <ul>
                            <li ng-repeat="(key, step) in config.steps track by $index" class="progress_item" ng-class="step_classes(key)">
                                <div class="item_icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <span class="item_label">%% step.title %%</span>
                            </li>
                        </ul>
                    </nav>
                </div>
            </header>

            <div class="splash_body">
                <div class="form_errors" ng-if="!(errors | isEmpty)">
                    <div class="alert alert-danger" role="alert" ng-repeat="error in errors track by $index">
                        %% error %%
                    </div>
                </div>

                <form class="form" ng-submit="submit()">
                    <div class="form_view" ng-if="step_active('account')">
                        <div class="form_row">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="field_block">
                                        <label>First Name</label>
                                        <input type="text" class="field" ng-model="fields.customer_name_first" />
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="field_block">
                                        <label>Last Name</label>
                                        <input type="text" class="field" ng-model="fields.customer_name_last" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form_row">
                            <div class="field_block">
                                <label>Email</label>
                                <input type="email" class="field" ng-model="fields.customer_email" />
                            </div>

                            <div class="field_block">
                                <div class="register_newsletter_field">
                                    <md-checkbox ng-model="isChecked" aria-label="Sign me up for Primo emails">
                                        Sign me up for Primo emails (you can unsubscribe from the newsletter at any time).
                                    </md-checkbox>
                                </div>
                            </div>
                        </div>

                        <div class="form_row">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="field_block">
                                        <label>Password</label>
                                        <input type="password" class="field" ng-model="fields.customer_pass" />
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="field_block">
                                        <label>Confirm Password</label>
                                        <input type="password" class="field" ng-model="fields.customer_pass_confirm" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form_view" ng-if="step_active('experience')">
                        <div class="customer_meta_row">
                            <label class="meta_label">How often do you use cannabis?</label>
                            <p class="meta_instruct">At Primo, we want to know exactly how often you smoke in order to be able to provide you with the best product to hit that need. So tell us, do you toke daily?</p>

                            <div class="meta_field">
                                <select class="field" ng-model="fields.meta.frequency">
                                    <option value="rarely">Rarely</option>
                                    <option value="weekly">Weekly</option>
                                    <option value="daily">Daily</option>
                                    <option value="hourly">Hourly</option>
                                </select>
                            </div>
                        </div>

                        <div class="customer_meta_row">
                            <label class="meta_label">Preferred method of use?</label>
                            <p class="meta_instruct">This is a description about the types of methods used to get high. Joints. Dabbing. Pills. Edibles. Sprays. Need some copy here that gives off brand personality.</p>

                            <div class="meta_field">
                                <select class="field" ng-model="fields.meta.method">
                                    <option value="cbd">CBD</option>
                                    <option value="bong">Bong</option>
                                    <option value="vape">Vape</option>
                                    <option value="joint">Joint</option>
                                </select>
                            </div>
                        </div>


                        <div class="customer_meta_row">
                            <label class="meta_label">Which effect best describes you?</label>
                            <p class="meta_instruct">The types of highs out there are infinite, but eveyrone has that special feeling that they’re always chasing after. We can help you find just that.</p>

                            <div class="meta_field">
                                <select class="field" ng-model="fields.meta.effect">
                                    <option value="effect1">Effect 1</option>
                                    <option value="effect2">Effect 2</option>
                                    <option value="effect3">Effect 3</option>
                                    <option value="effect4">Effect 4</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form_view" ng-if="step_active('success')">
                        <div class="box_splash">

                        </div>
                    </div>

                    <div class="register_actions_row">
                        <button type="button" class="btn btn_small action_btn_back" ng-click="go_back()" ng-if="allow_go_back()">Go Back</button>
                        <button type="submit" class="btn btn_small btn_primary action_btn_submit">Create Your Account</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>