<div class="body_section pad_top_small">
    <div class="section_body body_splash body_contain">
        <div class="register_view">
            <header class="register_header splash_header">
                <h1 class="header_title">
                    Email Verified!
                </h1>
            </header>

            <div class="splash_body">
                <p>Your registration was successful!</p>

                <div class="register_actions_row">
                    <a href="<?=bk_get_route_url('account'); ?>" class="btn btn_small action_btn_back">Your Account</a>
                    <a href="<?=home_url(); ?>" class="btn btn_small btn_primary action_btn_submit">Start Shopping</a>
                </div>
            </div>
        </div>
    </div>
</div>
