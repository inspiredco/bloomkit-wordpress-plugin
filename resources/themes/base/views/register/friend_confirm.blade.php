@if($friend_action === 'accept')
    <div class="body_section pad_top_small">
        <div class="section_body body_splash body_contain">
            <div class="register_view">
                <header class="register_header splash_header">
                    <h1 class="header_title">
                        Friend Request Accepted!
                    </h1>
                </header>

                <div class="splash_body">
                    <p>You are now friends</p>

                    <div class="register_actions_row">
                        <a href="{!! bk_get_route_url('account') !!}" class="btn btn_small btn_primary action_btn_submit">Return to Account</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@elseif($friend_action === 'decline')
    <div class="body_section pad_top_small">
        <div class="section_body body_splash body_contain">
            <div class="register_view">
                <header class="register_header splash_header">
                    <h1 class="header_title">
                        Friend Request Declined!
                    </h1>
                </header>

                <div class="splash_body">
                    <p>You are not friends</p>

                    <div class="register_actions_row">
                        <a href="{!! bk_get_route_url('account') !!}" class="btn btn_small btn_primary action_btn_submit">Return to Account</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@else
    <div class="body_section pad_top_small">
        <div class="section_body body_splash body_contain">
            <div class="register_view">
                <header class="register_header splash_header">
                    <h1 class="header_title">
                        Confirm Friend Request
                    </h1>
                </header>

                <div class="splash_body">
                    <p>You've been invited to join Primo. We see you are already a member, confirm this friend request.</p>

                    <div class="register_actions_row">
                        <a href="{{ $accept_url }}" class="btn btn_small btn_primary action_btn_submit">Accept</a>
                        <a href="{{ $decline_url }}" class="btn btn_small action_btn_back">Decline</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
