<?php
if(bk_allow_register()) {
    ?>


    <?php
    bk_json_load_var([

    ]);
    ?>

    <div class="body_section body_contain_md">
        <div class="row">
            <div class="col-lg-6 col-md-12">

                <div class="float_box">
                    <div class="box_header">
                        <span class="header_subtitle">Welcome Back</span>
                        <span class="header_title">{{ $customer->customer_name_full }}</span>

                        <div class="header_meta_row meta_email">
                            <i class="fa fa-envelope"></i> {{ $customer->customer_email }}
                        </div>

                        <div class="header_meta_row meta_verified_text">
                            <i class="fa fa-certificate"></i> Your email address has been verified
                        </div>

                        <div class="text_block_lg">
                            <p>You recently completed an order with The BWELL Market, but did not provide a password to complete your account creation.</p>
                            <p>Please provide a password to secure your account and complete the signup process.</p>
                        </div>
                    </div>


                    <form method="post" action="">
                        <div class="box_body body_bg_alt">
                            <div class="row">
                                <div class="col-12 col-md-6">
                                    <div class="field_block">
                                        <label>Password</label>
                                        <input type="password" class="field" name="customer_pass" value="" />
                                    </div>
                                </div>

                                <div class="col-12 col-md-6">
                                    <div class="field_block">
                                        <label>Confirm Password</label>
                                        <input type="password" class="field" name="customer_pass_confirm" value="" />
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="box_footer">
                            <button type="submit" class="btn btn_small btn_primary action_btn_submit">
                                Complete Account Registration
                            </button>
                        </div>
                    </form>

                </div>
            </div>

            <div class="col-lg-6 col-md-12">
                <div class="loyalty_point_summary">
                    <div class="summary_points_earned">
                        <div class="earned_points">
                            <div class="points_circle">
                                {{ $customer->loyalty->points_available }}
                            </div>
                        </div>

                        <div class="earned_label">
                            <span class="label_secondary">You earned these</span>
                            <span class="label_primary">You earned these</span>
                        </div>
                    </div>
                </div>

                <h1>The Benefits of a Bwell Account</h1>
                <p>The Bwell Market isn’t just another account, it is your door to to a new era of health & wellness. With access to America’s top brands and hundreds of products, this might be your most fun and satisfying account ever. </p>

                <ul class="icon_list icon_verified">
                    <li>Collect Loyalty Points and member rewards</li>
                    <li>Save your address and payment information for an express checkout</li>
                    <li>Follow your favourite brands and review your favourite products</li>
                    <li>Receive first dibs on promotions and exclusive products</li>
                </ul>

            </div>
        </div>
    </div>
    
    <?php
}
?>