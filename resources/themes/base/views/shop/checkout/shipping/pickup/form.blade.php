<div class="option_steps">
    <div class="option_step open">
        <div class="step_header">
            <div class="step_icon">
                <i class="fas fa-map-marker-alt"></i>
            </div>

            <div class="step_title">
                Select Your Store
            </div>
        </div>

        <div class="step_body">
            <select class="form-control" ng-model="order.order_shipping_method_value.store_id">
                <option value="0"> -- Select Pickup Location -- </option>
                <option ng-repeat="store in config.shipping_methods[0].config.stores track by $index" value="%% store.store_id %%"> %% store.store_name %%</option>
            </select>
        </div>
    </div>
</div>