<div class="checkout_review_pickup">
    <div class="pickup_summary_store">
        <div class="row">
            <div class="col-md-4">
                <img class="store_image" src="https://images.media.bloomkit.co/unsafe/200x200/common/sample_store_cover.png" />
            </div>

            <div class="col-md-8">
                <div class="store_info">
                    <div class="store_name">
                        %% order.shipping.value.store.store_name %%
                    </div>

                    <div class="store_address">
                        <address class="address">
                            <span class="address_street">404 Internet Ave</span>
                            <span class="address_street">North Vancouver, BC, Canada</span>
                            <span class="address_post_code">V7L404</span>
                        </address>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
