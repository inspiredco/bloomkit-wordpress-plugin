
<div class="checkout_method_summary">
    <div class="summary_row">
        <h5 class="summary_title">Shipping Rate</h5>
        <div class="block_shipping_rate">
            <div class="rate_title">%% order.shipping.value.rate.title %%</div>
            <div class="rate_arrival_est">Est Arrival: %% order.shipping.value.rate.date_arrival_est %%</div>
        </div>
    </div>

    <div class="summary_row">
        <h5 class="summary_title">Shipping Address</h5>
        <bloom-address-display address="order.shipping.value.address"></bloom-address-display>
    </div>
</div>
