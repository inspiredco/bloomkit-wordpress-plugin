<div class="option_steps">
    <div class="option_step open">
        <div class="step_header">
            <div class="step_icon">
                <i class="fas fa-map-marker-alt"></i>
            </div>

            <div class="step_title">
                Set Your Address
            </div>
        </div>

        <div class="step_body">
            {{--<checkout-customer-address-selector address="order.order_shipping_method_value.address"></checkout-customer-address-selector>--}}
        </div>
    </div>

    <div class="option_step">
        <div class="step_header">
            <div class="step_icon">
                <i class="fas fa-truck"></i>
            </div>

            <div class="step_title">
                Select Rate
            </div>
        </div>

        <div class="step_body">
            <div class="alert alert-info">
                Select or create an address first!
            </div>
        </div>
    </div>
</div>