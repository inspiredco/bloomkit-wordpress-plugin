<div class="checkout_method_summary">

    <div class="summary_row">
        <h5 class="summary_title">Credit Card</h5>
        <div class="block_cc">
            <span class="cc_holder">%% order.billing.value.card.card_holder_name %%</span>
            <span class="cc_number">
                <span class="cc_type_badge">%% order.billing.value.card.card_type %%</span>
                %% order.billing.value.card.card_masked_num %%
            </span>
            <span class="cc_expiry">%% order.billing.value.card.card_expiry_month %% / %% order.billing.value.card.card_expiry_year %%</span>
        </div>
    </div>

    <div class="summary_row">
        <h5 class="summary_title">Billing Address</h5>
        <bloom-address-display address="order.billing.value.card.address"></bloom-address-display>
    </div>
</div>