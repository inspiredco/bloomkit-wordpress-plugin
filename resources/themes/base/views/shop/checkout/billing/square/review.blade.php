<div class="checkout_method_summary">
    <div class="summary_row">
        <h5 class="summary_title">Cardholder Information</h5>
        <div class="block_cc">
            <p>@{{ order.billing.value.card.card_holder_name }}</p>
            <p class="cc_number"><span class="cc_type_badge">@{{ order.billing.value.card.card_type }}</span>@{{ order.billing.value.card.card_masked_num }}</p>
            <p>@{{ order.billing.value.card.card_expiry_month }} / @{{ order.billing.value.card.card_expiry_year }}</p>
        </div>
    </div>

    <div class="summary_row">
        <h5 class="summary_title">Billing Address</h5>
        <div class="item_body">
            <p>@{{ order.billing.value.card.address.address_street_1 }}</p>
            <p v-if="order.billing.value.card.address.address_street_2">@{{ order.billing.value.card.address.address_street_2 }}</p>
            <p>@{{ order.billing.value.card.address.city.city_title }}, @{{ order.billing.value.card.address.city.region_abbrev }}</p>
            <p>@{{ order.billing.value.card.address.city.country_title }}, @{{ order.billing.value.card.address.address_post_code }}</p>
        </div>
    </div>
</div>