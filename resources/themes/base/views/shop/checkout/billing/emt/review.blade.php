<div class="checkout_billing_method_summary">
    <div class="summary_row">
        <label class="summary_label">Send Email To</label>
        <span class="summary_value">%% order.billing.config.recipient_email %%</span>
    </div>

    <div class="summary_row">
        <label class="summary_label">Security Question</label>
        <span class="summary_value">Order #%% order.order_id %%</span>
    </div>

    <div class="summary_row">
        <label class="summary_label">Pass Phrase</label>
        <span class="summary_value">%% order.billing.config.pass_phrase %%</span>
    </div>
</div>