<div class="box_splash">
    <header class="splash_header">
        <h2>Thank you for your purchase!</h2>
        <h4 class="header_underline">Order #%% order.order_id %%</h4>
    </header>

    <div class="splash_body">
        <p>You will receive an email confirmation shortly with order details and a link to track it’s progress. Thanks for shopping with us!</p>
    </div>
</div>