<div class="checkout_review">
    <section class="content_section">
        <div class="row">
            <div class="col-lg-6">
                <div class="float_panel checkout_review_panel">
                    <div class="panel_header">
                        <h3 class="panel_title">Payment Method</h3>
                    </div>

                    <section class="panel_section">
                        <h3 class="section_title">%% order.billing.method_title %%</h3>
                    </section>

                    <section ng-cloak class="panel_section" ng-if="(order.billing.config.template_key === 'credit_card')">
                        @include('shop.checkout.billing.credit_card.review')
                    </section>

                    <section ng-cloak class="panel_section" ng-if="(order.billing.config.template_key  === 'emt')">
                        @include('shop.checkout.billing.emt.review')
                    </section>

                    <section ng-cloak class="panel_section" ng-if="(order.billing.config.template_key  === 'cod')">
                        @include('shop.checkout.billing.cod.review')
                    </section>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="float_panel account_customer">
                    <div class="panel_header">
                        <h3 class="panel_title">Shipping</h3>
                    </div>

                    <section class="panel_section">
                        <h3 class="section_title">%% order.shipping.method_title %%</h3>
                    </section>

                    <section ng-cloak class="panel_section" ng-if="(order.shipping.config.template_key === 'delivery')">
                        @include('shop.checkout.shipping.delivery.review')
                    </section>

                    <section ng-cloak class="panel_section" ng-if="(order.shipping.config.template_key === 'pickup')">
                        @include('shop.checkout.shipping.pickup.review')
                    </section>
                </div>
            </div>
        </div>
    </section>


    <section class="content_section">
        <!--<customer-cart-items-table items="order.items" edit="no" config="cart_config"></customer-cart-items-table>-->
    </section>
</div>