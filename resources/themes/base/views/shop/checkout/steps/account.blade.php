<div class="checkout_step">
    <div class="checkout_step_section div_bottom" ng-if="registering_customer()">
        <div class="section_header">
            <h3 class="section_title">Already A Member?</h3>
        </div>

        <div class="section_body">
            <div class="checkout_account_login">

                <div class="row">
                    <div class="col-xs-12 col-md-5">
                        <input type="text" class="field" placeholder="Your Email Address..." name="bk_customer_login_user" ng-model="customer.customer_login_email"/>
                    </div>

                    <div class="col-xs-12 col-md-5">
                        <input type="password" class="field" placeholder="Your Password..." name="bk_customer_login_pass" ng-model="customer.customer_login_pass" />
                    </div>

                    <div class="col-xs-12 col-md-2">
                        <button type="button" class="flat_btn" ng-click="checkout_login()">Login</button>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <div class="checkout_step_section ">
        <div class="section_header" ng-if="registering_customer()">
            <h3 class="section_title">Your First Order?</h3>
        </div>

        <div class="section_header" ng-if="!registering_customer()">
            <h3 class="section_title">Your Information</h3>
        </div>

        <div class="section_body">
            <div class="form_row">
                <div class="row">
                    <div class="col">
                        <label>First Name</label>
                        <input type="text" class="field" name="customer_name_first" ng-model="customer.customer_name_first" />
                    </div>

                    <div class="col">
                        <label>Last Name</label>
                        <input type="text" class="field" name="customer_name_last" ng-model="customer.customer_name_last" />
                    </div>
                </div>
            </div>

            <div class="form_row">
                <label>Email</label>
                <input type="text" class="field" ng-model="customer.customer_email" />
            </div>

            <div class="form_row" ng-if="(customer.customer_id | isEmpty)">
                <div class="row">
                    <div class="col">
                        <label>Password</label>
                        <input type="password" class="field" name="customer_pass" ng-model="customer.customer_pass" value="" />
                    </div>

                    <div class="col">
                        <label>Confirm Password</label>
                        <input type="password" class="field" name="customer_pass_confirm" ng-model="customer.customer_pass_confirm" value="" />
                    </div>
                </div>
            </div>
        </div>

    </div>


</div>