<section class="checkout_step">
    <div class="step_body">
        <div class="checkout_shipping_options method_options">
            <md-radio-group ng-model="order.order_shipping_method_id" ng-change="change_shipping_method()">
                @foreach($shipping_methods as $method)
                    <div class="option_item" ng-class="{ 'selected' : (order.order_shipping_method_id === '{{ $method->method_id }}')}">
                        <div class="item_header">
                            <md-radio-button value="{{ $method->method_id }}" class="md-primary"></md-radio-button>
                            <strong>{{ $method->method_title }}</strong>
                        </div>

                        @if(!empty($method->config->template_key))
                            <div class="item_body no_pad" ng-if="(order.order_shipping_method_id === '{{ $method->method_id }}')">
                                @include("shop.checkout.shipping.{$method->config->template_key}.form")
                            </div>
                        @endif
                    </div>
                @endforeach

            </md-radio-group>
        </div>
    </div>
</section>