<script>
    <?php
        $app_load = json_encode([
            'steps' => $steps,
            'order' => $order,
            'totals' => $totals,
            'shipping_methods' => $shipping_methods,
            'billing_methods' => $billing_methods
        ]);
    ?>

    var APP_LOAD =  <?=$app_load; ?>;
</script>

<div ng-controller="StoreCheckoutCtrl">

    <div class="shop_checkout">
        <header class="checkout_header">
            <nav class="nav_dot_progress">
                <div class="progress_bar">
                    <div class="bar_progress" ng-style="progress_bar_width()"></div>
                </div>

                <ul>
                    <li ng-repeat="(key, step) in config.steps track by $index" class="progress_item" ng-class="step_classes(key)" ng-click="step_switch(key)">
                        <div class="item_icon">
                            <i class="fa fa-check"></i>
                        </div>
                        <span class="item_label">%% step.title %%</span>
                    </li>
                </ul>
            </nav>
        </header>

        <form method="post" class="form" ng-submit="checkout_submit()">
            <div class="checkout_body row">
                <div class="col body_main">


                    <checkout-loyalty-points-alert points="loyalty_points_config"></checkout-loyalty-points-alert>


                    <div class="process_steps">
                        <div class="step_errors">
                            <div class="alert alert-danger" ng-repeat="(field, error) in config.step_errors">
                                %% error %%
                            </div>
                        </div>

                        <section ng-cloak class="step_item" ng-if="step_active('account')">
                            @include('shop.checkout.steps.account')
                        </section>

                        <section ng-cloak class="step_item" ng-if="step_active('shipping')">
                            @include('shop.checkout.steps.shipping')
                        </section>

                        <section ng-cloak class="step_item" ng-if="step_active('billing')">
                            @include('shop.checkout.steps.billing')
                        </section>

                        <section ng-cloak class="step_item" ng-if="step_active('review')">
                            @include('shop.checkout.steps.review')
                        </section>

                        <section ng-cloak class="step_item" ng-if="step_active('complete')">
                            @include('shop.checkout.steps.success')
                        </section>
                    </div>
                </div>

                <div class="body_sidebar col-md-4 col-lg-3">
                    <div class="box_panel">
                        <h3 class="panel_title header_underline">Order Summary</h3>

                        <order-totals-summary totals="totals"></order-totals-summary>

                        <div class="panel_section">
                            <checkout-coupon-form code="order.coupon_code"></checkout-coupon-form>
                        </div>

                        <div class="panel_actions">
                            <button type="submit" class="btn btn_primary btn_block">Continue</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

</div>