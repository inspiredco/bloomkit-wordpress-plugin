<div class="grid_item item_product">
    <div class="item_product grid_item_product">
        <a href="{!! $product->get_url() !!}" class="product_image">
            <img src="{!! $product->get_cover_image_url() !!}" alt="{!! $product->get_title() !!}" />
        </a>


        <div class="product_info">
            <h3 class="product_title">{!! $product->get_title() !!}</h3>

            <div class="product_rating">
                {!! $product->get_rating_average() !!}/5 ({!! $product->get_rating_count() !!} Reviews)
            </div>
        </div>

        <div class="product_price">
            <span class="price_single">${!! $product->get_price_base_value() !!}</span>
        </div>
    </div>
</div>