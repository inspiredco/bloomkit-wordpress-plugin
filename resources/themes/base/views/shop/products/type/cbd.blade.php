<div class="product_grid_item item1">
    <div class="product_image">
        <a href="{!! $product->get_url() !!}" class="product_image">
            <img src="{!! $product->get_cover_image_url() !!}" alt="{!! $product->get_title() !!}" />
        </a>
    </div>
    <div class="product_info">
        <p class="product_name">{!! $product->get_title() !!}</p>
        <p class="product_description">${!! $product->get_price() !!} &nbsp;&bull;&nbsp; {!! $product->get_brand_title() !!}</p>
    </div>
</div>