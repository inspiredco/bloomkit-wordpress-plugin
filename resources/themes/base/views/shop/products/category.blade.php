<?php
bk_json_load_var([
    //'categories' => $cats,
    'category' => $category->category
]);
?>

<div class="body_section pad_top_small" ng-controller="BloomProductIndexCtrl">
    <div class="section_body body_contain">
        <div class="shop_browse">
            <div class="browse_main">
                <div class="browse_header">
                    <h1 class="header_title"><?=$category->category->cat_title; ?></h1>
                </div>

                @include('shop.common.toolbar')

                <div class="browse_child_cats">
                    <?php
                    $current_id = $category->category->cat_id;

                    if(!empty($cat_nav['items'])) {
                        foreach ($cat_nav['items'] as $i => $level) {
                            $all_active = '';

                            if (empty($cat_nav['path'][$i])) {
                                $all_cat = $category->category;
                            } else {
                                $all_cat = $cat_nav['path'][$i];
                            }

                            if ($i >= (count($cat_nav['active']) - 1)) {
                                $all_active = 'active';
                            }
                            ?>

                            <div class="cats_nav_row shop_cat_nav_bar">
                                <div class="nav_mobile_scroll small">
                                    <div class="scroll_track">
                                        <div class="track_slide">
                                            <a class="<?= $all_active; ?>" href="<?= $all_cat->url; ?>">All <?= $all_cat->cat_title; ?></a>

                                            <?php
                                            foreach ($level as $item) {
                                                $active_class = (in_array($item->category->cat_id, $cat_nav['active'])) ? 'active' : '';
                                                ?>

                                                <a class="<?= $active_class; ?>" href="<?= $item->category->url; ?>"><?= $item->category->cat_title; ?></a>

                                                <?php
                                            }
                                            ?>
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <?php
                        }
                    }
                    ?>
                </div>


                <product-browse-viewer search="viewer_config"></product-browse-viewer>
            </div>

        </div>
    </div>
</div>

<?php
/*
if(!empty($category->category->cat_desc)) {
    ?>

    <div class="body_section body_contain cat_seo seo_section">
        <p><?= $category->category->cat_desc; ?></p>
    </div>

    <?php
}
*/


