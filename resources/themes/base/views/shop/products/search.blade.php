<?php
bk_json_load_var([
    //'categories' => $cats,
    'search_text' => $search_text
]);
?>

    <div class="body_section pad_top_small" ng-controller="BloomProductIndexCtrl">
        <div class="section_body body_contain">
            <div class="shop_browse">

                <div class="browse_main">
                    <div class="browse_header">
                        <h1 class="header_title">Search Results</h1>
                    </div>

                    @include('shop.common.toolbar')

                    <product-browse-viewer search="viewer_config"></product-browse-viewer>
                </div>

            </div>
        </div>
    </div>

<?php
/*
if(!empty($category->category->cat_desc)) {
    ?>

    <div class="body_section body_contain cat_seo seo_section">
        <p><?= $category->category->cat_desc; ?></p>
    </div>

    <?php
}
*/


