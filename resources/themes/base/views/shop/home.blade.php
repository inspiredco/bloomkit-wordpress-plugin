<?php

?>
<div class="body_banner">
    <img style="width:100%" src="{!! get_theme_static_url('img/shop_splash.png') !!}" />
</div>

<div class="body_contain">
    @include('shop.common.toolbar')
</div>


<div class="body_section body_contain">
    <div class="section_header">
        <h2 class="section_title">Featured Flowers</h2>
        <a href="{!! bk_get_route_url('shop', 'weed/category/38/') !!}" class="section_link">View All <i class="fas fa-chevron-right"></i></a>
    </div>

    <div class="section_body">
        <?=do_shortcode('[bloom_shop_embed_products limit="4" category="38"]'); ?>
    </div>
</div>

<div class="body_section body_contain">
    <div class="section_header">
        <h2 class="section_title">Featured Accessories</h2>
        <a href="<?=site_url('/shop/weed-accessories/category/41/'); ?>" class="section_link">View All <i class="fas fa-chevron-right"></i></a>
    </div>

    <div class="section_body">
        <?=do_shortcode('[bloom_shop_embed_products limit="4" category="41"]'); ?>
    </div>
</div>

<div class="body_section body_contain">
    <div class="section_header">
        <h2 class="section_title">Featured Vape Pens</h2>
        <a href="<?=site_url('/shop/weed-vape-pens-kits/category/39/'); ?>" class="section_link">View All <i class="fas fa-chevron-right"></i></a>
    </div>

    <div class="section_body">
        <?=do_shortcode('[bloom_shop_embed_products limit="4" category="39"]'); ?>
    </div>
</div>

<div class="body_section body_contain">
    <h2 class="section_title">Browse By Category</h2>

    <div class="section_body cat_link_grid">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <a href="<?=site_url('/shop/weed/category/38/'); ?>">WEED</a>
            </div>

            <div class="col-xs-12 col-md-4">
                <a href="<?=site_url('/shop/weed-vape-pens-kits/category/39/'); ?>">VAPE PENS</a>
            </div>

            <div class="col-xs-12 col-md-4">
                <a href="<?=site_url('/shop/weed-accessories/category/41/'); ?>">WEED ACCESSORIES</a>
            </div>
        </div>
    </div>
</div>
