@if(!empty($cart))
    <script>
        var APP_LOAD = <?= json_encode([ 'cart' =>  $cart]); ?>;
    </script>
@endif

<div class="checkout_cart" ng-controller="CheckoutCartCtrl">
    <div class="body_section body_contain_max">

        <div class="cart_header">
            <h2 class="section_title">Your Shopping Bag</h2>
        </div>


        <div class="section_body">

            <div class="cart_review">

                <div class="review_cart">
                    <customer-cart-items-table items="cart.items"></customer-cart-items-table>
                </div>

                @if($no_cart !== true)
                    <div class="review_order">
                        <div class="box_panel">
                            <h3 class="panel_title header_underline">Order Summary</h3>

                            <order-totals-summary totals="cart.totals"></order-totals-summary>

                            <div class="panel_actions">
                                <a href="{!! bk_get_route_url('checkout') !!}" class="btn btn_primary btn_block">Checkout</a>
                            </div>
                        </div>
                    </div>
                @endif

            </div>
        </div>
    </div>
</div>