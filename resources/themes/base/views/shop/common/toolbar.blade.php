<div class="shop_toolbar">
    <div class="row align-items-center">
        <div class="col-sm-12 col-lg-6">
            <nav class="product_cat_path">
                <a href="{!! site_url('shop') !!}" class="path_item">Shop</a>

                @if(!empty($cat_nav['path']))
                    @foreach($cat_nav['path'] as $path)
                        <a href="{{ $path->url }}" class="path_item">{{ $path->cat_title }}</a>
                    @endforeach
                @endif

                @if(!empty($category->category->cat_title))
                    <a href="" class="path_item">{{ $category->category->cat_title }}</a>
                @else
                    <a href="" class="path_item">All</a>
                @endif
            </nav>
        </div>

        <div class="col-sm-6 col-lg-3">
            <customer-my-store-hud></customer-my-store-hud>
        </div>

        <div class="col-sm-6 col-lg-3">
            <div class="toolbar_search">
                <form class="search_form" action="{!! site_url('shop/search') !!}" method="POST">
                    <input type="text" name="bloom_product_search" class="form-control search_field" placeholder="Search Products..." />
                    <button type="submit" class="search_btn"><i class="fas fa-search"></i></button>
                </form>

            </div>
        </div>
    </div>
</div>