<customer-cart-hud>
    <a href="<?=site_url('checkout'); ?>" class="btn btn_primary btn_small">CHECKOUT</a>
    <a href="<?=site_url('cart'); ?>" class="btn btn_secondary btn_small">VIEW CART</a>

    <?php
    if(bk_customer_is_auth()) {
    ?>

    <a href="<?=site_url('my-account'); ?>" class="btn btn_secondary btn_small">VIEW ACCOUNT</a>
    <a href="<?=site_url('my-account/logout'); ?>" class="btn btn_secondary btn_small">LOGOUT</a>

    <?php
    } else {
    ?>

    <a href="<?=site_url('login'); ?>" class="btn btn_secondary btn_small">LOGIN</a>

    <?php
    if(bk_allow_register()) {
    ?>

    <a href="<?=site_url('register'); ?>" class="btn btn_secondary btn_small">REGISTER</a>

    <?php
    }
    ?>



    <?php
    }
    ?>
</customer-cart-hud>