<script>
    APP_CONFIG.product = {!! json_encode($product->get_data()) !!};
</script>
<?php
//look($product->get_reviews());
//$product = $product->get_data();

$rating_type_values = [
    'weed_highness' => ['title' => 'Highness', 'values' => ['buzzed', 'middle', 'flying']],
    'weed_flavour' => ['title' => 'Flavour', 'values' => ['sweet', 'middle', 'sour']],
    'weed_effect' => ['title' => 'Effect', 'values' => ['relaxed', 'middle', 'hyper']],
];
?>

<script type="application/ld+json">
{
   "@context":"https:\/\/schema.org\/",
   "@type":"Product",
   "@id":"{{ $product->get_url() }}",
   "name":"{{ $product->get_title() }}",
   "image":"{{ $product->get_cover_image_url() }}",
   "description":"A hybrid meant to knock you out into a dream realm made for kings and queens, OG Poison or Poison OG. The $15 discount is already worked into the ounce price :) OG Poison strain is potent and fast-acting. This gorgeous flower was designed to knock out even the worst cases of insomnia. The nose and cure on this flower is beautiful. The nose is super pungent... smells like diesel mixed with vibrant earth. The combo is enough to knock your head back. Just make sure you have a place to store it if scent is of concern! This strain is long-lasting and provides a very soothing, heavy body sensation for users. Lastly, the nugs on the OG Poison are simply gorgeous. Big and dense. Caked in milky trichomes and the nugs have purple and blue hues. This could easily be a $240-250 per ounce strain but our growers love ya! Effects: Sleep, Sleep, Sleep, Relax Medical Effects: Stress, Insomnia, Pain THC: 27%",
   "sku":"poisonHIGHflower",
   "offers":[
      {
         "@type":"AggregateOffer",
         "lowPrice":"35.00",
         "highPrice":"200.00",
         "priceCurrency":"CAD",
         "availability":"https:\/\/schema.org\/InStock",
         "url":"https:\/\/getwhitepalm.com\/product\/og-poison-strain-highland\/",
         "seller":{
            "@type":"Organization",
            "name":"Mail Order Marijuana | Canada&#039;s Best Online Dispensary - GetWhitePalm",
            "url":"https:\/\/getwhitepalm.com"
         }
      }
   ],
   "aggregateRating":{
      "@type":"aggregateRating",
      "bestRating":"5",
      "worstRating":"0",
      "ratingValue":"5",
      "ratingCount":"1"
   }
}
</script>

<div class="shop_product_single" ng-controller="BloomProductSingleCtrl">
    <div class="body_section pad_top_small">
        <div class="section_body body_contain">
            <div class="breadcrumbs">
                <nav class="product_cat_path">
                    <?php echo primo_product_cat_path_html($product->get_data()); ?>
                </nav>
            </div>

            <div class="product_single_wrap">
                <?php
                $type_template = $product->get_type_name() ?: 'default';
                ?>

                @include("shop.product.type.{$type_template}")
            </div>
        </div>
    </div>
    <?php //look($product->get_data()); ?>

    <?php
    if(bk_allow_buy()) {
        ?>

        <div class="body_section bg_alt">
            <div class="review_rating">
                <div class="label_rating">
                    <span class="rating_value" id="product_rating_label"><?= $product->get_rating_average('?'); ?></span>
                </div>

                <div class="rating_title">
                    <span id="product_rating_count"><?= $product->get_rating_count(); ?></span> Reviews
                </div>
            </div>

            <div class="rating_totals">
                <?php
                $ratings = $product->get_data()->ratings;

                if (!empty($ratings)) {

                    $rating_values = [];
                    $rating_avgs = [];

                    foreach ($ratings as $rating) {
                        $key = str_replace('product_', '', $rating->rating_type);

                        if (!empty($rating_type_values[$key])) {
                            $values = $rating_type_values[$key]['values'];
                            $rating_values[$key][] = ((array_search($rating->value, $values) + 2) * $rating->count);
                        }
                    }

                    foreach ($rating_values as $key => $ratings) {
                        $label = $rating_type_values[$key]['title'];
                        $values = $rating_type_values[$key]['values'];
                        $values_count = count($values);

                        $value_max = $values[($values_count - 1)];
                        $value_min = $values[0];

                        $value_sum = array_sum($ratings);

                        $value_avg = ($value_sum / count($ratings));

                        $value_percent = (($value_avg / $value_sum) * 100);

                        ?>

                        <div class="rating_item">
                            <span class="rating_label"><?= $label; ?></span>

                            <div class="rating_scale">
                                <span class="scale_label"><?= $value_min; ?></span>
                                <div class="scale_bar">
                                    <div class="bar_point" style="left: <?= $value_percent; ?>%;"></div>
                                    <div class="bar_line"></div>
                                </div>
                                <span class="scale_label"><?= $value_max; ?></span>
                            </div>
                        </div>

                        <?php
                    }
                }
                ?>
            </div>
            <div class="review_write_btn">
                <a ng-click="write_review()" class="btn btn_small">WRITE REVIEW</a>
            </div>

            <div class="section_body body_contain">
                <reviews-viewer reviews="product.reviews"></reviews-viewer>
            </div>
        </div>

        <?php
    }
    ?>
</div>

<div class="body_section body_contain">
    <div class="section_header">
        <h2 class="section_title">Similar Products</h2>
    </div>


    <div class="section_body">
        <?=do_shortcode('[bloom_shop_embed_products limit="4"]'); ?>
    </div>
</div>

<script>
    $(function() {
        $(".share_bar").Primo_Share({
            shares : [
                { share: "facebook"},
                { share: "twitter"},
                { share: "pinterest"}
            ]
        });
    });
</script>