
<?=bk_jsonld_review($review); ?>

<div class="viewer_item">
    <div class="item_review_meta">
        <div class="meta_header">
            <div class="meta_author">
                @if (!empty($review->customer))
                    <div class="author_image">

                        @if(!empty($review->review_data->ratings->overall))
                        <div class="meta_rating_overall">
                            <div class="label_rating rating_small">
                                <span class="rating_value review_rating_value">{{ $review->review_data->ratings->overall }}</span>
                            </div>
                        </div>
                        @endif

                        <!--<img src='{{ $review->customer->customer_cover_image->image->sizes->thumb->url }}' />-->
                    </div>
                    <div class="author_info">
                        <span class="author_name">{{ $review->customer->customer_name_abbrev }}</span>
                        <span class="author_date">{{ date('m/d/Y', strtotime($review->review_date_created)) }}</span>
                    </div>
                @else
                    <div class="author_info">
                        <span class="author_name">Anonymous</span>
                    </div>
                @endif
            </div>
        </div>

        <!--
        <div class="meta_ratings">
            <div class="rating_item">
                <span class="rating_label">Highness</span>
                <div class="rating_scale">
                    <span class="scale_label">Buzzed</span>
                    <div class="scale_bar">
                        <div class="bar_point" style="left: %% rating.percent %%;"></div>
                        <div class="bar_line"></div>
                    </div>
                    <span class="scale_label">Flying</span>
                </div>
            </div>
            <div class="rating_item">
                <span class="rating_label">Flavour</span>
                <div class="rating_scale">
                    <span class="scale_label">Sweet</span>
                    <div class="scale_bar">
                        <div class="bar_point" style="left: %% rating.percent %%;"></div>
                        <div class="bar_line"></div>
                    </div>
                    <span class="scale_label">Sour</span>
                </div>
            </div>
            <div class="rating_item">
                <span class="rating_label">Effect</span>
                <div class="rating_scale">
                    <span class="scale_label">Relaxed</span>
                    <div class="scale_bar">
                        <div class="bar_point" style="left: %% rating.percent %%;"></div>
                        <div class="bar_line"></div>
                    </div>
                    <span class="scale_label">Hyper</span>
                </div>
            </div>
        </div>
        -->

    </div>

    <div class="item_review_main">
        <div class="item_review_main_body">
            <h4 class="item_review_title">{{ $review->review_data->title }}</h4>
            <p class="item_review_comment">{!! $review->review_data->comment !!}</p>
        </div>

        @if(!empty($review->media))
            <div class="review_images">
                @foreach($review->media as $media)
                    <div class="review_image_item">
                        <a class="review_image" href="{{ $media->image->sizes->large->url }}" data-rel="lightcase:reviewImages{{$review->review_id}}">
                            <img src="{{ $media->image->sizes->thumb->url }}" />
                        </a>
                    </div>
                @endforeach
            </div>
        @endif
    </div>
</div>
