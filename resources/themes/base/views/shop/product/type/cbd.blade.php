<div class="product_single">
    <div class="single_main">
        <div class="row">
            <div class="col-sm-12 col-md-7 col-lg-6">
                <div class="main_image_wrap">
                    <product-image-gallery images="product.media.images" cover="product.product_cover_image.image"></product-image-gallery>
                </div>
            </div>

            <div class="col-sm-12 col-md-5 col-lg-5">
                <div class="main_info_wrap">
                    <div class="info_header">
                        <h1 class="header_title"><?=$product->get_title(); ?></h1>
                        <?php
                        if($product->has_brand()) {
                        ?>

                        <h3 class="header_brand"><?=$product->get_brand_title(); ?></h3>

                        <?php
                        }
                        ?>
                    </div>

                    <?php
                    if(bk_allow_buy()) {
                    ?>

                    <div class="info_row">
                        <div class="product_cart_add">
                            <product-add-to-cart product="product"></product-add-to-cart>
                        </div>
                    </div>

                    <?php
                    }
                    ?>

                    <div class="info_row">
                        <h5 class="row_title">Product Details</h5>

                        <div class="product_desc">
                            <?=$product->get_desc(); ?>
                        </div>
                    </div>

                    <div class="info_row">
                        <div class="share_bar"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<?php //look($product->get_data()); ?>