<div class="product_main">
    <div class="row">
        <div class="col-md-6 col-lg-5 product_image_col">
            <div class="product_image image_brand_logo">
                <img src="<?=$product->get_brand_logo(); ?>" />
            </div>
        </div>

        <div class="col">
            <div class="product_header">
                <?php
                if($product->has_brand()) {
                    ?>

                    <h3 class="product_brand"><?=$product->get_brand_title(); ?></h3>

                    <?php
                }
                ?>

                <h1 class="product_title"><?=$product->get_title(); ?></h1>
            </div>

            <div class="product_desc">
                <?=$product->get_desc(); ?>
            </div>

            <div class="product_weed_summary">

            </div>
        </div>
    </div>
</div>