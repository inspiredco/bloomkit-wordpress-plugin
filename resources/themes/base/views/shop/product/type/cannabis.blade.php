<?php

$sativa = round($product->get_fields('sativa'));
$indica = round($product->get_fields('indica'));

$rating_type_values = [
    'weed_highness' => ['title' => 'Highness', 'values' => ['buzzed', 'middle', 'flying']],
    'weed_flavour' => ['title' => 'Flavour', 'values' => ['sweet', 'middle', 'sour']],
    'weed_effect' => ['title' => 'Effect', 'values' => ['relaxed', 'middle', 'hyper']],
];

//look($product->get_data());
?>

<?php
/*
$stats = ['THC', 'CBD', 'CBN', 'CBG'];

foreach($stats as $stat) {
$key = strtolower($stat);
$value = round($product->get_fields($key));

<?php
}
*/
?>

<div class="product_single">
    <div class="single_main">
        <div class="row">
            <div class="col-sm-12 col-md-7 col-lg-6">
                <div class="main_image_wrap">
                    <product-image-gallery images="product.media.images" cover="product.product_cover_image.image"></product-image-gallery>
                </div>
            </div>

            <div class="col-sm-12 col-md-5 col-lg-5">
                <div class="main_info_wrap">
                    <div class="product_header">
                        <h1 class="product_title"><?=$product->get_title(); ?></h1>



                        <?php
                        if($product->has_brand()) {
                            ?>

                            <h3 class="product_brand"><?=$product->get_brand_title(); ?></h3>

                            <?php
                        }
                        ?>

                        <div class="row">
                            <div class="col"></div>
                            <div class="col">
                                <div class="product_strain_tag">
                                    <?=$product->get_strain_species(); ?> DOMINANT
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php
                    if(bk_allow_buy()) {
                        ?>
                        <div class="info_row">
                            <div class="product_cart_add">
                                <product-add-to-cart product="product"></product-add-to-cart>
                            </div>
                        </div>

                        <?php
                    }
                    ?>

                    <div class="info_row no_border">
                        <h5 class="row_title">Product Details</h5>

                        <div class="product_desc">
                            <?=$product->get_desc(); ?>
                        </div>

                        <div class="product_details">
                            <?php
                            $details = ['cut', 'cure', 'dry', 'environment', 'batch', 'flush'];
                            $detail_blocks = [];
                            $icon_map = [
                            'cut_hand_cut' => 'Primo_Trimmed_Hand.svg',
                            'cut_machine_cut' => 'Primo_Trimmed_Machine.svg',
                            'dry_hung_dry' => 'Primo_Drying_Hang.svg',
                            'dry_rack_dry' => 'Primo_Drying_Rack.svg',
                            'batch_small_batch' => 'Primo_Quantity_SmallBatch.svg',
                            'batch_large_batch' => 'Primo_Quantity_LargeBatch.svg',
                            'flush_short_flush' => 'Primo_Flushtime_Short.svg',
                            'flush_long_flush' => 'Primo_Flushtime_Long.svg',
                            'cure_weeks' => 'Primo_Curetime_Weeks.svg',
                            'cure_months' => 'Primo_Curetime_Months.svg',
                            'environment_indoor' => 'Primo_Climate_Indoors.svg',
                            'environment_greenhouse' => 'Primo_Climate_GreenHouse.svg',
                            'environment_outdoor' => 'Primo_Climate_Outdoor.svg'
                            ];

                            foreach($details as $field) {
                            $value = $product->get_fields('grow_'.$field);

                            if(!empty($value)) {
                            $value_icon_key = $field.'_'.trim(preg_replace('/[^0-9a-z]+/i', '_', strtolower($value)), '_');
                            $icon = '';

                            //look($value_icon_key);

                            if(!empty($icon_map[$value_icon_key])) {
                            $icon = $icon_map[$value_icon_key];
                            }

                            $detail_blocks[] = [$field, $value, $icon];
                            }
                            }

                            if(!empty($detail_blocks)) {
                            ?>

                            <div class="block_body">

                                <div class="brand_features">
                                    <?php
                                    foreach($detail_blocks as $block) {
                                    $icon_url = get_theme_static_url('img/product_detail_icons/'.$block[2]);

                                    ?>

                                    <div class="feature_item item_<?=$block[0]; ?>" style="background-image: url('<?=$icon_url; ?>');">
                                        <?=$block[1]; ?>
                                    </div>

                                    <?php
                                    }
                                    ?>
                                </div>
                            </div>

                            <?php
                            }
                            ?>
                        </div>
                    </div>



                    <div class="info_row">
                        <h5 class="row_title">Product Effects</h5>

                        <div class="product_effects">
                            <div class="effect_meters">
                                @foreach($product->get_fields('effect')->general as $effect)
                                    <div class="meter_row">
                                        <div class="meter_bar">
                                            <div class="meter_bar_label">{{ $effect->effect_title }}</div>
                                            <div class="meter_bar_fill" style="width:{{ $effect->value_value }}%"></div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="info_row">
                        <h5 class="row_title">Medical Effects</h5>

                        <div class="product_effects">
                            <div class="effect_meters">
                                @foreach($product->get_fields('effect')->medical as $effect)
                                    <div class="meter_row">
                                        <div class="meter_bar">
                                            <div class="meter_bar_label">{{ $effect->effect_title }}</div>
                                            <div class="meter_bar_fill" style="width:{{ $effect->value_value }}%"></div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            <div class="effect_meters">
                                @foreach($product->get_fields('effect')->negative as $effect)
                                    <div class="meter_row">
                                        <div class="meter_bar">
                                            <div class="meter_bar_label">{{ $effect->effect_title }}</div>
                                            <div class="meter_bar_fill" style="width:{{ $effect->value_value }}%"></div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="info_row">
                        <h5 class="row_title">What People Are Saying</h5>

                        <div class="info_ratings">
                            <div class="rating_reviews">
                                <div class="reviews_total">
                                    <div class="total_avg">
                                        <div class="label_rating rating_small">
                                            <span class="rating_value" id="product_rating_label"><?=$product->get_rating_average('?'); ?></span>
                                        </div>
                                    </div>

                                    <div class="total_count">
                                        <h6 class="count_value"><?=$product->get_rating_count(); ?> Reviews</h6>
                                    </div>
                                </div>
                            </div>

                            <div class="rating_cats">
                                <div class="rating_totals">
                                    <?php
                                    $ratings = $product->get_data()->ratings;

                                    //look($ratings);

                                    if(!empty($ratings)) {

                                        $rating_values = [];
                                        $rating_avgs = [];

                                        foreach($ratings as $rating) {
                                            $key = str_replace('product_', '', $rating->rating_type);

                                            if(!empty($rating_type_values[$key])) {
                                                $values = $rating_type_values[$key]['values'];
                                                $rating_values[$key][] = ((array_search($rating->value, $values) + 2) * $rating->count);
                                            }
                                        }

                                        foreach($rating_values as $key => $ratings) {
                                            $label = $rating_type_values[$key]['title'];
                                            $values = $rating_type_values[$key]['values'];
                                            $values_count = count($values);

                                            $value_max = $values[($values_count - 1)];
                                            $value_min = $values[0];

                                            $value_sum = array_sum($ratings);

                                            $value_avg = ($value_sum / count($ratings));

                                            $value_percent = (($value_avg / $value_sum) * 100);

                                            ?>

                                            <div class="rating_item">
                                                <span class="rating_label"><?=$label; ?></span>

                                                <div class="rating_scale">
                                                    <span class="scale_label"><?=$value_min; ?></span>
                                                    <div class="scale_bar">
                                                        <div class="bar_point" style="left: <?=$value_percent; ?>%;"></div>
                                                        <div class="bar_line"></div>
                                                    </div>
                                                    <span class="scale_label"><?=$value_max; ?></span>
                                                </div>
                                            </div>

                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="info_row">
                        <div class="share"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>