<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;


use App\Classes\Wordpress;
use App\Classes\Config;
use App\Classes\Debug;
use App\Classes\Plugin;
use App\Classes\Sitemap;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        //Debug::look('boot init');


        Config::load();

        if(defined('BK_KEY_PAGES')) {
            $route_pages = BK_KEY_PAGES;

            Config::set('route_pages', $route_pages);
        }

        view()->addLocation(get_template_directory().'/bloomkit');
        view()->addLocation(BK_PLUGIN_DIR.'/resources/themes/base/views/');


        add_action('init', function() {
           //Debug::look('boot init');
            Plugin::init();
        });

        add_action('wp', function() {
            //Debug::look('boot wp');
            Plugin::init_page();

        });

        add_action('wp_loaded', function() {
            //Debug::look('boot wp looaded');
            Plugin::init_wp();
        });

//        add_action('widgets_init', function() {
//            Wordpress::register_widgets();
//        });





        Wordpress::add_shortcode('blog_posts', function() {
            $brand_posts = get_posts_by_type('post', [
                'posts_per_page' => 3,
//                'cat' => 5
            ]);

            foreach($brand_posts as $i => $post) {
                include(get_template_directory().'/template-parts/blog/posts/post-ticket.php');
            }
        });

        Wordpress::add_shortcode('blog_posts_from_serialized_ids', function($attrs = []) {

            $brand_posts = unserialize($attrs['posts']);

            $posts = get_posts_by_type('post', [
                'post__in' => $brand_posts
            ]);

            foreach($posts as $i => $post) {
                include(get_template_directory().'/template-parts/blog/posts/post-ticket.php');
            }
        });
    }

    public function boot() {





    }
}
