<?php
namespace App\Providers;

use App\Http\Controllers\BrandsController;
use Illuminate\Support\ServiceProvider;

use App\Classes\Debug;
use App\Classes\Wordpress;
use App\Classes\Routes;



class BrandsServiceProvider extends ServiceProvider {
    /**
     * Boot
     * Use this method to load any code you need to load on boot.
     */
    public function register() {

        Routes::register('brands', function() {
            return new BrandsController();
        }, [
            'title' => 'Brands Page'
        ]);

//        Wordpress::add_xhr_profile('register', function() {
//            RegisterController::route_api();
//        });


        Wordpress::add_rewrite_rule('^(weed-brands)/(.*)', 'index.php?pagename=$matches[1]&query=$matches[2]', [
            'query' => '(.*)'
        ]);

        Wordpress::add_rewrite_rule('^(cbd-brands)/(.*)', 'index.php?pagename=$matches[1]&query=$matches[2]', [
            'query' => '(.*)'
        ]);

        Wordpress::add_shortcode('brands', function($attrs = []) {
            echo BrandsController::shortcode_index($attrs);
        });
    }
}