<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
//use App\Classes\Wordpress;

use App\Classes\Debug;
use App\Classes\Customer;
use App\Classes\Routes;
use App\Classes\Wordpress;
use App\Classes\Website;

use App\Http\Controllers\ShopController;
use App\Http\Controllers\ShopCheckoutController;
use App\Http\Controllers\ShopCartController;

class ShopServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register() {
        Website::$_requests['store'] = true;

        $shop_base_slug = 'buy';
        $checkout_base_slug = 'checkout';

        Routes::register('shop', function() {
            return new ShopController();
        }, [
            'title' => 'Shop Page',
            'lang_keys' => [
                'es' => 'comprar'
            ]
        ]);

        Routes::register('checkout', function() {
            return new ShopCheckoutController();
        }, [
            'title' => 'Checkout Page'

        ]);

        Routes::register('cart', function() {
            return new ShopCartController();
        }, [
            'title' => 'Cart Page'
        ]);



        Wordpress::add_shortcode('customer_hud', function($attrs = []) {
            echo ShopController::shortcode_customer_hud($attrs);
        });







        Wordpress::add_widget('Customer_HUD_Widget');
        Wordpress::add_widget('Customer_Account_HUD_Widget');
        Wordpress::add_widget('Newsletter_Widget');
    }

    public function boot() {

    }
}

