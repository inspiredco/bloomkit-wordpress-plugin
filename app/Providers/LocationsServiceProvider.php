<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Classes\Wordpress;
use App\Classes\Debug;
use App\Classes\Routes;

use App\Http\Controllers\LocationsController;

class LocationsServiceProvider extends ServiceProvider {
    public function register() {

//        Wordpress::add_css('bk-location-map', bk_get_theme_static_url('css/app.84077338.css'), '', '', 'all', false);
//
//        Wordpress::add_js('bk-location-map', bk_get_theme_static_url('js/app.cc681162.js'), '', '', true, false);
//        Wordpress::add_js('bk-location-map-vendors', bk_get_theme_static_url('js/chunk-vendors.0084d553.js'), '', '', true, false);

        Wordpress::add_shortcode('locations_map', function($attrs = []) {
            echo LocationsController::shortcode_map($attrs);
        });

        Wordpress::add_xhr_profile('locations', function() {
            LocationsController::route_api();
        });
    }
}