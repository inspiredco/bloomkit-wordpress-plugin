<?php
namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Classes\Debug;
use App\Classes\Wordpress;
use App\Classes\Website;
use App\Classes\Routes;


use App\Http\Controllers\CustomerController;

use App\Http\Controllers\AccountController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterController;

class AccountServiceProvider extends ServiceProvider {
    /**
     * Boot
     * Use this method to load any code you need to load on boot.
     */
    public function register() {
        Routes::register('account', function() {
            return new AccountController();
        }, [
            'title' => 'Account Page'
        ]);

        Routes::register('login', function() {
            return new LoginController();
        }, [
            'title' => 'Login Page'
        ]);

        Routes::register('register', function() {
            return new RegisterController();
        }, [
            'title' => 'Register Page'
        ]);
    }
}