<?php 
namespace Atomos\Plugin\Controllers;

use Atomos\Plugin\Classes\Debug;

use Atomos\Plugin\Classes\WordpressPosts;

class DispensariesController extends BaseFrontController {

    public static function render_map() {
        $search = [
            'numberposts' => '',
            'offset' => 0,
            'cat' => '',
            'orderby' => 'post_date',
            'order' => 'DESC',
            'include' => '',
            'exclude' => '',
            'meta_key' => '',
            'meta_value' => '',
            'post_type' => 'dispensary',
            'post_status' => 'publish',
            'suppress_filters' => true
        ];

        $posts = WordpressPosts::find($search);

        //look($posts);

        return view('dispensaries/map');
    }

    public static function route() {
        $out = self::render_map();

        return $out;
    }

    public static function route_api() {
        $post = input()->all();

        switch($post['method']) {
            case 'review_submit':

            break;
        }

        self::output_json();
    }

    public static function render_html() {
        return self::route();
    }
}