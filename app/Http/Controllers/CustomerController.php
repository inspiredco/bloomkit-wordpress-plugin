<?php
namespace App\Http\Controllers;

use App\Classes\Email;
use App\Classes\Debug;
use App\Classes\Orders;
use App\Classes\Customer;


class CustomerController extends BaseFrontController {

    public static function route_api() {
        $post = self::get_post();

        if(Customer::is_auth()) {
            switch ($post['method']) {
                case 'save':
                    self::api_save();
                break;

                case 'order_get':
                    self::api_orders_get();
                break;

                case 'orders_search':
                    self::api_orders_search();
                break;

                case 'reset_pass':
                    self::api_reset_pass();
                break;

                case 'follow':
                    self::api_follow();
                break;

                case 'invite':
                    self::api_invite();
                break;
            }
        } else {
            switch ($post['method']) {
                case 'register':
                    self::api_register();
                break;
            }
        }

        self::output_json();
    }

    public static function route_orders() {
        $action = self::get_query(1);


        $out = '';

        switch($action) {
            case 'view':
                $out = self::render_order_view();
            break;

            default:
                $out = self::render_order_index();
            break;
        }

        return $out;
    }

    public static function api_register() {
        $post = self::get_post();

        if(!empty($post)) {

            $customer = $post['customer'];

            if(!empty($customer)) {
                $register = Customer::register($customer);

               //Debug::look($register);

                if($register->is_success()) {
                    self::json_success();
                    self::json('customer', Customer::get_data());
                } else {
                    self::json('errors', $register->get_errors());
                }
            }
        }
    }

    public static function api_save() {
        $post = self::get_post();

        //look('API Save1');

        //look($post);

        if(!empty($post)) {
            //look('API Save');

            $customer = $post['customer'];
            $errors = [];

            //look($customer);
            $req = Customer::save($customer);

            //look($save);

            if($req->is_success()) {
                $save = $req->get_data();

                if($save->success === true) {
                    Customer::save_token_cookie();

                    self::json_success();
                    self::json('customer', Customer::get_data());
                }
            }

        }
    }

    public static function api_follow() {
        $post = self::get_post();

        $follow_id = (int)$post['id'];

        if(!empty($follow_id)) {
            $follow = Customer::follow('brand', $follow_id, $post['follow']);
        }

        self::json_success();
    }

    public static function api_invite() {
        $post = self::get_post();

        if(!empty($post)) {
            $invite = Customer::invite($post['invites']);
        }
        


        self::json_success();
    }

    public static function api_reset_pass() {
        $post = input()->all();

        //look($post);

        if(!empty($post)) {
            $action = Customer::change_pass($post['invites']);
        }

        wp_mail('postle@inspired.co', 'Reset Your Password', 'test!!');

        self::json_success();
    }

    public static function api_orders_get() {
        $post = self::get_post();
    }

    public static function api_orders_search() {
        $post = self::get_post();
        $config = [];

        if(!empty($post['status'])) {
            $config['status'] = $post['status'];
        }

        $search = Orders::search($config);

        if(!empty($search)) {
            self::json_success();
            self::json('orders', $search['orders']);
        }
    }
}