<?php
namespace App\Http\Controllers;

/**
 * Base Controller.
 * Extend the base controller to get the core functionality within your controllers.
 * 
 */
class BaseFrontController extends BaseController {
    public static $_json_data = [];
    public static $_query = [];


    public function set_page_title($title) {
        add_filter('wp_title', function($title) {
            return 'PRODUCT22';
        }, 100, 2);
    }

    public static function set_query() {
        $var = get_query_var('query');

        if(!empty($var)) {
            self::$_query = explode('/', $var);
        }
    }

    public static function get_query($idx = false) {
        if($idx !== false) {
            if(!empty(self::$_query[$idx])) {
                return self::$_query[$idx];
            }

            return false;
        }

        return self::$_query;
    }

    public static function get_post($key = false) {
        return $_POST;
    }

    public static function json($key, $value) {
        if(!empty($key)) {
            self::$_json_data[$key] = $value;
        }
    }


    public static function json_success($state = true) {
        self::json('success', $state);
    }

    public static function get_json() {
        return json_encode(self::$_json_data);
    }

    public static function output_json() {
        wp_send_json(self::$_json_data);
    }
} 