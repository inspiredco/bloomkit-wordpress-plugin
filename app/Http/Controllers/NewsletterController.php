<?php 
namespace Atomos\Plugin\Controllers;

use Atomos\Plugin\Classes\Website;
use Atomos\Plugin\Classes\Debug;
use Atomos\Plugin\Classes\Newsletter;

use Atomos\Plugin\Classes\ProductBrands;

class NewsletterController extends BaseFrontController {

    function __construct() {

    }

    public static function route() {
        self::set_query();
    }

    public static function route_api() {
        $post = input()->all();

        switch($post['method']) {
            case 'submit':
                self::api_submit();
            break;
        }

        self::output_json();
    }

    public static function api_submit() {
        $post = input()->all();

        if(!empty($post)) {
            if(!empty($post['email'])) {
                $subscribe = Newsletter::subscribe($post['email']);

                if($subscribe['success'] === true) {
                    self::json_success();
                } else {
                    self::json('error', $subscribe['error']);
                }
            }
        }

       //$save = Checkout::save($post);



        //self::json('order', Checkout::get_order()->get_data());
        //self::json('customer', Checkout::get_customer());
    }


    public static function render_html() {
        echo self::route();
    }
}