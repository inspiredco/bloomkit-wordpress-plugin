<?php
namespace App\Http\Controllers;

use App\Classes\Website;
use App\Classes\Debug;
use App\Classes\Wordpress;
use App\Classes\Customer;


class LoginController extends BaseFrontController {

    public static function register() {
        Wordpress::add_rewrite_rule('^.*/(login)/(.*)', 'index.php?pagename=$matches[1]&query=$matches[2]', [
            'query' => '(.*)'
        ]);
    }

    public static function route() {
        self::set_query();

        $method = self::get_query(0);
        $out = '';

        //look($method);
        switch($method) {
            case 'reset-password-success':
                $out = self::login_reset_pass_success();
            break;

            case 'reset-password':
                $out = self::login_reset_pass();
            break;

            case 'forgot-password':
                $out = self::login_forgot_pass();
            break;

            default:
                $out = self::login_register_form();
            break;
        }


        return $out;
    }

    public static function login_reset_pass_success() {
        $data['success'] = true;

        return view('login/reset-password', $data);
    }

    public static function login_reset_pass() {
        $post = self::get_post();

        $data = [];

        $id = self::get_query(1);
        $hash = self::get_query(2);

        //look($hash);
        //look($post);

        if(!empty($post['reset_password'])) {
            if($post['reset_password'] === $post['reset_password_confirm']) {
//                $send = Customer::change_pass($post['reset_password']);
                $send = Customer::reset_pass($id, $hash, $post['reset_password']);

                if($send->is_success()) {
                    //wp_redirect(site_url('login/reset-password-success'));

                    if (!empty($send->_model_out_data['data']->customer)) {
                        Customer::load($send->_model_out_data['data']->customer);
                    }
                    $data['success'] = true;
                    return view('login/reset-password', $data);
                } else {
                    $error = 'Something went wrong. Please try again.';
                }
            } else {
                $error = 'Passwords do not match.';
            }
        } else {
            $error = 'Please provide a reset password.';
        }


        //look($error);
        if (! empty($error)) {
            $data['error'] = $error;
        }

        return view('login/reset-password', $data);
    }

    public static function login_forgot_pass() {
        $post = self::get_post();
        $data = [];

        if(Customer::is_auth()) {
            //wp_redirect('account');
        }

        //look($post);

        if(!empty($post['bk_forgot_pass_email'])) {
            $send = Customer::forgot_pass($post['bk_forgot_pass_email']);

            $data['success'] = true;
        }

        return view('login/forgot-password', $data);
    }

    public static function login_register_form() {
        $post = self::get_post();
        $data = [];

        if(Customer::is_auth() === true) {
            wp_redirect(bk_get_route_url('account'));
        }

        if(!empty($post)) {
            if(!empty($post['bk_customer_login_user']) && !empty($post['bk_customer_login_pass'])) {
                $login = Customer::login($post['bk_customer_login_user'], $post['bk_customer_login_pass']);

                if($login === true) {
                    wp_redirect(bk_get_route_url('account'));
                } else {
                    $data['error'] = 'The username or password you entered was not correct!';
                }
            } else {
                $data['error'] = 'Your email address and password are required.';
            }
        }

        return view('login-register', $data);
    }

    public static function render_html() {
        return self::route();
    }
}