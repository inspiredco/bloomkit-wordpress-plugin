<?php
namespace App\Http\Controllers;

use App\Classes\Website;
use App\Classes\Debug;
use App\Classes\Wordpress;

use App\Classes\Customer;
use App\Classes\Bloomkit;
use App\Classes\Email;

class RegisterController extends BaseFrontController {

    public static $_config = [];
    public static $_data = [];


    public static function register() {
        Wordpress::add_rewrite_rule('^(register)/(.*)', 'index.php?pagename=$matches[1]&query=$matches[2]', [
            'query' => '(.*)'
        ]);

        // DEPRECATING
        Wordpress::add_xhr_profile('register', function() {
            RegisterController::route_api();
        });
    }

    /**
     * @return \Illuminate\View\View|string
     */
    public static function route() {
        self::set_query();

        $method = self::get_query(0);
        $out = '';

        self::$_data['customer'] = Customer::get_data();

        self::$_config['steps'] = [
            'details' => [
                'title' => 'My Account',
                'url' => '',
            ],

            'secure' => [
                'title' => 'Profile',
                'url' => '',
            ],

            'complete' => [
                'title' => 'Complete',
                'url' => '',
            ]
        ];

        self::$_config['step'] = $method ?: 'details';

        self::$_data['invite_token'] = false;

        self::set_steps();

        switch($method) {
            case 'verify':
                $out = self::render_verify();
            break;

            case 'contest':
                $out = self::render_home_contest();
            break;

            case 'complete-signup':
                $out = self::render_home_complete_signup();
            break;

            default:
                if($method === 'invite') {
                    $token = base64_decode(self::get_query(1));

                    //Debug::look([$token]);

                    $req = Bloomkit::post('customer/invited', [
                        'token' => $token
                    ]);

                    //Debug::look($invite);

                    if($req->is_success()) {
                        $invite = $req->get_data();

                        //Debug::look($invite);

                        if($invite->success === true) {
                            self::$_data['invite_token'] = $token;
                        }
                    }
                }

                if (Customer::is_auth() && !empty($token)) {
                    $out = self::render_friend_confirm($token);
                } else {
                    $out = self::render_home();
                }
            break;
        }

        //Debug::look($out);

        return $out;
    }

    /**
     * Signup page for customers registered through the Bwell Content
     *
     * @return \Illuminate\View\View
     */
    public static function render_home_contest() {

        //$token = '94a8f389b181102ce4cc50023c8b54c6';
        $token = self::get_query(1);

        if(!empty($token)) {
            $req = Bloomkit::post('contest/winner', [
                'token' => $token
            ]);

            if($req->is_success()) {
                $contest = $req->get_data();

                //Debug::look($contest);

                if($contest->success === true) {


                    if(!empty($contest->customer)) {
                        Customer::load($contest->customer);

                        //look($contest->customer);

                        self::$_data['customer'] = $contest->customer;
                    }

                    if(!empty($contest->winner)) {
                        self::$_data['winner'] = $contest->winner;
                    }

                    if(!empty($contest->contest)) {
                        self::$_data['contest'] = $contest->contest;
                    }
                }
            }
        }

        return view('register.contest', self::$_data);
    }


    /**
     * Complete Signup
     *
     * Meant for guest customer accounts that were created during checkout, but not assigned a password.
     * An email would be sent to customer including a token/verify link, which would lead here.
     *
     * @return \Illuminate\View\View
     */
    public static function render_home_complete_signup() {
        $post = self::get_post();

        $token = self::get_query(2);
        $cid = (int)self::get_query(1);

        $customer = false;

        if(Customer::is_auth()) {
            $status = Customer::get_data('customer_status');

            if($status === 2) {

            }
        }

        //Customer::is_auth() === false &&
        if(!empty($token) && !empty($cid)) {

            $req = Bloomkit::post('customer/get', [
                'id' => $cid,
                'token' => $token
            ]);

            //look($req);

            if($req->is_success()) {
                $complete = $req->get_data();

                //Debug::look($contest);

                if($complete->success === true) {
                    if(!empty($complete->customer)) {
                        // We found the customer and are ready to complete signup...
                        Customer::load($complete->customer);

                        // Save the customer auth cookie.
                        Customer::save_token_cookie();

                        self::$_data['customer'] = $complete->customer;

                        if(!empty($post)) {
                            if(!empty($post['customer_pass']) && $post['customer_pass'] === $post['customer_pass_confirm']) {
                                $save = Customer::save([
                                    'customer_pass' => $post['customer_pass'],
                                    'customer_pass_confirm' => $post['customer_pass_confirm']
                                ]);

                                //look($save);

                                if($save->is_success()) {
                                    $res = $save->get_data();


                                    wp_redirect(bk_get_route_url('account'));
                                }
                            }
                        }
                    }
                }
            }
        }

        return view('register.complete', self::$_data);
    }

    /**
     * @return \Illuminate\View\View
     */
    public static function render_home() {
        if(Customer::is_auth()) {
            wp_redirect(bk_get_route_url('account'));
        }

        //Email::send('scott@foxo.ca', 'Welcome to Primo!', 'Test!!');

        return view('register.simple', self::$_data);
    }

    /**
     * @param $token
     * @return \Illuminate\View\View
     */
    public static function render_friend_confirm($token) {
        $post = self::get_post();

        $action = self::get_query(2);

        //Debug::look([$token]);

        if(!empty($token)) {
            $req = Bloomkit::post('customer/invited', [
                'token' => $token
            ]);

           //look($req);
//            look($token);
//
            //Debug::look($invite);

            if($req->is_success()) {
                $invite = $req->get_data();

                //Debug::look($invite);

                if($invite->success === true) {
                    self::$_data['invite_token'] = $token;

                }
            }
        }

        if(!empty($action)) {
            if($action === 'accept') {

            } elseif($action === 'decline') {

            }

            self::$_data['friend_action'] = $action;

            $friend = Customer::friend_request($token, ($action === 'accept'));

            self::$_data['accept_url'] = '';
            self::$_data['decline_url'] = '';

        } else {
            self::$_data['friend_action'] = 'decide';

            self::$_data['accept_url'] = bk_get_route_url('register', 'invite/'.self::get_query(1).'/accept');
            self::$_data['decline_url'] = bk_get_route_url('register', 'invite/'.self::get_query(1).'/decline');
        }



        return view('register.friend_confirm', self::$_data);
    }

    /**
     * @return \Illuminate\View\View
     */
    public static function render_verify() {
        $token = self::get_query(1);

        if(!empty($token)) {

            Bloomkit::set_config(['customer_token' =>  $token]);

            $req = Customer::verify_email();

            if($req->is_success()) {
                Customer::save_token_cookie();
            }
        }

        return view('register.verify');
    }

    /**
     *
     */
    public static function route_api() {
        $post = self::get_post();

        switch($post['method']) {
            case 'save':
                self::save();
            break;
        }

        self::output_json();
    }

    public static function save() {
        $post = self::get_post();

        $email = false;

        //look($post['customer']);

        if(!empty($post['customer'])) {
            $customer = $post['customer'];
            $errors = [];

            //look($customer);

            $valid = self::validate_step_input($customer);

            //look($valid);

            if($valid->success === true) {
                if(!Customer::is_auth()) {
                    $email = true;
                }

                $req = Customer::save($customer);

                if($req->is_success()) {
                    $save = $req->get_data();

                    if($save->success === true) {
                        Customer::save_token_cookie();

                        if(Customer::get_data()->customer_verified_email === 'N') {
                            //Customer::email_send_welcome();
                        }

                        self::json_success();
                        self::json('customer', Customer::get_data());
                    }
                }
            } else {
                self::json('errors', $valid->errors);
            }

        }
    }

    public static function validate_step_input($post) {

        $key = $post['step'];

        $out = ['success' => false];
        $errors = [];

        $steps['account'] = [
            'customer_name_first' => [
                'name' => 'First Name',
                'required' => true,
                'error' => 'A valid first name is required!'
            ],
            'customer_name_last' => [
                'name' => 'Last Name',
                'required' => true,
                'error' => 'A valid last name is required!'
            ],
            'customer_email' => [
                'name' => 'Email',
                'required' => true,
                'error' => 'A valid email is required!'
            ],
            'customer_pass' => [
                'name' => 'Password',
                'required' => true,
                'match' => 'customer_pass_confirm',
                'error' => 'You must provide a password to secure your account.'
            ]
        ];

        if(!empty($post['invite_token'])) {
            unset($steps['account']['customer_email']);
        }

        if(!empty($key) && !empty($steps[$key])) {
            $step = $steps[$key];

            foreach($step as $field => $rules) {
                if(empty($post[$field]) && $rules['required'] === true) {
                    $errors[$field] = $rules['error'];
                }
            }
        }

        if(!empty($errors)) {
            $out['errors'] = $errors;
        } else {
            $out['success'] = true;
        }

        return (object)$out;
    }

    public static function set_steps() {
        $steps = self::$_config['steps'];
        $step_completed = true;
        $completed = 0;

        if(!empty($steps)) {
            foreach($steps as $key => $step) {
                $current = (self::$_config['step'] == $key);

                if($current === true) {
                    $step_completed = false;
                }

                if($step_completed === true) {
                    $completed+=1;
                }

                $steps[$key]['complete'] = $step_completed;
                $steps[$key]['active'] = $current;
            }

            self::$_data['steps'] = $steps;
            self::$_data['steps_percent'] = ($completed / (count($steps)-1) * 100);
        }

    }

    public static function render_html() {
        return self::route();
    }
}