<?php
namespace App\Http\Controllers;

use App\Classes\Customer;
use App\Classes\Wordpress;
use App\Classes\Checkout;
use App\Classes\Bloomkit;
use App\Classes\Debug;

class ShopCheckoutController extends BaseFrontController {

    public static $_config = [];
    public static $_data = [];


    public static function init() {
        self::set_query();

        Checkout::init();

        //look(Customer::get_token_cookie());

        self::$_config['steps'] = [];

        self::$_data['steps_html'] = [];

        self::$_data['order'] = Checkout::get_order()->get_data();
        //self::$_data['cart'] = Customer::get_cart()->get_data(true);

        self::$_data['shipping_methods'] = Checkout::list_shipping_methods();
        self::$_data['billing_methods'] = Checkout::list_billing_methods();

        //look(self::$_data);

        self::$_data['totals'] = Checkout::get_totals();
    }


    public static function register() {

        $checkout_base_slug = 'checkout';

        Wordpress::add_xhr_profile('checkout', function() {
            ShopCheckoutController::route_api();
        });

        // Register Checkout Rewrite/Shortcode
        Wordpress::add_rewrite_rule('^('.$checkout_base_slug.')/?(.*)?', 'index.php?pagename=$matches[1]&query=$matches[2]', [
            'query' => '(.*)'
        ]);
    }

    public static function route() {

        self::init();

        $order = Checkout::get_order()->get_data();

        if(!empty($order)) {
            $order_config = $order->order_config;
        }


        $method = self::get_query(0);

        if($method === 'verify') {
            self::verify_customer_token();
        }

        self::$_config['steps']['account'] = [
            'title' => 'Account',
            'url' => '',
        ];

        self::$_config['steps']['shipping'] = [
            'title' => 'Shipping',
            'url' => '',
        ];

        if(empty($order_config->no_billing)) {
            self::$_config['steps']['billing'] = [
                'title' => 'Payment',
                'url' => '',
            ];
        }

        self::$_config['steps']['review'] = [
            'title' => 'Review',
            'url' => '',
        ];

        self::$_config['steps']['complete'] = [
            'title' => 'Complete',
            'url' => '',
        ];

        self::$_config['step'] = $method ?: 'account';

        if(Customer::get_cart()->has_items() === false) {
            wp_redirect(bk_get_route_url('cart'));
        }

        self::set_steps();

        $out = self::render_checkout();

        return $out;
    }

    public static function verify_customer_token() {
        $cid = (int)self::get_query(1);
        $token = base64_decode(self::get_query(2));

        if(!empty($token) && !empty($cid)) {

            $req = Bloomkit::post('customer/get', [
                'id' => $cid,
                'token' => $token
            ]);

            //look($req);

            if($req->is_success()) {
                $complete = $req->get_data();

                //Debug::look($contest);

                if($complete->success === true) {
                    if(!empty($complete->customer)) {
                        // We found the customer and are ready to complete signup...
                        Customer::load($complete->customer);

                        // Save the customer auth cookie.
                        Customer::save_token_cookie();

                        wp_redirect(bk_get_route_url('checkout'));
                    }
                }
            }
        }
    }

    public static function route_api() {
        Checkout::init();

        $post = self::get_post();

        switch($post['method']) {
            case 'login':
                self::api_login();
            break;

            case 'save':
                self::api_save();
            break;

            case 'complete':
                self::api_complete();
            break;
        }

        self::output_json();
    }

    public static function api_login() {
        $post = self::get_post();

        if(!empty($post['customer'])) {
            $customer = $post['customer'];

            $login = Customer::login($customer['customer_login_email'], $customer['customer_login_pass']);
        }

        //look($customer);


    }

    public static function api_save() {
        $post = self::get_post();

        //look($post);

//        if(!empty($post['customer'])) {
//            $customer = $post['customer'];
//
//            if(!empty($customer['customer_login_email']) && !empty($customer['customer_login_pass'])) {
//                $login = Customer::login($customer['customer_login_email'], $customer['customer_login_pass']);
//
//                //look('Save');
//
//                //look('Login');
//                //look($login);
//                //look($customer);
//
//                if(Customer::is_auth() === true) {
//                    unset($post['customer']);
//
//                }
//            }
//        }


        $save = Checkout::save($post);

        //look($save);

        if(!empty($save)) {
            if($save->success === true) {
                self::json_success();

                self::json('totals', Checkout::get_totals());

                self::json('order', Checkout::get_order()->get_data());
                self::json('customer', Checkout::get_customer());
            } else {
                self::json_success(false);

                if(!empty($save->errors)) {
                    self::json('errors', $save->errors);
                }
            }
        }
    }

    public static function api_complete() {
        $post = self::get_post();

        $save = Checkout::complete($post);

        self::json('totals', Checkout::get_totals());

        if(!empty($save)) {
            if ($save->success === true) {
                self::json_success();

                self::json('order', Checkout::get_order()->get_data());
                self::json('customer', Checkout::get_customer());
            } else {
                self::json_success(false);

                if (!empty($save->errors)) {
                    self::json('errors', $save->errors);
                }
            }
        }
    }

    public static function set_steps() {
        $steps = self::$_config['steps'];
        $step_completed = true;
        $completed = 0;

        if(!empty($steps)) {
            foreach($steps as $key => $step) {
                $current = (self::$_config['step'] == $key);

                if($current === true) {
                    $step_completed = false;
                }

                if($step_completed === true) {
                    $completed+=1;
                }

                $steps[$key]['complete'] = $step_completed;
                $steps[$key]['active'] = $current;
            }

            self::$_data['step'] = self::$_config['step'];
            self::$_data['steps'] = $steps;
            self::$_data['steps_percent'] = ($completed / (count($steps)-1) * 100);
        }

    }

    public static function render_checkout() {
        return view('shop/checkout/shell', self::$_data);
    }

    public static function render_html() {
        return self::route();
    }
}