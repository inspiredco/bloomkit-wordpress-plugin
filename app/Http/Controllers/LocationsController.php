<?php
namespace App\Http\Controllers;

use App\Classes\Website;
use App\Classes\Debug;
use App\Classes\Wordpress;
use App\Classes\Stores;

class LocationsController extends BaseFrontController {

    function __construct() {

    }

    public static function shortcode_map($attrs = []) {

        $data = [];
        $config = [
            'limit' => 8,
        ];


        if(!empty($attrs) && is_array($attrs)) {
            $config = array_merge($config, $attrs);
        }

        $data['config'] = $config;
        $data['config_json'] = json_encode($config);

        return view('locations.shortcodes.map', []);
    }

    public static function route_api() {
        $post = self::get_post();

        switch($post['method']) {
            case 'search':
                self::api_locations_search();
            break;
        }

        self::output_json();
    }


    public static function api_locations_search() {
        $post = self::get_post();

        if(!empty($post)) {
            $config = [];

            if(!empty($post['limit'])) {
                $config['limit'] = (int)$post['limit'];
            }

            if(!empty($post['per_page'])) {
                $config['per_page'] = (int)$post['per_page'];
            }

            if(!empty($post['page'])) {
                $config['page'] = (int)$post['page'];
            }

            if(!empty($post['category'])) {
                $config['category'] = (int)$post['category'];
            }

            if(!empty($post['search'])) {
                $config['search'] = $post['search'];
            }

            if(!empty($post['brand'])) {
                $config['brand'] = (int)$post['brand'];
            }

            if(!empty($post['ids'])) {
                $config['ids'] = $post['ids'];
            }

            $search = Stores::post_find($config);

            if (! empty($search)) {
                self::json('dispensaries', $search);
                self::json_success();
            }
        }
    }
}