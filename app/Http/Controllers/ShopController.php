<?php
namespace App\Http\Controllers;

use App\Classes\Website;
use App\Classes\WebsiteSEO;
use App\Classes\Debug;
use App\Classes\Wordpress;

use App\Classes\Customer;
use App\Classes\Products;
use App\Classes\Shop;
use App\Classes\ProductCategories;

class ShopController extends BaseFrontController {

    private static $_tally = 0;

    function __construct() {

    }

    public static function register() {
        Website::$_requests['store'] = true;

        $shop_base_slug = 'buy';

        Wordpress::add_rewrite_rule('^.*/('.$shop_base_slug.')/(.*)/category/([0-9]*)/?(.*)', 'index.php?pagename=$matches[1]&cat_id=$matches[3]&query=$matches[4]', [
            'cat_id' => '(.*)',
            'query' => '(.*)'
        ]);

        Wordpress::add_rewrite_rule('^.*/('.$shop_base_slug.')/(.*)/product/([0-9]*)/?', 'index.php?pagename=$matches[1]&product_id=$matches[3]', [
            'product_id' => '(.*)'
        ]);

        Wordpress::add_rewrite_rule('^.*/('.$shop_base_slug.')/search/?(.*)', 'index.php?pagename=$matches[1]&query=$matches[2]', [
            'query' => '(.*)'
        ]);

        Wordpress::add_shortcode('shop', function($attrs = []) {
            echo ShopController::shortcode($attrs);
        });

        Wordpress::add_xhr_profile('shop', function() {
            ShopController::route_api();
        });

        Wordpress::add_shortcode('shop_embed_products', function($attrs = []) {
            echo ShopController::shortcode_embed_products($attrs);
        });
    }

    public static function route() {
        global $wp_query;

        $cat_id = (int)get_query_var('cat_id');
        $p_id = (int)get_query_var('product_id');



        //Debug::look($wp_query->query_vars);
        // Debug::look();
//
//        Debug::look($cat_id);
//        Debug::look($p_id);

        if(self::is_search()) {
            $out = self::search();
        } else {
            if(!empty($p_id)) {
                $out = self::product_single($p_id);
            }

            if(!empty($cat_id)) {
                $out = self::products_index($cat_id);
            }

            if(empty($out)) {
                $out = self::home();
            }
        }

        return $out;
    }


    public static function shortcode($attrs = []) {

        $data = [];
        $config = [
            'limit' => 8,
        ];


        if(!empty($attrs) && is_array($attrs)) {
            $config = array_merge($config, $attrs);
        }

        $data['config'] = $config;
        $data['config_json'] = json_encode($config);

        return view('shop.shortcodes.product_grid_embed', []);
    }

    public static function shortcode_embed_products($attrs = []) {
        $data = [];
        $config = [
            'limit' => 8,
        ];


        if(!empty($attrs) && is_array($attrs)) {
            if (! empty($attrs['block_title'])) {
                $title = $attrs['block_title'];
                unset($attrs['block_title']);
            }
            if (! empty($attrs['block_desc'])) {
                $description = $attrs['block_desc'];
                unset($attrs['block_desc']);
            }
            if (! empty($attrs['block_link'])) {
                $block_link = $attrs['block_link'];
                unset($attrs['block_link']);
            }
            $config = array_merge($config, $attrs);
        }

        $data['config'] = $config;
        $data['config_json'] = json_encode($config);

        $data['index'] = self::$_tally;
        if (! empty($title)) {
            $data['title'] = $title;
        }
        if (! empty($description)) {
            $data['description'] = $description;
        }
        if (! empty($block_link)) {
            $data['block_link'] = $block_link;
        }

        self::$_tally++;

        //Debug::look($data);

        return view('shop.shortcodes.product_grid_embed', $data);
    }

    public static function shortcode_customer_hud($attrs = []) {
        $data = [];

        return view('shop.shortcodes.customer_hud', $data);
    }

    public static function is_search() {
        $post = self::get_post();

        if(!empty($post) && !empty($post['bloom_product_search'])) {
            return true;
        }

        return false;
    }


    public static function route_api() {
        $post = self::get_post();

        Customer::load();
        //look($post);

        switch($post['method']) {
            case 'product_reviews':
                self::api_product_reviews();
            break;

            case 'products_search':
                self::api_products_search();
            break;

            case 'review_submit':
                self::product_review_submit();
            break;


        }

        self::output_json();
    }

    public static function home() {
        $data = [
            'categories' => ProductCategories::list_all()
        ];

        return view('shop.home', $data);
    }

    public static function api_product_reviews() {
        $post = self::get_post();

        if(!empty($post)) {

            $pid = (int)$post['product_id'];

            if(!empty($pid)) {


                $list = Products::review_list($pid);

                //look($list);

                if($list->success === true) {
                    $html = [];

                    if(!empty($list->reviews)) {
                        foreach($list->reviews as $review) {
                            //Debug::look($review);

                            $html[] = view('shop.product.reviews.single', [
                                'review' => $review
                            ]);
                        }
                    }

                    self::json('reviews', join(PHP_EOL, $html));
                    self::json_success();
                }
            }

        }
    }

    public static function api_products_search() {
        $post = self::get_post();

        if(!empty($post)) {
            $config = [];

            if(!empty($post['query'])) {
                $config = $post['query'];
            } else {
                if(!empty($post['limit'])) {
                    $config['limit'] = (int)$post['limit'];
                }

                if(!empty($post['per_page'])) {
                    $config['per_page'] = (int)$post['per_page'];
                }

                if(!empty($post['page'])) {
                    $config['page'] = (int)$post['page'];
                }

                if(!empty($post['search'])) {
                    $config['search'] = $post['search'];
                }

                if(!empty($post['lang'])) {
                    $config['lang'] = $post['lang'];
                }

//            $post['filters'] = [
//                'delivery' => ['oral'],
//                'potency' => ['17.16', '65.18', '80.00']
//            ];

                if(!empty($post['filters'])) {
                    $config['filters'] = $post['filters'];
                }

                // todo: remove once front-end updated
                if(!empty($post['category'])) {
                    $post['category_id'] = $post['category'];
                }

                if(!empty($post['category_id'])) {
                    $config['category_id'] = $post['category_id'];
                }

                // todo: remove once front-end updated
                if(!empty($post['brand'])) {
                    $post['brand_id'] = $post['brand'];
                }

                if(!empty($post['brand_id'])) {
                    $config['brand_id'] = $post['brand_id'];
                }


                // todo: remove once front-end updated
                if(!empty($post['ids'])) {
                    $post['id'] = $post['ids'];
                }

                if(!empty($post['id'])) {
                    $config['id'] = $post['id'];
                }

                $config['sort'] = 'price-high';

                if(!empty($post['sort'])) {
                    $config['sort'] = $post['sort'];
                }
            }








            //look($config);

            $browse = Shop::browse([
                'include' => ['products'],
                'query' => $config
            ]);

            //look($browse);



            if($browse->success === true) {
                $products = [];

                $browse_products = $browse->products->results;

                //look(array_keys($browse->products));

                if(!empty($browse_products)) {
                    foreach($browse_products as $index => $product) {

                        $view_key = $product->get_type_name();
                        $view_path = 'shop.products.type.';

                        // If view doesn't exist for type, fall back to default
                        if(view()->exists($view_path.$view_key) === false) {
                            $view_key = 'default';
                        }

                        $page_number = 0;
                        if (! empty($post['page'])) {
                            $page_number = $post['page'];
                        }

                        // If the view exists (make sure default is there), add to response.
                        if(view()->exists($view_path.$view_key)) {
                            $products[] = view($view_path.$view_key, [
                                'product' => $product,
                                'index' => $index,
                                'page' => $page_number
                            ]);
                        }

                        // todo: Handle error where default view doesn't exist.
                    }
                }

                self::json('info', $browse->products->info);
                self::json('products', join(PHP_EOL, $products));
                self::json_success();
            }
        }



        //self::json('review', Customer::get_cart()->get_data());
    }

    public static function product_review_submit() {
        $post = self::get_post();

        if(!empty($post)) {
            if(!empty($post['review'])) {
                $review = $post['review'];

                $submit = Products::review_submit($post['product_id'], $review);

                //look($submit);

                if($submit->success === true) {
                    self::json('review', $submit->review);
                    self::json('rating', $submit->rating);
                    self::json_success();
                }
            }

        }



        //self::json('review', Customer::get_cart()->get_data());
    }

    public static function product_single($id) {
        Shop::set_product_by_id($id);

        $product = Shop::get_product();

        //look($product);
        //dd(Website::get_lang());

        if(!empty($product)) {
//            Wordpress::set_page_title($product->get_title());

            $meta_title = "Buy ".$product->get_title()." | ".$product->get_brand_title()." | Product Review";
            $meta_desc = "Looking for a ".$product->get_brand_title()." ".$product->get_title()." online? ".get_bloginfo('name')." has a great selection of ".$product->get_top_category_title()." to choose from.";

            WebsiteSEO::set_meta([
                'title' => $meta_title,
//                'title' => $product->get_title(),
                'metadesc' => $meta_desc,
//                'metadesc' => $product->get_desc_short(),
                'canonical' => $product->get_url(),
                'opengraph_image' => $product->get_cover_image_url(),
                'opengraph_type' => 'product'
            ]);

            $data['product'] = $product;

            return view('shop.product.single', $data);
        } else {
            die('No Product!');
        }


    }

    public static function products_index($cat_id) {
        $data = [
            'filters' => (object)[],
            'sort' => (object)[]
        ];

        $browse = Shop::browse([
            'include' => ['filters', 'sort', 'category', 'categories', 'brands'],
            'query' => [
                'category_id' => $cat_id
            ]
        ]);

        $query = get_query_var('query');

        if(!empty($query)) {
            $data['search_query'] = Shop::browse_parse_query($query);
        }

        Shop::set_category_by_id($cat_id);

        //look($data['search_query']);

        if(!empty($browse->filters)) {
            $data['filters'] = $browse->filters;
        }

        if(!empty($browse->brands)) {
            $data['brands'] = $browse->brands;
        }

        if(!empty($browse->sort)) {
            $data['sort'] = $browse->sort;
        }

        $category = Shop::get_category();

        if(!empty($category)) {
//            Wordpress::set_page_title($category->category->cat_title);

            // set long form...
            $meta_title = $category->category->cat_title." Online USA - User Reviews";

            // if long form too long, use a shorter version...
            if (strlen($meta_title) > 50) {
                $meta_title = $category->category->cat_title." Online USA";
            }

            // push to BK SEO -> Yoast...
            WebsiteSEO::set_meta([
                'title' => $meta_title,
                'metadesc' => $category->category->cat_desc,
                'canonical' => $category->category->url,
                'opengraph_type' => 'category'
            ]);

        } else {
            die('No Category Found!');
        }

        $cat_nav_rows = [];
        $cat_nav_path = [];
        $cat_nav_active_ids = [];

        $add_nav_row = function($id) use (&$add_nav_row, &$cat_nav_rows, &$cat_nav_active_ids, &$cat_nav_path) {
            $parent = ProductCategories::get_by_id($id);

            if(!empty($parent)) {
                $cat_nav_path[] = $parent->category;
                $cat_nav_active_ids[] = $id;

                if(!empty($parent->children)) {
                    $cat_nav_rows[] = $parent->children;
                }

                $parent_id = (int)$parent->category->cat_parent_id;

                if($parent_id > 0) {
                    $add_nav_row($parent_id);
                }
            }
        };

        if(!empty($category)) {
            $cat_nav_active_ids[] = $category->category->cat_id;

            if(!empty($category->children)) {
                $cat_nav_rows[] = $category->children;
            }

            if(!empty($category->category->cat_parent_id)) {
                $add_nav_row($category->category->cat_parent_id);
            }

            $data['category'] = $category;
            $data['parent'] = $category;

            $data['cat_nav'] = [
                'path' => array_reverse($cat_nav_path),
                'active' => $cat_nav_active_ids,
                'items' => array_reverse($cat_nav_rows)
            ];

            //look($data);
        }


        //Debug::look($data);

        return view('shop.products.category', $data);
    }

    public static function search() {
        $post = self::get_post();

        $data = [
            'search_text' => $post['bloom_product_search']
        ];

//        $search = Products::search([
//            'search' => $post['search_text']
//        ]);
//
//        //look($search);
//
//        if(!empty($search->result->products)) {
//            $data['products'] = $search->result->products;
//        }

        return view('shop.products.search', $data);
    }

    public static function render_html() {
        return self::route();
    }
}
