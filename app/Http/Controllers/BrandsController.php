<?php
namespace App\Http\Controllers;

use App\Classes\Website;
use App\Classes\Debug;
use App\Classes\WebsiteSEO;
use App\Classes\Wordpress;

use App\Classes\Customer;
use App\Classes\Products;
use App\Classes\Shop;
use App\Classes\ProductCategories;
use App\Classes\ProductBrands;


class BrandsController extends BaseFrontController {

    function __construct() {

    }

    public static function route() {
        self::set_query();

        $out = false;
        $slug = self::get_query(0);

        //look($slug);

        if(!empty($slug)) {
            $out = self::single($slug);
        }

        return $out;
    }

    public static function route_api() {
        $post = self::get_post();

        switch($post['method']) {
            case 'review_submit':
                //self::product_review_submit();
            break;
        }

        self::output_json();
    }

    public static function single($slug) {
        $data = [];

        $search = ProductBrands::post_find([
            'name' => $slug,
            'limit' => 1
        ]);

//        Debug::look($slug);
//        Debug::look($search);

        if($search->is_success()) {
            $data['brand'] = $search->get_data();

//            Debug::look($data['brand']);
            $brand = $data['brand']->get_data();
            if (! empty($brand) && ! empty($brand->post)) {
                $meta_title = get_post_meta($brand->post->id, '_yoast_wpseo_title', true);
                $meta_desc = get_post_meta($brand->post->id, '_yoast_wpseo_metadesc', true);
                $meta_arr = [
                    'title' => $meta_title,
                    'metadesc' => $meta_desc,
                    'canonical' => $brand->post->url,
                    'opengraph_url' => $brand->post->url,
                    'opengraph_type' => 'brand'
                ];

                WebsiteSEO::set_meta($meta_arr);
            }

            return view('brands.single', $data);
        }

        return 'No Brand Found!';

    }

    public static function render_html() {
        return self::route();
    }

    public static function shortcode_index($attrs = []) {
        $data = [];

        // Any default attributes for this shortcode?
        $default_attrs = [];

        // Merge any shortcode attributes with the defaults (ex: [bloom_shortcode limit="4" template="full" color="red"])
        if(is_array($attrs)) {
            $attrs = array_merge($default_attrs, $attrs);
        }


        $search = ProductBrands::post_find();

        if($search->is_success()) {
            $data['brands'] = $search->get_data();
        }

        return view('brands.shortcodes.index', $data);
    }
}
