<?php
namespace App\Http\Controllers;

use App\Classes\Email;
use App\Classes\Wordpress;
use App\Classes\Debug;
use App\Classes\Orders;
use App\Classes\Customer;


class AccountController extends BaseFrontController {

    public static function register() {


        Wordpress::add_xhr_profile('customer', function() {
            CustomerController::route_api();
        });

        // MOVING BLOOM_ACCOUNT API
        Wordpress::add_xhr_profile('account', function() {
            AccountController::route_api();
        });

        Wordpress::add_rewrite_rule('^.*/(account)/(.*)', 'index.php?pagename=$matches[1]&query=$matches[2]', [
            'query' => '(.*)'
        ]);
    }

    public static function route() {
        self::set_query();

        $method = self::get_query(0);
        $out = '';

//        Debug::look(Customer::get_data());
//        Debug::look(Customer::is_auth());

        //Debug::look(Customer::is_auth());

        if(Customer::is_auth() === false) {
            wp_redirect(site_url('login'));
        } else {
            switch($method) {
                case 'edit':
                    $out = self::render_edit();
                break;

                case 'orders':
                    $out = self::route_orders();
                break;

                case 'logout':
                    $out = self::route_logout();
                break;

                default:
                    $out = self::render_home();
                break;
            }
        }

        return $out;
    }

    public static function route_api() {
        $post = self::get_post();

        //Customer::load();

//        look($post);
//        look('route api');
       // look(Customer::is_auth());

        if(Customer::is_auth()) {
            switch ($post['method']) {
                case 'save':
                    self::api_save();
                break;

                case 'order_get':
                    self::api_orders_get();
                break;

                case 'orders_search':
                    self::api_orders_search();
                break;

                case 'reset_pass':
                    self::api_reset_pass();
                break;

                case 'follow':
                    self::api_follow();
                break;

                case 'invite':
                    self::api_invite();
                break;
            }
        }

        self::output_json();
    }

    public static function route_orders() {
        $action = self::get_query(1);


        $out = '';

        switch($action) {
            case 'view':
                $out = self::render_order_view();
            break;

            default:
                $out = self::render_order_index();
            break;
        }

        return $out;
    }

    public static function api_save() {
        $post = self::get_post();

        //look('API Save1');

        //look($post);

        if(!empty($post)) {
            //look('API Save');

            $customer = $post['customer'];
            $errors = [];

            //look($customer);
            $req = Customer::save($customer);

            //look($save);

            if($req->is_success()) {
                $save = $req->get_data();

                if($save->success === true) {
                    Customer::save_token_cookie();

                    self::json_success();
                    self::json('customer', Customer::get_data());
                }
            }

        }
    }

    public static function api_follow() {
        $post = self::get_post();

        $follow_id = (int)$post['id'];

        if(!empty($follow_id)) {
            $follow = Customer::follow('brand', $follow_id, $post['follow']);
        }

        self::json_success();
    }

    public static function api_invite() {
        $post = self::get_post();

        if(!empty($post)) {
            $invite = Customer::invite($post['invites']);
        }

        self::json_success();
    }

    public static function api_reset_pass() {
        $post = input()->all();

        //look($post);

        if(!empty($post)) {
            $action = Customer::change_pass($post['invites']);
        }

        wp_mail('postle@inspired.co', 'Reset Your Password', 'test!!');

        self::json_success();
    }

    public static function api_orders_get() {
        $post = self::get_post();
    }

    public static function api_orders_search() {
        $post = self::get_post();
        $config = [];

        if(!empty($post['status'])) {
            $config['status'] = $post['status'];
        }

        $search = Orders::search($config);

        if(!empty($search)) {
            self::json_success();
            self::json('orders', $search['orders']);
        }
    }

    public static function render_home() {
        $data['customer'] = Customer::get_data();

        return view('account/overview', $data);
    }

    public static function render_edit() {
        $data['customer'] = Customer::get_data();
        //look(Customer::get_data());

        return view('account/edit', $data);
    }

    public static function render_order_view() {
        $data = [ 'order' => []];

        $id = (int)self::get_query(2);

        if(!empty($id)) {
            $get = Orders::get_by_id($id);

            //look($get);

            if(!empty($get)) {
                $data['order'] = $get;
            }
        }

        return view('account/orders_single', $data);
    }

    public static function render_order_index() {
        return view('account/orders');
    }

    public static function route_logout() {
        Customer::logout();
        wp_redirect(site_url('login'));
    }

    public static function render_html() {
        $data['body_html'] = self::route();
        $data['customer'] = Customer::get_data();

        return view('account/shell', $data);
    }
}
