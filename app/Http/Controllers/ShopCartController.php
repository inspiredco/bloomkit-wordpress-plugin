<?php

namespace App\Http\Controllers;

use App\Classes\Customer;
use App\Classes\Wordpress;
use App\Classes\Checkout;
use App\Classes\Products;
use App\Classes\Debug;


class ShopCartController extends BaseFrontController {

    function __construct() {

    }

    public static function home() {
        Checkout::init();

        $data = [];

        $data['no_cart'] = false;

        if(Customer::get_cart()->has_items()) {
            $data['cart'] = Customer::get_cart()->get_data(true);
            $data['cart']->totals = Checkout::get_totals();
        } else {
            $data['no_cart'] = true;
        }

        //look($data);

        return view('shop.cart.cart_table', $data);
    }

    public static function register() {
        Wordpress::add_xhr_profile('customer_cart', function() {
            ShopCartController::route_api();
        });
    }

    public static function route() {

        if(empty($out)) {
            $out = self::home();
        }

        return $out;
    }

    public static function route_api() {
        $post = self::get_post();
        $action = false;

        Customer::load();

        if(!empty($post) && is_array($post)) {

            if(!empty($post['item'])) {
                $item = $post['item'];
            }

            if(!empty($post['method'])) {
                $method = $post['method'];
            }
        }


        if(!empty($method) && !empty($item)) {

            //die('here');

            switch($method) {
                case 'item_add':
                    $action = Customer::get_cart()->add_item($item);
                break;

                case 'item_update':
                    $action = Customer::get_cart()->update_item($item);
                break;

                case 'item_remove':
                    $action = Customer::get_cart()->remove_item($item);
                break;
            }

        }


        //look($action);
        //look($item);

        self::json_success(false);

        if(!empty($action)) {
            if($action->is_success()) {

                if(!empty($action->get_data('order'))) {
                    self::json('order', $action->get_data('order'));
                }

                self::json_success();
            }

            if($action->has_errors()) {
                self::json('errors', $action->get_errors());
            }
        }

        $cart = Customer::get_cart()->get_data(true);

        //look($cart);

//        if(!empty($cart)) {
//            if(Checkout::get_order() !== false) {
//                $cart->totals = Checkout::get_totals();
//            }
//        }


        self::json('cart', $cart);

        self::output_json();
    }

    public static function render_html() {
        return self::route();
    }
}