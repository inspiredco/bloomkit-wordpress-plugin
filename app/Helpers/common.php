<?php

use App\Classes\Customer;
use App\Classes\Stores;
use App\Classes\Routes;
use App\Classes\Debug;

require 'seo.php';

class Out_Return {
    public $_model_out_data = [
        'success' => false,
        'data' => [],
    ];

    public function __construct($data = null, $success = false) {
        $this->success($success);
        $this->data($data);
    }

    public function error($error, $group = false) {
        if(!empty($error)) {
            if(empty($this->_model_out_data['errors'])) {
                $this->_model_out_data['errors'] = [];
            }

            if(is_array($error)) {
                $this->_model_out_data['errors'] = array_merge($this->_model_out_data['errors'], $error);
            } else {
                $this->_model_out_data['errors'][] = $error;
            }
        }
    }

    public function get_errors() {
        return (!empty($this->_model_out_data['errors'])) ? $this->_model_out_data['errors'] : [];
    }

    public function has_errors() {
        return (!empty($this->_model_out_data['errors']));
    }

    public function success($state = true) {
        $this->_model_out_data['success'] = $state;
    }

    public function is_success() {
        return ($this->_model_out_data['success']);
    }

    public function data($key, $value = null) {
        if(!empty($key)) {
            if(is_array($key)) {
                $this->_model_out_data['data'] = array_merge($this->_model_out_data['data'], $key);
            } elseif(is_object($key)) {
                $this->_model_out_data['data'] = $key;
            } else {
                if($value !== null) {
                    $this->_model_out_data['data'][$key] = $value;
                } else {
                    $this->_model_out_data['data'][] = $key;
                }
            }
        }
    }

    public function get_data($key = null) {
        if(!empty($key)) {
            $data = null;

            if(is_array($this->_model_out_data['data']) && isset($this->_model_out_data['data'][$key])) {
                return $this->_model_out_data['data'][$key];
            }

            if(is_object($this->_model_out_data['data']) && isset($this->_model_out_data['data']->{$key})) {
                return $this->_model_out_data['data']->{$key};
            }

            return $data;
        } else {
            return $this->_model_out_data['data'];
        }
    }

    public function get_return() {
        return $this->_model_out_data ?: [];
    }
}

function bk_get_route_slug($key) {
    $id = Routes::get_key_page_id($key);
    $slug = false;

    if(!empty($id)) {
        $slug = get_post_field( 'post_name', $id);
    }

    return $slug;
}

function bk_get_route_url($key, $path = '') {
    $id = Routes::get_key_page_id($key);
    $url = false;

    if(!empty($id)) {
        $url = get_permalink($id);
    }

    if(!empty($url)) {
        if(!empty($path)) {
            $url .= trim($path, '/');
        }
    } else {
        $url = site_url($path);
    }

    return $url.'/';
}


function bk_get_plugin_dir() {
    return BK_PLUGIN_DIR;
}

function bk_get_theme_dir() {
    return  bk_get_plugin_dir().'/resources/themes/base';
}

function bk_get_theme_url() {
    return plugins_url().'/bloomkit/resources/themes/base';
}

function bk_get_theme_static_url($path = '') {
    return bk_get_theme_url().'/static/'.trim($path, '/');
}

function bk_get_included_js() {
    $ver = '3.2.0';

    $scripts = [
        'jquery' => bk_get_theme_static_url('js/libs/jquery.min.js'),
        'lodash' => bk_get_theme_static_url('js/libs/lodash.min.js'),
        'axios' => bk_get_theme_static_url('js/libs/axios.min.js'),
        'qs' => 'https://cdnjs.cloudflare.com/ajax/libs/qs/6.7.0/qs.min.js',
        'bloomkit-wordpress' => bk_get_theme_static_url('js/libs/bloomkit-wordpress.js'),
    ];

    wp_deregister_script( 'jquery' );

    foreach($scripts as $ref => $script) {

        $foot = false;

        if(is_array($script)) {
            $sd = $script;

            $script = $sd['url'];

            if(!empty($sd['defer'])) {
                $foot = true;
            }
        }

        wp_enqueue_script($ref, $script, [], false, $foot);
    }
}

function bk_get_included_css() {
    $ver = '3.0.2';

    $styles = [
        'app' =>  bk_get_theme_static_url('css/app.css?v='.$ver),
    ];

    foreach($styles as $ref => $style) {
        wp_enqueue_style($ref, $style);
    }
}

function bk_json_load_var($vars = [], $var_name = 'APP_LOAD') {
    ?>

    <script>
        <?=$var_name; ?> = <?=json_encode($vars); ?>;
    </script>

    <?php
}

function bk_allow_buy() {
    if(defined('BK_ALLOW_BUY')) {
        return BK_ALLOW_BUY;
    }

    return false;
}

function bk_allow_register() {
    if(defined('BK_ALLOW_JOIN')) {
        return BK_ALLOW_JOIN;
    }

    return false;
}

function bk_customer_data() {
    return Customer::get_data();
}

function bk_stores_list() {
    return Stores::list_all();
}

function bk_customer_cart() {
    $cart = Customer::get_cart();

//    look('BK CART');
//    look($cart->get_data(true));

    if(!empty($cart)) {
        return $cart->get_data(true);
    }

    return false;
}

function bk_make_slug($title) {
    $title = strtolower($title);
    
    return trim(preg_replace('/[^0-9a-z]+/i', '-', $title), '-');
}

function bk_customer_is_auth() {
    return Customer::is_auth();
}

function bk_shop_url($path = '') {
    return site_url('buy/'.trim($path, '/')).'/';
}

// Legacy
function bk_shop_base_url($path = '') {
    return bk_shop_url($path);
}

function primo_product_cat_path_html($product) {
    $path = [];
    $url_path = [];
    $out = [];

    //look($product->category_primary);

//    if(!empty($product->brand)) {
//        $path[] = [$product->brand->brand_title, site_url('weed-brands/'.$product->brand->brand_slug)];
//    }

    $path[] = ['Shop', site_url()];

    if(!empty($product->category_primary->path)) {
        foreach($product->category_primary->path as $cat) {
            $url_path[] = $cat->cat_slug;

            $path[] = [$cat->cat_title, bk_get_route_url('shop', join('/', $url_path).'/category/'.$cat->cat_id)];
        }
    }

    $path[] = [$product->product_title];

    foreach($path as $link) {
        $elem = 'a';
        $href = '';

        if(count($link) === 1) {
            $elem = 'span';
        } else {
            $href = ' href="'.$link[1].'"';
        }

        $out[] = '<'.$elem.' class="path_item"'.$href.'>'.$link[0].'</'.$elem.'>';
    }

    return join(PHP_EOL, $out);
}




function look($var) {
    Debug::look($var);
}