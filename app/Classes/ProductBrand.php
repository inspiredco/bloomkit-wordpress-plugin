<?php
namespace App\Classes;

class ProductBrand {
    public $_data;

    public function __construct($data) {
        $this->load($data);
    }

    public function load($data) {
        $this->_data = (object)$data;

        $this->_data->customer_is_following = false;

        if(!empty($this->get_id())) {
            $this->_data->customer_is_following = Customer::is_following('brand', $this->get_id());
        }

    }

    public function get_data() {
        return $this->_data;
    }

    public function get_id() {
        if(!empty($this->_data->brand_id)) {
            return $this->_data->brand_id;
        }

        return false;
    }

    public function get_post_id() {
        if(!empty($this->_data->post->id)) {
            return $this->_data->post->id;
        }

        return false;
    }

    public function get_post_title() {
        if(!empty($this->_data->post->title)) {
            return $this->_data->post->title;
        }

        return false;
    }

    public function get_logo_image_url($size = 'large') {
        return $this->get_post_field_image_url('brand_media_logo', $size);
    }

    public function get_post_desc_short() {
        return $this->get_post_field('brand_desc_short');
    }

    public function get_post_desc_overview() {
        return $this->get_post_field('brand_desc_overview');
    }

    public function get_url() {
        return bk_get_route_url('brands', $this->_data->post->slug);

        //return site_url('weed-brands/'.$this->_data->post->slug).'/';
        //return site_url('weed-brands/'.$this->_data->post->slug);
    }

    public function get_post_field($name, $default = '') {
        if(!empty($this->_data->post->fields[$name])) {
            return $this->_data->post->fields[$name];
        }

        return $default;
    }

    public function get_rating_count() {
        if(!empty($this->_data->rating->count)) {
            return $this->_data->rating->count;
        }

        return 0;
    }

    public function get_rating_average($replace_zero = false) {
        if(!empty($this->_data->rating->average)) {
            $rating = round($this->_data->rating->average, 1);

            //return $this->_data->brand_rating_average;
            if($replace_zero !== false && empty($rating)) {
                return $replace_zero;
            }

            return $rating;
        }

        return 0;
    }

    public function get_reviews() {
        if(!empty($this->_data->reviews)) {
            return $this->_data->reviews;
        }

        return [];
    }

    public function get_post_field_image_url($name, $size = 'large') {
        $field = $this->get_post_field($name);

        if(!empty($field)) {
            if(!empty($field['sizes'][$size])) {
                return $field['sizes'][$size];
            }
        }

        return '';
    }

    public function get_post_full_banner_image() {
        if(!empty($this->_data->post->fields['brand_media_banner']['url'])) {
            return $this->_data->post->fields['brand_media_banner']['url'];
        }

        return '';
    }
}