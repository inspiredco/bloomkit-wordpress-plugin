<?php
namespace App\Classes;

class Sitemap {
    protected static $_pages = [];
    protected static $_indexes = [];
    protected static $_lastmod = false;

    public function __construct() {

    }

    public static function get_lastmod_time() {
        return (int)self::$_lastmod;
    }

    public static function get_nextmod_time() {
        $last = self::get_lastmod_time();

        return ($last + 36000);
    }

    public static function cache_outdated() {
        //Debug::look('COMPARE: '. self::get_nextmod_time() .' = '. time());

        return (self::get_nextmod_time() < time());
    }

    public static function xml_init() {
        add_filter('wpseo_sitemap_index', [__CLASS__, 'xml_add_indexes'], 99);
        add_action('wpseo_do_sitemap_product-categories', [__CLASS__, 'xml_index_output_cats'], 99);
        add_action('wpseo_do_sitemap_products', [__CLASS__, 'xml_index_output_products'], 99);
    }

    public static function xml_add_indexes() {
        //Using $wpseo_sitemaps global object getting the posts last update date.
        global $wpseo_sitemaps;
        $indexes = [];

        if(!empty(self::$_pages) && is_array(self::$_pages)) {
            $indexes = array_keys(self::$_pages);
        }

        //Debug::look(self::$_pages);

        //Debug::look($indexes);

        $xml = [];
        $last_date = $wpseo_sitemaps->get_last_modified('post');

        foreach($indexes as $index) {
            //Inject custom item to the sitemap index

            $xml[] = '<sitemap>
					<loc>'.get_bloginfo('url').'/'.$index.'-sitemap.xml</loc>
					<lastmod>'.$last_date.'</lastmod>
				</sitemap>';
        }

        return join(PHP_EOL, $xml);
    }

    public static function xml_index_output_cats() {
        self::xml_index_output('product-categories');
    }

    public static function xml_index_output_products() {
        self::xml_index_output('products');
    }

    public static function xml_index_output($index) {
        global $wpseo_sitemaps;

//        Debug::look('Index output...');
//        Debug::look($wpseo_sitemaps);

        if(class_exists('WPSEO_Sitemaps')) {

            $pages = self::$_pages[$index];

            $output = '';

            if(!empty($pages)) {
                foreach($pages as $page){
                    $output .= '<url>
                        <loc>'.$page['url'].'</loc>
                        <lastmod>'.$page['mod'].'</lastmod>
                        <changefreq>'.$page['chf'].'</changefreq>
                        <priority>'.$page['pri'].'</priority>
                    </url>';
                }
            }

            if(empty($output)) {
                $wpseo_sitemaps->bad_sitemap = true;
                return;
            }

            //Build the full sitemap
            $sitemap = '<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ';
            $sitemap .= 'xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" ';
            $sitemap .= 'xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n";
            $sitemap .= $output . '</urlset>';

            $wpseo_sitemaps->set_sitemap($sitemap);
        }
    }

    public static function clear_pages() {
        self::$_pages = [];
    }

    public static function add_page($index, $url, $pri = 0.5, $freq = 'monthly', $last_mod = false) {
        if(!$last_mod) {
            $last_mod = date('c');
        }

        if(empty(self::$_pages[$index])) {
            self::$_pages[$index] = [];
        }

        self::$_pages[$index][] = [
            'url' => $url,
            'pri' => $pri,
            'chf' => $freq,
            'mod' => $last_mod
        ];
    }

    public static function cache_file_path() {
        $cache_dir = Plugin::get_upload_dir('cache', true);
        $cache_file = 'sitemap_cache.json';

        if(!empty($cache_dir)) {
            return $cache_dir.$cache_file;
        }

        return false;
    }

    public static function cache_save() {
        $json = [
            'time' => time(),
            'data' => []
        ];

        $cache_path = self::cache_file_path();

        if(!empty($cache_path)) {
            if(is_array(self::$_pages)) {
                //Debug::look(self::$_pages);

                $json['data'] = self::$_pages;
            }

            return file_put_contents($cache_path, json_encode($json));
        }

        return false;
    }

    public static function cache_load() {
        $cache_path = self::cache_file_path();

        if(!empty($cache_path) && file_exists($cache_path)) {
            $json = json_decode(file_get_contents($cache_path), true);

            if(!empty($json)) {
                self::$_pages = $json['data'];
                self::$_lastmod = $json['time'];
            }
        }

        return false;
    }
}