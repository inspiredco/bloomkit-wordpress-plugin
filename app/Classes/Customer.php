<?php
namespace App\Classes;


class Customer {
    public static $_data;
    public static $_cart;

    public function __construct() {
        //self::load();
    }

    public static function load($customer = false) {

        self::$_cart = new CustomerCart();

        if(!empty($customer)) {
            self::get_cart()->set_customer($customer);

            self::$_data = $customer;

            if(!empty($customer->token)) {
                Bloomkit::set_config(['customer_token' => $customer->token]);

                self::save_token_cookie();
            }

            if(!empty($customer->cart)) {
                self::get_cart()->clear_local();

                self::get_cart()->load($customer->cart);
            }

            //Debug::look($customer->follows);

            if(!empty($customer->follows)) {
                foreach($customer->follows->brands as $x => $brand) {
                    $find = ProductBrands::post_find([
                        'brand_id' => $brand->follow_assoc_id,
                        'limit' => 1
                    ]);

                    if($find->is_success()) {
                        self::$_data->follows->brands[$x]->brand = $find->get_data();
                    }
                }
            }
        } else {
            if(empty($token)) {
                self::get_cart()->cookie_load();
            }
        }
    }

    public static function has_data() {
        //Debug::look(self::$_data);

        return (!empty(self::$_data) && !empty(self::$_data->customer_id));
    }

    public static function get_cart() {
        return self::$_cart;
    }

    public static function get_id() {
        if(self::has_data()) {
            return self::$_data->customer_id;
        }

        return false;
    }

    public static function get_email() {
        if(self::has_data()) {
            return self::$_data->customer_email;
        }

        return false;
    }

    public static function get_name() {
        if(self::has_data()) {
            return self::$_data->customer_name_full;
        }

        return false;
    }


    public static function get_data() {
        return self::$_data;
    }

    public static function has_loyalty_points() {
        return true;
    }

    public static function get_loyalty_points() {
        if(self::has_loyalty_points()) {
            return self::get_data()->loyalty;
        }

        return false;
    }

    public static function register($post = []) {
        $out = new \Out_Return();

        $post['local_cart_items'] = Customer::get_cart()->get_items();

        $req = Bloomkit::post('customer/save', $post);

        //look($req);

        if($req->is_success()) {
            $register = $req->get_data();

            //look($register);

            if($register->success === true) {
                $out->success();

                if(!empty($register->customer)) {
                    self::load($register->customer);

                    if(self::get_data()->customer_verified_email === 'N') {
                       // self::email_send_welcome();
                       // self::email_send_admin_new_customer();
                    }
                }
            } else {
                if(!empty($register->errors)) {
                    $out->error($register->errors);
                }
            }
        }

        return $out;
    }

    public static function login($user, $pass) {

        $to_api = [
            'login_user' => $user,
            'login_pass' => $pass
        ];

        $to_api['local_cart_items'] = Customer::get_cart()->get_items();

        $req = Bloomkit::post('customer/login', $to_api);

        if($req->is_success()) {
            $login = $req->get_data();

            if(!empty($login)) {
                if(!empty($login->customer)) {
                    self::load($login->customer);

                    return true;
                }
            }
        }

        return false;
    }

    public static function logout() {
        //self::load(false);
        self::delete_token_cookie();

        return true;
    }

    public static function invite($invites) {
        //look($invites);
        $req = Bloomkit::post('customer/invite', [
            'invites' => $invites
        ]);

        if($req->is_success()) {
            $action = $req->get_data();

            if($action->success === true) {
                if(!empty($action->created)) {
                    foreach($action->created as $invite) {
                        $email_data = [
                            'invite_url' => bk_get_route_url('register', 'invite/'.base64_encode($invite->token))
                        ];

                        //echo view('email/customer_invite', $email_data);

                        Email::send($invite->invite_data, 'You have been invited to join Primo', view('email/customer_invite', $email_data));
                    }
                }
            }

            return $action;
        }
    }

    public static function friend_request($token, $confirm = true) {
        //look($invites);
        $out = new \Out_Return();

        $req = Bloomkit::post('customer/friend_request', [
            'token' => $token,
            'confirm' => $confirm
        ]);
        
        if($req->is_success()) {
            $action = $req->get_data();

            if($action->success === true) {

                if(!empty($action->customer)) {
                    self::load($action->customer);
                }

                $out->success();
            }
        }

        return $out;
    }

    public static function email_send_welcome($email_data = []) {
        $email_data = [
            'verify_url' => bk_get_route_url('register', 'verify/'.self::get_data()->token)
        ];

        return self::email_send('welcome', [
            'data' => $email_data
        ]);
    }

    public static function email_send_admin_new_customer($email_data = []) {
        $email_data = [

        ];

        return self::email_send('admin_new_customer', [
            'data' => $email_data
        ]);
    }

    public static function email_send($key, $config = []) {
        //look($invites);
        $out = new \Out_Return();

        $emails = [
            'welcome' => [
                'subject' => 'Confirm Your Email',
                'view' => 'email.customer_welcome'
            ],
            'admin_new_customer' => [
                'subject' => 'New Customer',
                'view' => 'email.admin_new_customer'
            ]
        ];

        $email_data = [
            'customer' => self::get_data()
        ];

        //look($config);

        if(is_array($config['data'])) {
            $email_data = array_merge($email_data, $config['data']);
        }

//        Debug::look('Customer Send Email');
//        Debug::look([
//            'key' => $key,
//            'email' => self::get_data()->customer_email
//        ]);

        if(!empty($key) && !empty($emails[$key])) {

            $email = $emails[$key];

//            Debug::look($email);
//            Debug::look($email_data);

            if ($key === 'admin_new_customer') {
                if (BK_API_KEY === "www.thebwellmarket.com") {
                    Email::send('info@thebwellmarket.com', $email['subject'], view($email['view'], $email_data));
                } else if (BK_API_KEY === "primo.inspiredcloud.com") {
//                    Email::send('support@itsprimo.com', $email['subject'], view($email['view'], $email_data));
                }
            } else {
                Email::send(self::get_data()->customer_email, $email['subject'], view($email['view'], $email_data));
            }
        }

        return $out;
    }

    public static function verify_email() {
        //look($invites);
        $out = new \Out_Return();

        $req = Bloomkit::post('customer/verify', []);

        if($req->is_success()) {
            $action = $req->get_data();

            if($action->success === true) {

                if(!empty($action->customer)) {
                    self::load($action->customer);
                }

                $out->success();
            }
        }

        return $out;
    }

    public static function is_following($type, $id) {
        if(!empty(self::$_data) && !empty(self::$_data->follows) && !empty($id)) {
            $items = self::$_data->follows->brands;

            if(!empty($items) && is_array($items)) {
                foreach($items as $item) {
                    if($item->follow_assoc_id == $id) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public static function follow($type, $id, $state = true) {

        $req = Bloomkit::post('customer/follow', [
            'id' => $id,
            'type' => $type,
            'follow' => $state
        ]);

        if($req->is_success()) {
            return $req->get_data();
        }

        return false;
    }

    public static function change_pass($pass) {
        $req = Bloomkit::post('customer/save', [
            'customer_pass' => $pass,
            'customer_pass_confirm' => $pass
        ]);

        //look($req);

        if($req->is_success()) {
            $action = $req->get_data();

            if ($action->success === true) {
                self::load($action->customer);
            }

            return $action;
        }
    }

    public static function reset_pass($id, $hash, $pass) {
//        look([
//            'id' => $id,
//            'hash' => $hash,
//            'pass' => $pass
//        ]);

        $reset = Bloomkit::post('customer/reset-password', [
            'id' => $id,
            'hash' => $hash,
            'pass' => $pass
        ]);

        return $reset;
    }

    public static function forgot_pass($email) {
        $req = Bloomkit::post('customer/forgot-password', [
            'email' => $email
        ]);

        if($req->is_success()) {
            $reset = $req->get_data();

            if(!empty($reset)) {
                //Debug::look($reset);
                //Debug::look($user.' '.$pass);

                if($reset->success === true) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function is_auth() {
        if(!empty(self::get_token_cookie())) {
            //return true;
        }

        return (self::get_id() !== false);
    }

    public static function get_token_cookie() {
        if(!empty($_COOKIE[config('plugin.cookie_name_customer')])) {
            $token = $_COOKIE[config('plugin.cookie_name_customer')];

            if(!empty($token)) {
                return $token;
            }
        }

        return false;
    }

    public static function save_token_cookie() {
        $customer = self::get_data();

        if(!empty($customer->token)) {
            setcookie(config('plugin.cookie_name_customer'), $customer->token, strtotime('+1 month'), '/', config('plugin.cookie_domain'));

            return true;
        }

        return false;
    }

    public static function delete_token_cookie() {
        $domain = parse_url(home_url());

        setcookie(config('plugin.cookie_name_customer'), '', (0 - strtotime('+1 month')), '/', config('plugin.cookie_domain'));

        return true;
    }

    public static function save($post = []) {
        if(!empty($post)) {
            $save = Bloomkit::post('customer/save', $post);

            //look($save);

            if($save->is_success()) {
                $customer = $save->get_data('customer');

                if(!empty($customer)) {
                    self::load($customer);

                    if(!empty($post['newsletter_subscribe'])) {
                        self::subscribe();
                    }
                }
            }

            return $save;
        }

        return false;
    }

    public static function address_save($post = []) {
        if(!empty($post)) {
            $save = Bloomkit::post('customer/address/save', $post);

            //look($post);

            if($save->is_success()) {
                $customer = $save->get_data('customer');

                if(!empty($customer)) {
                    self::load($customer);
                }
            }

            return $save;
        }

        return false;
    }

    public static function subscribe($config = []) {
        $email = self::get_data()->customer_email;

        if(!empty($email)) {
            //$subscribe = Newsletter::subscribe($email);

            //look($subscribe);

            //return $subscribe;
        }

        return false;
    }
}