<?php
namespace App\Classes;

class Product {
    public $_data;

    public function __construct($data) {

        $this->load($data);

        return $this;
    }

    public function load($data) {
        //look($data);

        if(!empty($data->brand->brand_id)) {

            // Find the WP CPT for Brand.
            $get = ProductBrands::post_find([
                'brand_id' => $data->brand->brand_id,
                'limit' => 1
            ]);


            // If a WP Post was found, add it to the product->brand object
            if($get->is_success()) {
                $brand = $get->get_data();

                if(!empty($brand->get_data()->post)) {
                    $data->brand->post = $brand->get_data()->post;
                }

            }
        }

        //look($data);

        $this->_data = $data;
    }

    public function get_self() {
        return $this;
    }

    public function get_data() {
        return $this->_data;
    }

    public function get_id() {
        return $this->_data->product_id;
    }

    public function get_type_name() {
        if(!empty($this->_data->type->type_table_name)) {
            return $this->_data->type->type_table_name;
        }

        return 'default';
    }

    public function get_type() {
        if(!empty($this->_data->type)) {
            return $this->_data->type;
        }

        return false;
    }

    public function get_title() {
        return $this->_data->product_title;
    }

    public function has_price() {
        return (!empty($this->_data->price));
    }

    public function has_brand() {
        return (!empty($this->_data->brand->brand_id));
    }

    public function is_variation() {
        return (!empty($this->_data->product_is_variation));
    }

    public function get_brand() {
        if(!empty($this->_data->brand)) {
            return $this->_data->brand;
        }

        return false;
    }

    public function get_brand_logo() {
        if(!empty($this->_data->brand)) {
            return $this->_data->brand->cover_image->image->sizes->{'large'}->url;
        } else {
            return '';
        }
    }
    
    public function get_brand_title() {
        if(!empty($this->_data->brand)) {
            return $this->_data->brand->brand_title;
        }

        return '';
    }

    public function get_slug() {
        $slug = strtolower($this->_data->product_title);

        return trim(preg_replace('/[^0-9a-z]+/i', '-', $slug), '-');
    }

    public function get_url() {
        //look($this->get_data());
        $product = $this->get_data();
        $path = [];
        $brand_slug_url = '';

        if(!empty($product->category_primary_path)) {
            foreach($product->category_primary_path as $cat) {
                $path[] = bk_make_slug($cat->cat_title);
            }
        }

        if(!empty($this->get_brand_title())) {
            $brand_slug = bk_make_slug($this->get_brand_title());
            //$path[] = $brand_slug;
            $brand_slug_url = '/'.$brand_slug;
        }

        //return bk_shop_url(join('/', $path).'/'.$this->get_slug().$brand_slug_url.'/product/'.$this->get_id());
        return bk_shop_url(end($path).'/'.$this->get_slug().$brand_slug_url.'/product/'.$this->get_id());
    }

    public function get_primary_category() {
        if(!empty($this->_data->category_primary)) {
            return $this->_data->category_primary;
        }

        return false;
    }

    public function get_top_category_title() {
        $category = $this->get_primary_category();

        if (! empty($category) && !empty($category->path)) {
            $parent = $category->path[0];

            if (! empty($parent)) {
                return $parent->cat_title;
            } else {
                return $category->cat_title;
            }
        }

        return false;
    }

    public function get_inventory() {
        return (float)$this->_data->product_inventory;
    }

    public function has_inventory() {
        return (!empty($this->get_inventory()));
    }

    public function can_buy() {
        return ($this->has_inventory() && $this->has_price());
    }

    public function get_desc() {
        return $this->_data->product_desc;
    }

    public function get_desc_short() {
        return $this->_data->product_desc_short;
    }

    public function get_rating_count() {
        return $this->_data->product_rating_count;
    }

    public function get_rating_average() {
        return round($this->_data->product_rating_average, 1);
    }

    public function get_reviews() {
        if(!empty($this->_data->reviews)) {
            return $this->_data->reviews;
        }

        return [];
    }

    public function get_cover_image_url($size = 'thumb_large') {
        //if($this->_data->type->type_table_name !== 'accessories') {
        //    return $this->get_brand_logo();
        //} else {
            if(!empty($this->_data->product_cover_image)) {
                if(!empty($this->_data->product_cover_image->image->sizes->{$size})) {
                    return $this->_data->product_cover_image->image->sizes->{$size}->url;
                }
            }
        //}


        return '';
    }

    public function get_effects_group($group = false) {
        $effects = [];

        if(!empty($this->_data->fields->effect)) {
            $effects = $this->_data->fields->effect;

            if(!empty($group) && !empty($effects->{$group})) {
                $effects = $effects->{$group};
            }
        }

        return $effects;
    }

    public function get_fields() {
        $args = func_get_args();
        $fields = $this->_data->fields;

        $out = '';
        $value = $fields;

        if(!empty($args)) {
            foreach($args as $arg) {
                if(is_array($value)) {
                    if(isset($value[$arg])) {
                        $value = $value[$arg];
                        $out = $value;
                    }
                }

                if(is_object($value)) {
                    if(isset($value->{$arg})) {
                        $value = $value->{$arg};
                        $out = $value;
                    }
                }
            }

            return $out;
        }

        return $value;
    }

    public function get_price_base_value() {
        $cost = 0;

        if(!empty($this->_data->base_price)) {
            $cost = $this->_data->base_price->price_cost;
        }

        return number_format($cost, 2, '.', ',');
    }

    public function price_is_range() {
        return (!empty($this->_data->price_has_range));
    }

    public function get_price() {
        return $this->_data->price;
    }

    public function get_price_currency() {
        return 'USD';
    }

    public function get_price_range($key = false) {
        if(!empty($key)) {
            return $this->_data->price_range_limits->{$key};
        }

        return $this->_data->price_range_limits;
    }

    public function get_price_range_limits() {
        return $this->_data->price_range_limits;
    }

    public function get_top_effect() {
        if(!empty($this->get_fields()->top_effect)) {
            return $this->get_fields()->top_effect;
        }

        return false;
    }

    public function get_strain_species() {
        //dd($this->get_fields());

        if(!empty($this->get_fields()->species)) {
            return $this->get_fields()->species;
        }

        return false;
    }
}