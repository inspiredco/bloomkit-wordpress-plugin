<?php

namespace App\Classes;

class Checkout {
    
    public static $_config = [];
    public static $_order = false;
    public static $_data = [];
    public static $_totals = [];

    public static function get_order() {
        if(self::$_order === false) {
            self::$_order = new Order();
        }

        return self::$_order;
    }

    public static function init() {

        $to_api = [];

        if(Customer::is_auth() === false) {
            $to_api['local_items'] = Customer::get_cart()->get_items();
        }


        // Request some general info for the checkout - shipping/billing methods,
        $req = Bloomkit::post('checkout/init', $to_api);


        //look($to_api);

        if($req->is_success()) {
            $init =  $req->get_data();

            //look($init);

            if(!empty($init->checkout)) {
                self::load($init->checkout);
            }
        }


    }

    public static function load($data = []) {
        self::$_data = $data;

        //Debug::look($data);

        if(!empty($data->order)) {
            self::get_order()->load($data->order);

            if(!empty($data->order->totals)) {
                self::$_totals = $data->order->totals;
            }
        }

        if(!empty($data->totals)) {
            self::$_totals = $data->totals;
        }

        if(!empty($data->customer)) {
            Customer::load($data->customer);
        }

        if(!empty($data->config)) {
            self::$_config = $data->config;
        }


    }


    public static function get_totals() {

        $totals = (array)self::$_totals;

//        if(self::get_order() !== false && self::get_order()->is_loaded() === true) {
//            $totals = (array)self::$_totals;
//        } else {
//            $cart = Customer::get_cart()->get_data();
//
//            if(!empty($cart)) {
//                $subtotal = number_format($cart->totals->cost, 2);
//
//                $totals['subtotal'] = $subtotal;
//                $totals['total'] = $subtotal;
//
//                $totals['items'] = $cart->totals->items;
//            }
//        }

        //Debug::look($totals);

        return $totals;
    }


    public static function save($post) {
        $to_api = [];

        if(!empty($post['token'])) {
            $to_api['token'] = $post['token'];
        }

        if(!empty($post['order'])) {
            $to_api['order'] = $post['order'];
        }

        if(!empty($post['customer'])) {
            $to_api['customer'] = $post['customer'];
        }

        if(Customer::is_auth() === false) {
            $to_api['local_cart_items'] = Customer::get_cart()->get_items();
        }

        //Debug::look($to_api);

        $req = Bloomkit::post('checkout/save', $to_api);

        //Debug::look($req);

        if($req->is_success()) {
            $res = $req->get_data();

            if(!empty($res) && is_object($res)) {
                if(!empty($res->success)) {
                    if(!empty($res->customer)) {

                        // This stuff needs to be moved. Emails should be sent out through BK or atleast controller
                        $cust_welcome_email = true;

                        if(Customer::is_auth() === true) {
                            $cust_welcome_email = false;
                        }

                        Customer::load($res->customer);

                        Customer::get_cart()->clear_local();

                        if($cust_welcome_email === true) {
                            Customer::email_send_welcome();
                        }


                        if(!empty($res->order->cart)) {
                            Customer::get_cart()->load($res->order->cart);
                        }
                    }

                    self::load($res);

                    //self::send_email('customer-order-received', []);
                }
            }

            return $res;
        }

        return false;
    }

    public static function complete($post) {
        $out = new \Out_Return();

        $req = Bloomkit::post('checkout/submit', $post);

        if($req->is_success()) {
            $res = $req->get_data();

            if($res->success === true) {
                self::load($res);
//
//                self::send_email('customer-order-received', [
//                    'customer' => Customer::get_data(),
//                    'order' => self::get_order()
//                ]);
            }

            return $res;
        }

        return false;
    }

    public static function report_view() {

    }

    public static function get_steps() {
        $steps = [
            'shipping' => 'Select Shipping',
            'billing' => 'Payment Method',
            'review' => 'Review Order',
            'completed' => 'Complete'
        ];

        return $steps;
    }

    public static function has_items() {
        return (!empty(self::$_order->items));
    }
    
    public static function get_data() {
        return self::$_data;
    }



    public static function get_customer() {
        return Customer::get_data();
    }

    public static function send_email($key, $data = []) {
        $emails = [
            'customer-order-received' => [
                'subject' => 'We got your order!',
                'view' => 'email.customer_order_received'
            ]
        ];

        if(!empty($key) && !empty($emails[$key])) {
            $email = $emails[$key];

            //look($email);
            $subject = $email['subject'];
            $body = view($email['view'], $data);

            Email::send(Customer::get_data()->customer_email, $subject, $body);
            Email::send('postle@inspired.co', 'A new order has been received!', $body);
            if (BK_API_KEY === "www.thebwellmarket.com") {
                Email::send('info@thebwellmarket.com', 'A new order has been received!', $body);
            }
            //Email::send('support@itsprimo.com', 'A new order has been received!', $body);
        }
    }

    public static function send_customer_email($subject, $body) {
        $email = Customer::get_data()->customer_email;

        if(!empty($email)) {
            //Email::send($email, $subject, $body);
        }
    }

    public static function list_shipping_methods() {
        $methods = [];
        
        if(!empty(self::$_config->shipping_methods)) {
            $methods = self::$_config->shipping_methods;
        }
        
        return $methods;
    }
    
    public static function list_billing_methods() {
        $methods = [];
        
        if(!empty(self::$_config->billing_methods)) {
            $methods = self::$_config->billing_methods;
        }
        
        return $methods;
    }
}