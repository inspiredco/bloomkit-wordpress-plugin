<?php
namespace App\Classes;

use App\Classes\Debug;

class ProductCategories {

    public static $_data;

    public function __construct($data) {
        //$this->load($data);
    }

    public static function load($data) {
        //Debug::look('Loaded Cats');

        if(!empty($data)) {
            self::map($data);
        }
    }

    public static function map($cats, $parent_ids = []) {
        foreach($cats as $cat) {
            self::$_data[$cat->category->cat_id] = self::normalize($cat);

            if(!empty($cat->children)) {
                self::map($cat->children);
            }
        }
    }

    public static function get_by_id($id) {

        if(!empty(self::$_data[$id])) {
            $cat = self::$_data[$id];

            return $cat;
        }

        return false;
    }

    public static function list_all() {
        return self::$_data;
    }

    public static function list_top_level_categories() {
        return array_filter(self::list_all(), [__CLASS__, "filter_parent_cats"]);
    }

    private static function filter_parent_cats($cat) {
        if ($cat->category->cat_parent_id === "0")
            return TRUE;
        else
            return FALSE;
    }

    public static function make_slug($title) {
        $slug = strtolower($title);

        return trim(preg_replace('/[^0-9a-z]+/i', '-', $slug), '-');
    }

    public static function normalize($cat) {
        $cat->category->url = bk_shop_url(join('/', $cat->category->path_slugs).'/category/'.$cat->category->cat_id);

        //Debug::look($cat);

        return $cat;
    }

    public static function api_search($config = []) {
        $out = [];
        $search = Bloomkit::post('store/categories/list', $config);

        //Debug::look($search);

        if($search->is_success()) {
            $search = $search->get_data();

            if(!empty($search->result)) {
                foreach($search->result as $cat) {
                    $out['data'][] = $cat;
                }
            }

            return $out;

        }

        return false;
    }
}