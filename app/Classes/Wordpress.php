<?php
namespace App\Classes;

use mysql_xdevapi\Exception;

class Wordpress {
    public static $_config = [
        'page_title' => '',
        'page_html' => '',
        'routes' => [],
        'xhr' => [],
        'widgets' => [],
        'shortcodes' => [],
        'rewrite_rules' => [],
        'rewrite_vars' => [],
        'scripts' => [],
        'styles' => []
    ];


    public static function add_routes($routes) {
        self::$_config['routes'] = self::$_config['routes'] + $routes; //array_merge(self::$_config['routes'], $routes);
    }

    public static function get_routes() {
        return self::$_config['routes'];
    }

    public static function set_page_title($title = '') {
        self::$_config['page_title'] = $title;
    }

    public static function register_page_title() {
        $title = self::$_config['page_title'];

        add_filter('wp_title', function($text) use ($title) {
            $new_title = $text;

            if(!empty($title)) {
                $new_title = $title.' - '.$text;
            }

            return $new_title;
        }, 100, 2);
    }

    public static function register_page_meta() {
        WebsiteSEO::register_meta();
    }

    public static function get_page_content() {
        $html = self::$_config['page_html'];

        return $html;
    }

    public static function set_page_content($html = false) {
        //dd('Setting Content...', $html);
        self::$_config['page_html'] = $html;
    }

    public static function add_shortcode($action, $fn, $attr = false, $post_type = false) {
        self::$_config['shortcodes']['bloom_'.$action] = array(
            'action' => 'bloom_'.$action,
            'post_type' => $post_type,
            'function' => $fn,
            'attr' => $attr
        );
    }

    public static function register_shortcodes() {
        $shortcodes = self::$_config['shortcodes'];

        if(count($shortcodes) > 0) {
            foreach($shortcodes as $shortcode) {
                add_shortcode($shortcode['action'], [__CLASS__, 'route_shortcode']);
            }
        }
    }

    public static function route_shortcode($atts, $content, $tag) {
        $shortcode = self::$_config['shortcodes'][$tag];

        if(!empty($shortcode['attr'])) {
            $atts = shortcode_atts($shortcode['attr'], $atts);
        }

        return call_user_func($shortcode['function'], $atts);
    }

    public static function add_xhr_profile($profile, $fn) {
        self::$_config['xhr']['bloom_'.$profile] = $fn;
    }

    /* LOAD XHR PROFILES */
    public static function register_xhr_profiles() {
        $profiles = self::$_config['xhr'];

        if(count($profiles) > 0) {
            foreach($profiles as $profile => $xhr_fn) {
                //look($xhr_fn);

                add_action('wp_ajax_'.$profile, $xhr_fn);
                add_action('wp_ajax_nopriv_'.$profile, $xhr_fn);
            }
        }
    }

    public static function add_widget($class) {
        self::$_config['widgets'][] = $class;
    }

    public static function register_widgets() {
        $widgets = self::$_config['widgets'];

        //Debug::look($widgets);

        if(count($widgets) > 0) {
            foreach($widgets as $widget) {
                if(class_exists($widget)) {
                    //Debug::look($widget.' exists');

                    register_widget( $widget);
                } else {
                    Debug::look('No Widget');

                    throw new Exception('No widget class found!');
                }

            }
        }
    }

    public static function add_rewrite_rule($rule, $url, array $query_vars, $pos = 'top') {
        self::$_config['rewrite_rules'][$rule] = array(
            'rule' => $rule,
            'url' => $url,
            'pos' => $pos
        );

        if(!empty($query_vars)) {
            foreach($query_vars as $var => $rx) {
                self::$_config['rewrite_vars'][$var] = $rx;
            }
        }

    }

    public static function register_rewrite_rules() {
        $exist_rules = get_option('rewrite_rules');
        $flush = true;

        $rules = self::$_config['rewrite_rules'];
        $vars = self::$_config['rewrite_vars'];

//        Debug::look($rules);
//        Debug::look($vars);

        //dd(self::$_config);

        if(is_array($vars)) {
            foreach($vars as $var => $rule) {
                add_rewrite_tag('%'.$var.'%', $rule);
                //add_rewrite_endpoint($var, EP_PERMALINK | EP_PAGES );
            }
        }

        if(is_array($rules)) {
            foreach($rules as $rule) {

                // If a rule does not exist, make sure rules are flushed.
                if(!isset($exist_rules[$rule['rule']])) {
                    $flush = true;
                }

                //Debug::look($rule);
                //Debug::look($flush);

                add_rewrite_rule($rule['rule'], $rule['url'], $rule['pos']);
            }
        }

        if($flush === true) {
            flush_rewrite_rules();
        }
    }

    public static function add_js($ref, $src, $dep = [], $ver = false, $footer = false, $admin_only = false) {
        self::$_config['scripts'][$ref] = [
            'url' => $src,
            'ref' => $ref,
            'dep' => $dep,
            'ver' => $ver,
            'footer' => $footer,
            'admin' => $admin_only
        ];
    }

    public static function add_css($ref, $src, $dep = [], $ver = false, $media = 'all', $admin_only = false) {
        self::$_config['styles'][$ref] = [
            'url' => $src,
            'ref' => $ref,
            'dep' => $dep,
            'ver' => $ver,
            'media' => $media,
            'admin' => $admin_only
        ];
    }

    public static function register_js($admin = false) {
        $items = self::$_config['scripts'];

        foreach($items as $ref => $item) {
            wp_enqueue_script($ref, $item['url'], $item['deps'], $item['ver'], $item['footer']);
        }
    }

    public static function register_css($admin = false) {
        $items = self::$_config['styles'];

        foreach($items as $ref => $item) {
            wp_enqueue_style($ref, $item['url'], $item['deps'], $item['ver'], $item['media']);
        }
    }

}
