<?php
namespace App\Classes;

use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Client;

use App\Classes\Debug;

//TEST

class Bloomkit {
    public static $_config = [
        'return_flat' => true
    ];

    public static function post($path = '/', $post_data = [], $config = []) {
        if(!empty($post_data) && is_array($post_data)) {
            $config['form_params'] = $post_data;
        }

        //look($config);

        return self::make_request('POST', $path, $config);
    }

    public static function set_config($config = [], $clean = false) {
        if($clean === false) {
            self::$_config = array_merge(self::$_config, $config);
        } else {
            self::$_config = $config;
        }
    }

    public static function make_request($method, $path, $config = []) {
        $out = new \Out_Return();

        $client = self::make_request_client();

        try {
            $result = $client->request($method, $path, $config);
        } catch (RequestException $e) {
            Debug::look(\GuzzleHttp\Psr7\str($e->getResponse()));

            if ($e->hasResponse()) {
                Debug::look($e->getResponse());
            }
        }

        if(!empty($result)) {
            if($result->getStatusCode() === 200) {
                $res_body = (string) $result->getBody();
                //Debug::look($res_body);

                if(!empty($res_body)) {
                    $res_value = json_decode($res_body);

                   //Debug::look($res_value);

                    if(is_array($res_value) || is_object($res_value)) {
                        $out->success();
                        $out->data($res_value);
                    } else {
                        Debug::look($res_body);
                        $out->error('Failed to process response! Bad JSON.');
                    }
                }
            } else {
                $out->error('Unable to connect to API!');
            }
        }

        return $out;
    }

    public static function make_request_client() {
        $config = [
            'base_uri' => 'https://manage.bloomkit.co/api/',
            'headers' => [
                'X-VENDOR-DOMAIN' => BK_API_KEY
            ]
        ];

        if(defined('BK_DEV_MODE') && BK_DEV_MODE === true) {
            $config['base_uri'] = 'https://stage.bloomkit.co/api/';
        }

        if(defined('BK_DEV_LOCAL') && BK_DEV_LOCAL === true) {
            $config['base_uri'] = 'http://build.bloomkit.co/api/';
        }

        // Set Client Request Headers
        if(!empty(self::$_config['hostname'])) {
            $config['headers']['X-VENDOR-DOMAIN'] = self::$_config['hostname'];
        }

        if(!empty(self::$_config['customer_token'])) {
            $config['headers']['X-CUSTOMER-TOKEN'] = self::$_config['customer_token'];
        }

        if(!empty(self::$_config['language'])) {
            $config['headers']['X-LANGUAGE'] = self::$_config['language'];
        }

        //Debug::look($config);

        $client = new Client($config);

        return $client;
    }
}