<?php
namespace App\Classes;

class Orders {
    public static $_data;

    public static function get_data() {
        return self::$_data;
    }

    public static function get_by_id($id) {

        $get = Bloomkit::post('customer/orders/get', [
            'id' => $id
        ]);

        if($get->is_success()) {
            $get = $get->get_data();

            if(!empty($get)) {
                if($get->success === true) {
                    return new Order($get->order);
                }
            }
        }
        //Debug::look($product);



        return false;
    }

    public static function search($config = []) {
        $out = [
            'orders' => []
        ];

        //Debug::look($config);

        $search = Bloomkit::post('customer/orders/list', $config);

        if($search->is_success()) {
            $search = $search->get_data();

            if(!empty($search)) {

                if ($search->success === true) {
                    foreach ($search->orders as $order) {
                        $out['orders'][] = $order;
                    }
                }

                return $out;
            }
        }

        return false;
    }
}