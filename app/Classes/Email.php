<?php
namespace App\Classes;

class Email {
    public static function send($to, $subject, $body) {

        $html = (string)view('email.wrapper', [
            'body_html' => (string)$body
        ]);

        $headers = ['Content-Type: text/html; charset=UTF-8'];

//        add_filter('wp_mail_from', function($email) {
//            return 'Primo'; //config('email_from_address');
//        });
//
//        add_filter('wp_mail_from_name', function($name) {
//            return 'info@itsprimo.com'; //config('email_from_name');
//        });

        wp_mail($to, $subject, $html, $headers);
    }
}
