<?php
namespace App\Classes;

class WebsiteSEO {
    public static $_meta = [];

    public static function set_meta($meta) {
        self::$_meta = $meta;
    }

    public static function register_meta() {
        $meta = self::$_meta;

        //Debug::look($meta);

        foreach($meta as $key => $value) {
            add_filter('wpseo_'.$key, function($str) use ($value) {

                if(!empty($value)) {
                    return $value;
                }

                return $str;
            }, 10, 1);
        }
    }

}