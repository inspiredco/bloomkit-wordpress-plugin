<?php
namespace App\Classes;

use App\Classes\Bloomkit;
use App\Classes\Products;
use App\Classes\ProductCategories;

class Shop {
    public static $_data;
    public static $_config;

    public static function get_data() {
        return self::$_data;
    }

    public static function set_product_by_id($id) {
        self::$_data['product'] = Products::get_by_id($id);
    }

    public static function get_product() {
        return self::$_data['product'];
    }

    public static function set_category_by_id($id) {
        self::$_data['category'] = ProductCategories::get_by_id($id);
    }

    public static function get_category() {
        return self::$_data['category'];
    }

    public static function browse_parse_query($query) {

        if(is_string($query)) {
            $query = explode('/', $query);
        }

        $allow_keys = [
            'brand' => 'brand_id',
            'page' => 'page',
            'sort' => 'sort'
        ];

        $config = [];

        if(!empty($query) && count($query) > 0) {


            $label_sep = '-';
            $value_sep = '+';

            foreach($query as $part) {
                $key = false;
                $value = null;

                if(!empty($part)) {
                    $parts = explode('-', $part);

                    if(count($parts) > 1) {
                        $key = $parts[0];

                        // remove the key from the array before joining again for value
                        // value may include the label sep
                        unset($parts[0]);

                        // join the value so it can be exploded on value_sep
                        $value_str = join($label_sep, $parts);
                        $value_arr = [];

                        if(!empty($value_str)) {
                            $value_arr = explode($value_sep, $value_str);
                        }

                        if(!empty($value_arr)) {
                            $value = $value_arr;

                            // If only 1 value is found, flatten
                            if(count($value) === 1) {
                                $value = $value[0];
                            }
                        }
                    }
                }

                if(is_string($key) && $value !== null) {
                    // Some keys may need to be remapped - brand_id, etc
                    if(!empty($allow_keys[$key])) {
                        $key = $allow_keys[$key];

                        $config[$key] = $value;

                    } else {
                        $config['filters'][$key] = $value;
                    }
                }
            }
        }

        //look($config);

        return $config;
    }

    public static function browse($config = []) {
        $out = [];

        //look($config);

        $browse = Bloomkit::post('store/browse', $config);

        //look($browse);

        if($browse->is_success()) {
            $browse = $browse->get_data();

            if(!empty($browse->categories)) {
                ProductCategories::load($browse->categories);
            }

            if(!empty($browse->products)) {
                $browse_products = $browse->products;

                //look([$browse->filters, $browse->sort]);

                if(!empty($browse_products->results)) {
                    array_walk($browse_products->results, function(&$product, $key) {
                        //look($product);
                        if(!empty($product)) {
                            $product = new Product($product);
                        }
                    });
                }
            }
            return $browse;
        }

        return false;
    }
}