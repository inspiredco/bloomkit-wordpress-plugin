<?php
namespace App\Classes;

use App\Classes\Debug;

class Stores {

    public static $_data;

    public function __construct($data) {
        //$this->load($data);
    }

    public static function load($data) {
        //Debug::look($data);

        if(!empty($data)) {
            self::map($data);
        }
    }

    public static function map($items) {
        foreach($items as $item) {
            self::$_data[$item->store_id] = self::normalize($item);
        }
    }

    public static function get_by_id($id) {
        if(!empty(self::$_data[$id])) {
            $item = self::$_data[$id];

            return $item;
        }

        return false;
    }

    public static function list_all() {
        return self::$_data;
    }

    public static function normalize($data) {
        return $data;
    }

    public static function post_find($config = []) {
        $out = new \Out_Return();

        $search = [
            'posts_per_page' => -1,
            'numberposts' => '',
            'offset' => 0,
            'cat' => '',
            'orderby' => 'post_date',
            'order' => 'DESC',
            'include' => '',
            'exclude' => '',
            'meta_key' => '',
            'meta_value' => '',
            'post_type' => 'dispensaries',
            'post_status' => 'publish',
            'suppress_filters' => true
        ];

        if(!empty($config['name'])) {
            $search['name'] = $config['name'];
        }

        if(!empty($config['limit'])) {
            $search['numberposts'] = $config['limit'];
        }

        $posts = WordpressPosts::find($search);

        if(!empty($posts)) {
            $out->success();

            if(!empty($config['limit']) && $config['limit'] === 1) {
                $out->data($posts[0]);
            } else {
                $out->data($posts);
            }
        }

//        return $out;
        return $posts;
    }
}