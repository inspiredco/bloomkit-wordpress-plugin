<?php
namespace App\Classes;

use App\Classes\Bloomkit;
use App\Classes\Product;

class Products {
    public static $_data;

    public static function get_data() {
        return self::$_data;
    }

    public static function get_by_id($id) {

        $get = Bloomkit::post('store/products/get', [
            'id' => $id
        ]);

        if($get->is_success()) {
            $product = $get->get_data('product');

            if(!empty($product)) {
                return new Product($product);
            }
        }

        return false;
    }

    public static function search($config = []) {
        $out = [];

        look($config);

        $search = Bloomkit::post('store/products/search', $config);

        if($search->is_success()) {
            $search = $search->get_data();

            if(!empty($search->result) && !empty($search->result->products)) {
                array_walk($search->result->products, function(&$product, $key) {
                    //look($product);
                    if(!empty($product)) {
                        $product = new Product($product);
                    }
                });
            }

            return $search;
        }

        return false;
    }

    public static function review_list($id, $config = []) {
        //$out = [ 'success' => false ];
        $id = (int)$id;

        if(!empty($id)) {
            $res = Bloomkit::post('store/products/reviews/list', [
                'product_id' => $id
            ]);

            if($res->is_success()) {
                return $res->get_data();
            }
        }

        return false;
    }

    public static function review_submit($id, $post) {
        //$out = [ 'success' => false ];
        $id = (int)$id;

        if(!empty($id)) {
            $res = Bloomkit::post('store/products/reviews/submit', [
                'product_id' => $id,
                'data' => $post,
            ]);

            //Debug::look($res);

            if($res->is_success()) {
                return $res->get_data();
            }
        }

        return false;
    }
}