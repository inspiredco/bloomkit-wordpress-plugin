<?php
namespace App\Classes;

class Debug {
    public static function look($at, $trace = false, $die = false) {
        if($die === true) {
            die(var_dump($at));
        }

        $bt = debug_backtrace();
        $caller = array_shift($bt);
        $usage = memory_get_usage();

        $mem_formated = function($size) {
            $unit=array('b','kb','mb','gb','tb','pb');
            return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
        };

        echo '<div style="background: #fff;"><div style="padding: 5px; margin-bottom: -2px; border: 1px solid rgba(0, 0, 0, 0.15); background: #eee; color: #555; font-size: 10px; line-height: 12px; letter-spacing: 1px;">';
        echo '<strong>LINE: '.$caller['line'].'</strong><br />';
        echo '<strong>FILE: '.$caller['file'].'</strong><br />';
        echo '<strong>MEM ALLOC: '.$mem_formated($usage).'</strong><br />';
        echo '</div>';

        if($trace) {
            echo '<strong>BACKTRACE:</strong><br /><pre>';
            var_dump(debug_backtrace());
            echo '</pre><br />';
        }

        echo '<pre>';

        if(is_array($at) || is_object($at)) {
            print_r($at);
        } else {
            var_dump($at);
        }

        echo '</pre> <hr /></div>';
    }

}
