<?php
namespace App\Classes;

use App\Classes\Bloomkit;

class WordpressPosts {
    public static $_posts;

    public static function get($i = false) {
        if(is_numeric($i)) {
            return self::$_posts[$i];
        }

        return self::$_posts;
    }

    public static function find($args = false) {
        self::$_posts = [];

        $query = new \WP_Query($args);

        if($query->have_posts()) {
            while($query->have_posts()) {
                $query->the_post();

                $post_id = get_the_ID();
                $post_text = do_shortcode(get_the_content());

                $post = array(
                    'id' => $post_id,
                    'title' => get_the_title(),
                    'slug' =>  get_post_field( 'post_name'),
                    'text' => $post_text,
                    'date' => get_the_date(),
                    'unix_date' => strtotime(get_the_date()),
                    'featured_image' => wp_get_attachment_url( get_post_thumbnail_id($post_id) ),
                    'url' => get_permalink($post_id),
                    'fields' => get_fields()
                );

                self::$_posts[] = $post;
            }
        }

        wp_reset_postdata();

        return self::get();
    }

    public function extract_images($content) {
        $images = false;

        $rx = preg_match_all('<img.*?src="(?P<src>.*?)".*?/>', $content, $matches, PREG_OFFSET_CAPTURE);

        if($rx) {
            $images = array();

            foreach($matches['src'] as $src) {
                $images[] = $src[0];
            }
        }

        return $images;
    }
}