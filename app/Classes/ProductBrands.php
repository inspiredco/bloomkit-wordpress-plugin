<?php
namespace App\Classes;

use App\Classes\Bloomkit;
use App\Classes\WordpressPosts;
use App\Classes\ProductBrand;

class ProductBrands {
    public static $_data;

    public static function get_data() {
        return self::$_data;
    }

    public static function post_find($config = []) {
        $out = new \Out_Return();

        $search = [
            'numberposts' => '',
            'offset' => 0,
            'cat' => '',
            'orderby' => 'post_date',
            'order' => 'DESC',
            'include' => '',
            'exclude' => '',
            'meta_key' => '',
            'meta_value' => '',
            'post_type' => 'bk-product-brand',
            'post_status' => 'publish',
            'suppress_filters' => true
        ];

        if(!empty($config['name'])) {
            $search['name'] = $config['name'];
        }

        if(!empty($config['limit'])) {
            $search['numberposts'] = $config['limit'];
        }

        if(!empty($config['brand_id'])) {
            $search['meta_query'][] = [
                'key'     => 'brand_id',
                'value'   => $config['brand_id'],
                'compare' => 'AND',
            ];
        }

        $posts = WordpressPosts::find($search);

//       look($config);
//       look($search);
//       look($posts);

        if(!empty($posts)) {
            $out->success();

            $brands = self::normalize_outputs($posts, $config);

            if(!empty($config['limit']) && $config['limit'] === 1) {
                $out->data($brands[0]);
            } else {
                $out->data($brands);
            }
        }

        return $out;
    }

    public static function api_get_by_id($id) {

        $res = Bloomkit::post('store/products/brands/get', [
            'id' => $id
        ]);

        if($res->is_success()) {
            return $res->get_data();
        }
        //Debug::look($product);

        if(!empty($req)) {
            if($req->success === true) {
                return $req->brand;
            }
        }

        return false;
    }

    public static function api_search($config = []) {
        $out = [];
        $search = Bloomkit::post('store/products/brands/list', $config);

        //Debug::look($search);

        if($search->is_success()) {
           $search = $search->get_data();

            if(!empty($search->result) && !empty($search->result->brands)) {
                foreach($search->result->brands as $brand) {
                    $out['data'][] = $brand;
                }
            }

            return $out;
        }

        return false;
    }

    public static function normalize_outputs($posts, $config = []) {
        $out = [];

        $item_ids = [];
        $api_items = [];

        if(!empty($posts)) {
            foreach ($posts as $idx => $post) {
                if(!empty($post['fields']['brand_id'])) {
                    $item_ids[$post['fields']['brand_id']] = $idx;
                }
            }

            if(!empty($item_ids)) {
                $search = self::api_search([
                    'ids' => $item_ids
                ]);

                //look($search);

                foreach ($search['data'] as $item) {
                    $api_items[$item->brand_id] = $item;
                }
            }

            foreach ($posts as $post) {
                if(!empty($post['fields']['brand_id'])) {
                    $id = (int)$post['fields']['brand_id'];

                    if (!empty($api_items[$id])) {
                        $item = $api_items[$id];
                    } else {
                        $item = (object)[];
                    }

                    $item->post = (object)$post;
                    $out[] = self::normalize_output($item);
                }
            }
        }
        //look('OUT');

        //look($out);

        return $out;
    }

    public static function normalize_output($post) {
        return new ProductBrand($post);
    }
}