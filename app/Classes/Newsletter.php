<?php
namespace App\Classes;

use \DrewM\MailChimp\MailChimp;

class Newsletter {

    public static function subscribe($email) {
        $out = ['success' => false];

        $MailChimp = new MailChimp('609399823871470e599726f86d18219f-us18');

        $list_id = '4f3135a898';

        if(!empty($email)) {
            $post = [
                'email_address' => $email,
                'status' => 'subscribed'
            ];

            $merge = [];

            if(!empty($_SERVER["HTTP_CF_IPCOUNTRY"])) {
                $merge['COUNTRY'] = $_SERVER["HTTP_CF_IPCOUNTRY"];
            }

            if(!empty($merge)) {
                $post['merge_fields'] = $merge;
            }

            $result = $MailChimp->post('lists/'.$list_id.'/members', $post);

            if($result['status'] === 'subscribed') {
                $out['success'] = true;
            } else {
                $error_codes = [
                    'Invalid Resource' => 'Please submit a valid email address!',
                    'Member Exists' => 'You have already subscribed to our newsletter!'
                ];

                $error_msg = $error_codes[$result['title']] ?: 'Unknown error has occurred. Please try again.';

                $out['error'] = $error_msg;
            }
        }

        return $out;
    }
}
