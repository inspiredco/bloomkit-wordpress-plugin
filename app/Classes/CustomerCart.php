<?php
namespace App\Classes;

use Illuminate\Support\Facades\Crypt;

class CustomerCart {

    public $_data;
    public $_items = [];
    public $_products;
    public $_customer = false;
    public $_order = false;

    public function load($data = false) {
        $this->_data = (object)[];

        if(!empty($data)) {
            if(!empty($data->items)) {
                $this->load_items($data->items);

                unset($data->items);
            }

            $this->_data = $data;
        }

        //look($data);

        return $this;
    }

    public function load_items($items) {
        if(!empty($items)) {
            $cart_items = [];

            foreach($items as $i => $item) {
                //Debug::look($item);

                if(!empty($item->product)) {
                    $this->_products[$i] = $item->product;

                    unset($item->product);
                }

                $cart_items[$i] = $item;
            }

            $this->_items = $cart_items;
        }
    }

    public function reload() {
        $res = Bloomkit::post('', [
            'serve_requested' => [
                'cart' => $this->get_items()
            ]
        ]);

        //look($res);

        if($res->is_success()) {
            $res = $res->get_data();

            if(!empty($res->success)) {
                $this->load($res->cart);

                if(!empty($res->order)) {
                    $this->_order = $res->order;
                }
            }
        }

        //Debug::look($res);
        //Debug::look($this->get_items());

    }

    public function clear_local() {

        $this->cookie_save(true);

        //setcookie(config('plugin.cart_cookie_name'), '', (0-time()), '/', config('plugin.cart_cookie_domain'));
        //look('Clearing Local');
        //
    }

    public function cookie_load() {
        $cookie_name = config('plugin.cookie_name_cart');


        if(!empty($_COOKIE[$cookie_name])) {
            $cookie = $_COOKIE[$cookie_name];


            if(!empty($cookie)) {
                $data = json_decode(stripslashes($cookie));

                //Debug::look($data);

                if($data === null) {
                    $data = json_decode(base64_decode($cookie));
                }

                //Debug::look($data);
                $this->load_items($data);

                $this->reload();
                //Debug::look($this->get_item_products());
            }
        }
    }

    public function cookie_save($clear = false) {
        $time_add = ((60*60)*24)*90;

        if($clear === true) {
            $time = (time() - $time_add);
            $value = '';
        } else {
            $value = json_encode($this->_items);
            $time = time() + $time_add;

            $value = base64_encode($value);
        }

        //Debug::look($value);

        setcookie(config('plugin.cookie_name_cart'), $value, $time, '/', config('plugin.cookie_domain'));
    }



    public function set_customer($state = false) {
        $this->_customer = $state;
    }

    public function has_customer() {
        return Customer::is_auth();
    }

    public function has_items() {
        return (!empty($this->_items));
    }

    public function get_id() {
        return $this->_data->cart_id;
    }

    public function get_data($include_items = true) {
        $data = false;

        if(!empty($this->_data) && is_object($this->_data)) {
            $data = $this->_data;

            $data->totals = self::get_totals();

            if($include_items === true) {
                $data->items = [];

                $items = self::get_item_products();

                if(!empty($items)) {
                    $data->items = $items;
                }
            }
        }

        return $data;
    }

    public function get_totals() {
        $totals = (object)[
            'cost' => 0,
            'items' => 0
        ];


        if(!empty($this->_data->totals)) {
            $totals = $this->_data->totals;
        } else {
            if(!empty($this->_data->cart_total_cost)) {
                $totals->cost = $this->_data->cart_total_cost;
            }

            if(!empty($this->_data->cart_total_items)) {
                $totals->items = $this->_data->cart_total_items;
            }
        }

        return $totals;
        //return $this->_data->totals;
    }

    public function load_cart($cart = false) {

    }



    public function has_item($product_id, $variant_id = false, $bulk_id = false) {
        $items = $this->get_items();
        $product_id = (int)$product_id;

        if(!empty($items)) {
            foreach($items as $index => $item) {
                if((int)$item->item_product_id === $product_id) {

                    if((int)$bulk_id == (int)$item->item_product_price_id && (int)$variant_id == (int)$item->item_product_variant_id) {
                        return $index;
                    }
                }
            }
        }

        return false;
    }

    public function get_item($index) {
        if(is_numeric($index)) {
            if(!empty($this->_items[$index])) {
                return $this->_items[$index];
            }
        }

        return false;
    }

    public function get_items($config = []) {
        $items = [];

        //$config['include_product'] = true;

        if(!empty($this->_items) && is_array($this->_items)) {
            foreach($this->_items as $i => $item) {

                if(!empty($config['include_product'])) {
                    $product = (object)[];

                    if(!empty($this->_products[$i])) {
                        $product = $this->_products[$i];
                    }

                    $item->product = $product;
                }

                $items[] = $item;
            }
        }

        return $items;
    }

    public function get_item_products() {
        return $this->get_items([
            'include_product' => true
        ]);
    }

    public function add_item($item) {
        $out = new \Out_Return();

//        Debug::look($item);
//        Debug::look($this->_customer.'');

        // If customer is logged in, add item through API
        if($this->has_customer() === true) {
            $res = Bloomkit::post('cart/item/add', [
                'item' => $item
            ]);

            //

            if($res->is_success()) {
                $res = $res->get_data();

                if (!empty($res->success)) {
                    $this->load($res->cart);

                    if(!empty($res->order)) {
                        $this->_order = $res->order;
                    }

                    $out->success();
                }

                if (!empty($res->errors)) {
                    $out->error($res->errors);
                }

            }
        } else {
            // If no customer, update the local cart.
            $out = $this->add_item_local($item);
        }

        if(!empty($this->_order)) {
            $out->data('order', $this->_order);
        }

        return $out;
    }

    public function remove_item($item) {
        $out = new \Out_Return();


        if($this->has_customer()) {
            $res = Bloomkit::post('cart/item/remove', [
                'id' => $item['item_id']
            ]);

            if($res->is_success()) {
                $res = $res->get_data();

                if(!empty($res->order)) {
                    $this->_order = $res->order;
                }

                if(!empty($res->success)) {
                    $this->load($res->cart);

                    $out->success();
                }
            }
        } else {
            $out = $this->remove_item_local($item);
        }

        if(!empty($this->_order)) {
            $out->data('order', $this->_order);
        }

        return $out;
    }

    public function update_item($item) {
        $out = new \Out_Return();

        if($this->has_customer() === true) {
            $res = Bloomkit::post('cart/item/update', [
                'id' => $item['item_id'],
                'quantity' => $item['item_quantity']
            ]);

            //Debug::look($res);
            if($res->is_success()) {
                $res = $res->get_data();

                if(!empty($res->order)) {
                    $this->_order = $res->order;
                }

                if (!empty($res->success)) {
                    $this->load($res->cart);

                    $out->success();
                }
            }
        } else {
            $out = $this->update_item_local($item);
        }

        if(!empty($this->_order)) {
            $out->data('order', $this->_order);
        }

        return $out;
    }


    public function add_item_local($item) {
        $out = new \Out_Return();

        //Debug::look($item);

        if(!empty($item)) {
            $pid = (int)$item['item_product_id'];
            $quantity = (float)$item['item_quantity'];

            $price_id = (int)$item['item_product_price_id'];
            $variant_id = (int)$item['item_product_variant_id'];


            $exist_index = $this->has_item($pid, $variant_id, $price_id);

            if($exist_index === false) {
                $out->data('mode', 'insert');

                $this->_items[] = (object)[
                    'item_product_id' => $pid,
                    'item_quantity' => $quantity,
                    'item_product_price_id' => $price_id,
                    'item_product_variant_id' =>  $variant_id
                ];
            } else {
                $out->data('mode', 'update');
                $out->data('index', $exist_index);

                $existing = $this->_items[$exist_index];
                $quantity = (float)$existing->item_quantity + $quantity;

                $this->_items[$exist_index] = (object)[
                    'item_product_id' => $pid,
                    'item_quantity' => $quantity,
                    'item_product_price_id' => $price_id,
                    'item_product_variant_id' =>  $variant_id ,
                ];
            }

            //Debug::look($this->_items);

            $out->success();
            $this->reload();
            $this->cookie_save();

        } else {
            $out->error('No item was submitted!');
        }

        return $out;
    }


    public function update_item_local($item) {
        $out = new \Out_Return();

        if(!empty($item)) {
            $item = (array)$item;

            //Debug::look($item);

            $pid = (int)$item['item_product_id'];

            $quantity = (float)$item['item_quantity'];

            $price_id = (int)$item['item_product_price_id'];
            $variant_id = (int)$item['item_product_variant_id'];


            // Does this item exist already in the cart? Return index or false
            $exist_index = $this->has_item($pid, $variant_id, $price_id);

            if($exist_index !== false) {
                $this->_items[$exist_index]->item_quantity = $quantity;

                $out->data('index', $exist_index);

                $this->reload();
                $this->cookie_save();


                $out->success();
            }
        } else {
            $out->error('No item was submitted!');
        }

        return $out;
    }

    public function remove_item_local($item) {
        $out = new \Out_Return();

        if(!empty($item)) {
            $item = (array)$item;

            $pid = (int)$item['item_product_id'];
            $price_id = (int)$item['item_product_price_id'];
            $variant_id = (int)$item['item_product_variant_id'];

            $exist_index = $this->has_item($pid, $variant_id, $price_id);

            if($exist_index !== false) {
                $out->data('index', $exist_index);

                unset($this->_items[$exist_index]);

                $this->reload();
                $this->cookie_save();


                $out->success();
            }

        } else {
            $out->error('No item was submitted!');
        }

        return $out;
    }
}