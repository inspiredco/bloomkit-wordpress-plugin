<?php
namespace App\Classes;

class Order {

    public $_data = false;

    public function __construct($data = false) {
        $this->load($data);
    }

    public function load($data) {
        if(!empty($data)) {
            if(is_array($data)) {
                $data = (object)$data;
            }

            if(is_object($data)) {
                $this->_data = $data;
            }
        }
    }

    public function is_loaded() {
        return $this->has_id();
    }

    public function has_id() {
        return (!empty($this->_data->order_id));
    }

    public function is_paid() {
        return ($this->_data->order_paid === 'Y');
    }

    public function is_complete() {
        return ($this->_data->order_completed === 'Y');
    }

    public function is_approved() {
        return ($this->_data->order_approved === 'Y');
    }

    public function get_id() {
        if($this->has_id()) {
            return $this->_data->order_id;
        }

        return false;
    }

    public function get_data() {
        return $this->_data;
    }

    public function get_status_title() {
        return $this->_data->status->title;
    }

    public function get_date($type = 'created', $format = 'F jS, Y @ g:i a') {
        $date_str = $this->get_data()->{'order_date_'.$type};

        if(!empty($date_str)) {
            //$date = Carbon::createFromFormat('Y-m-d H:i:s', $date_str);
            $date = $date_str;

            if(!empty($date)) {
                return $date;
                //return $date->format($format);
            }
        }

        return false;
    }

    public function get_items() {
        $items = $this->get_data()->items;

        //Debug::look($items);

        if(!empty($items)) {
            $collection = collect($items)->map(function($item) {
                $item->product = new Product($item->product);

                return $item;
            });

            return $collection;
        }

        return [];
    }

    public function get_item_count() {
        if($this->has_id()) {
            $count = 0;

            if(!empty($this->_data->order_items_total)) {
                $count = $this->_data->order_items_total;
            }

            return $count;
        }

        return false;
    }

    public function get_totals() {
        return $this->get_data()->totals;
    }

    public function get_cost($key = '', $format = false) {
        if($this->has_id()) {
            if(!empty($key)) {
                $cost = 0;

                if(!empty($this->_data->{'order_cost_'.$key})) {
                    $cost = $this->_data->{'order_cost_'.$key};
                }

                return ($format === true) ? number_format($cost, 2, '.', ',') : $cost;
            }
        }

        return false;
    }

    public function get_cost_subtotal($format = false) {
        return $this->get_cost('subtotal', $format);
    }

    public function get_cost_total($format = false) {
        return $this->get_cost('total', $format);
    }

    public function get_cost_shipping($format = false) {
        return $this->get_cost('shipping', $format);
    }

    public function get_cost_tax($format = false) {
        return $this->get_cost('tax', $format);
    }

    public function get_method_id($type) {
        $id = false;

        if($this->has_id()) {
            if($type === 'shipping') {
                $id = $this->_data->order_shipping_method_id;
            } elseif($type === 'billing') {
                $id = $this->_data->order_billing_method_id;
            }
        }
        return $id;
    }

    public function get_method($type) {
        if($this->has_id()) {
            if(!empty($this->_data->{$type})) {
                return $this->_data->{$type};
            }
        }

        return false;
    }

    public function get_shipping_method() {
        return $this->get_method('shipping');
    }

    public function get_shipping_method_key() {
        $method = $this->get_method('shipping');

        if(!empty($method) && !empty($method->service)) {
            return $method->service->service_key;
        }

        return false;
    }

    public function get_billing_method() {
        return $this->get_method('billing');
    }

    public function get_billing_method_key() {
        $method = $this->get_method('billing');

        if(!empty($method) && !empty($method->service)) {
            return $method->service->service_key;
        }

        return false;
    }

    public function has_address($type) {
        if($this->has_id()) {
            if(!empty($this->get_data()->shipping_address)) {
                return true;
            }
        }

        return false;
    }

    public function get_address($type) {
        if($this->has_address($type)) {
            return $this->get_data()->shipping_address;
        }

        return false;
    }

    public function get_shipping_address() {
        return $this->get_address('shipping');
    }
}