<?php
namespace App\Classes;


class Website {
    public static $_data;
    public static $_requests = [];

    public static $_config = [
        'lang' => 'en'
    ];

    public static function init() {
        self::customer_auth();

        // set lang, get from BK API
        self::request_init();
    }

    public static function parse_request_url($url) {
        $parts = explode('/', trim($url));

        $supported_langs = ['en-us', 'es'];

        $lang = 'en-us';

        if(strlen($parts[0]) === 2) {
            $lang = $parts[0];
        } elseif(strlen($parts[1]) === 2) {
            $lang = $parts[1];
        }

        //dd($parts, strlen($parts[1]), isset($supported_langs[$parts[1]]));

        if(!empty($lang)) {
            if(in_array($lang, $supported_langs)) {
                self::set_lang($lang);
            }

            //dd(self::get_lang());
        }

    }

    public static function set_lang($key) {
        self::$_config['lang'] = $key;
    }

    public static function get_lang() {
        return self::$_config['lang'];
    }

    public static function customer_auth() {
        //$post = input()->all();
        $post = $_POST;

        Customer::load();

        if(!empty($post['bk_customer_login_user']) && !empty($post['bk_customer_login_pass'])) {
            $login = Customer::login($post['bk_customer_login_user'], $post['bk_customer_login_pass']);

            if($login === true) {
                wp_redirect(bk_get_route_url('account'));
            }
        }
    }


    public static function get_data() {
        return self::$_data;
    }

    public static function request_init() {

        self::$_requests['customer'] = true;
        self::$_requests['cart'] = true;
        self::$_requests['client_locations'] = true;
        self::$_requests['sitemap'] = false;

        $path = $_SERVER['REQUEST_URI'];

        self::parse_request_url($path);

        Bloomkit::set_config(['language' => self::get_lang()]);

        // Check if a customer token exists in cookie
        $token = Customer::get_token_cookie();

        if(!empty($token)) {
            // Add the token to all BK API requests
            Bloomkit::set_config(['customer_token' => $token]);
        }

        if(!empty(Customer::get_cart())) {
            $cart_cookie = Customer::get_cart()->get_items();

            if(!empty($cart_cookie)) {
                self::$_requests['cart'] = $cart_cookie;
            }
        }


        // If sitemap cache is outdated, request new from API
        //Debug::look('COD:'.Sitemap::cache_outdated());

        if(Sitemap::cache_outdated()) {
            self::$_requests['sitemap'] = true;
        }


        //look(self::$_requests);


        $post_data = [
            'serve_requested' => self::$_requests
        ];

        if(!empty($path)) {
            $path = explode('/', trim($path, '/'));

            $post_data['path'] = $path;
        }

        //look($post_data);

        $init = Bloomkit::post('', $post_data);

        if($init->is_success()) {
            $init_data = $init->get_data();

            // If there is customer data, load customer

            //Debug::look($init_data->categories);

            if(!empty($init_data->customer)) {
                Customer::load($init_data->customer);

                if(Customer::is_auth()) {
                    Customer::get_cart()->clear_local();
                }
            } else {

                Customer::delete_token_cookie();

                //Debug::look('Remove Customer Cookie...');
            }

            if(!empty($init_data->cart)) {
                Customer::get_cart()->load($init_data->cart);
                //look($init_data->cart);
            }


            if(!empty($init_data->categories)) {
                ProductCategories::load($init_data->categories);
            } else {
                Shop::browse([
                    'include' => ['categories']
                ]);
            }

            if(!empty($init_data->stores)) {
                Stores::load($init_data->stores);
            }

            //Debug::look(Customer::get_cart()->get_data());
            //look(self::$_requests);
            //Debug::look($init_data->sitemaps);

            self::$_data = $init_data;

            if(Sitemap::cache_outdated()) {
                //Debug::look('Site Map Out of Date');
                if(!empty($init_data->sitemaps)) {
                    //self::sitemap_save($init_data->sitemaps);
                }
            }

            return true;
        }

        return false;
    }

    public static function sitemap_init() {
        Sitemap::cache_load();
//
//        if(Sitemap::cache_outdated()) {
//            //Debug::look('Site Map Out of Date');
//           self::sitemap_save();
//        }

        Sitemap::xml_init();
    }

    public static function sitemap_save($indexes) {

        if(class_exists('WPSEO_Sitemaps')) {

            Sitemap::clear_pages();

            if(!empty($indexes->products)) {
                foreach($indexes->products as $prod) {
                    $product = new Product($prod);

                    Sitemap::add_page('products', $product->get_url());
                }
            }

            $cats = ProductCategories::list_all();

            foreach($cats as $id => $data) {
                $cat = $data->category;

                Sitemap::add_page('product-categories', $cat->url);

            }

            Sitemap::cache_save();
            //
        }
    }


}