<?php
namespace App\Classes;

use Mockery\Exception;

class Plugin {


    public function __construct() {

    }



    public static function init() {

        //Debug::look('PLUGIN INIT');
        Website::sitemap_init();

        Wordpress::register_xhr_profiles();
        Wordpress::register_rewrite_rules();
        Wordpress::register_shortcodes();
    }

    public static function init_wp() {

       // Debug::look('PLUGIN WP LOAD');

        // Init the website, request BK init info, load customer
        Website::init();

        add_action( 'wp_enqueue_scripts', function() {
            bk_get_included_css();
            bk_get_included_js();
        });

    }

    public static function init_site() {
        //Website::customer_auth();

    }

    public static function init_page() {
        global $wp_query;

        if(!empty($wp_query->query['pagename'])) {
            $page = get_page_by_path($wp_query->query['pagename']);
        }

        //Debug::look($page);

        if(!empty($page)) {
            if(!empty($page->ID)) {
                $html = false;
                $page_route = Routes::get_by_page_id($page->ID);

                if(!empty($page_route)) {
                    if(is_callable($page_route['controller'])) {
                        $controller = $page_route['controller']();
                    }
                }

                //Debug::look($page_route);

                if(!empty($controller)) {
                    if(method_exists($controller, 'render_html')) {
                        $html = $controller::render_html();

                        //Debug::look((string)$html);
                    } else {
                        throw new Exception('Controller must have render_html method');
                    }
                }

                if($html !== false) {
                    Wordpress::register_page_title();
                    Wordpress::register_page_meta();

                    add_filter('the_content', function ($body) use ($html) {
                        if(empty($html)) {
                            return $body;
                        }

                        return $html;
                    });
                }
            }
        }
    }

    public static function get_upload_dir($type = false, $create = true) {
        $out = false;
        $upload_dir = wp_upload_dir();

        if(!empty($upload_dir['basedir'])) {
            $dir = $upload_dir['basedir'].'/bloomkit/';

            if(!empty($type)) {
                $dir .= $type.'/';
            }

            if(is_writable($dir)) {
                $out = $dir;
            } else {
                if($create === true) {
                    if(mkdir($dir, 0775, true) === true) {
                        $out = $dir;
                    }
                }
            }
        }

        return $out;
    }
}