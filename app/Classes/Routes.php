<?php
namespace App\Classes;

class Routes {
    public static $_routes = [];
    public static $_route_pages = [];
    public static $_lang_map = [];


    public static function load_route_pages() {
        $route_pages = Config::get('route_pages');

        if(!empty($route_pages)) {
            self::$_route_pages = $route_pages;
        }
    }

    public static function add_lang_page($lang, $key) {

    }

    public static function register($key, $fn, $config = []) {

        //todo: quick hack to allow route registration to be moved from providers, into their controller
        // need to make controllers not static
        $control = $fn();

        if(method_exists($control, 'register')) {
            $control::register();
        }

        unset($control);

        self::$_routes[$key] = [
            'controller' => $fn,
            'config' => $config
        ];
    }

    public static function get_by_page_id($id) {
        $id = (int)$id;

        if(!empty($id)) {
            $page_route_keys = Config::get('route_pages');

            if (!empty($page_route_keys) && !empty($page_route_keys[$id])) {
                $page_route_key = $page_route_keys[$id];

                return Routes::get_by_key($page_route_key);
            }
        }

        return false;
    }

    public static function get_by_key($key) {
        if(!empty(self::$_routes[$key])) {
            return self::$_routes[$key];
        }

        return false;
    }


    public static function get_key_page_id($key) {
        $id = false;
        $page_route_keys = Config::get('route_pages');

        if(!empty($page_route_keys) && is_array($page_route_keys)) {
            $id = array_search($key, $page_route_keys);
        }

        return $id;
    }

}