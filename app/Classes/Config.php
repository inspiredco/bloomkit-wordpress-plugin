<?php
namespace App\Classes;

class Config {
    public static $_config_key = 'bloomkit_plugin_config';
    public static $_data = [];

    public static function load() {
        $option = get_option(self::$_config_key);

        if(!empty($option)) {
            self::$_data = $option;
        }
    }

    public static function get($key = false) {
        if(!empty($key)) {
            if(!empty(self::$_data[$key])) {
                return self::$_data[$key];
            }
        } else {
            return self::$_data;
        }

        return false;
    }

    public static function set($key, $value = null) {
        $save = false;

        if(!empty($key)) {
            if(is_array($key)) {
                self::$_data = array_merge(self::$_data, $key);
                $save = true;
            } elseif(is_string($key)) {
                if($value !== null) {
                    self::$_data[$key] = $value;
                    $save = true;
                }
            }
        }

        if($save === true) {
            self::save();
        }
    }

    public static function save() {
        update_option(self::$_config_key, self::$_data);
    }
}