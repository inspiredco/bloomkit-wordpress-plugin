<?php

$url = parse_url(home_url());
$url_key = '';

//todo: Just a quick hack to randomize cookie name a bit between dev/stage/local
if(BK_DEV_MODE === true) {
    $url_key = '_'.substr(md5($url['host']), 0, 6);
}


return [
    'email_from_name' => 'Primo',
    'email_from_address' => 'info@itsprimo.com',

    'plugin_dir' => __DIR__,

    'cookie_domain' => $url['host'],
    'cookie_name_cart' => 'bk_customer_local_cart'.$url_key,
    'cookie_name_customer' => 'bk_customer_token'.$url_key,
];